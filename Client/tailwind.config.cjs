/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ['./src/**/*.{js,jsx}'],
    theme: {},
    plugins: [require('tailwindcss'), require('postcss')],
};
