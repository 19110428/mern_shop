import axiosClient from '../../api/axios.config';
import { productService } from '../../services';
import {
    getAllProducts,
    handleFilter,
    getLocationProduct,
    getProductDetail,
    updateAllProduct,
} from './productsSlice';

export const getProductQuery = async (dispatch, data) => {
    let res = await axiosClient.get(`/product/query?category=${data.category}&brand=${data.brand}&limit=${data.limit}`);
    dispatch(getAllProducts(res));
};

export const getProductQueryVer2 = async (dispatch, data) => {
    let res = await axiosClient.post(`/product/queryproduct`,data);
    dispatch(getAllProducts(res));
};

export const getAllProductByCategory = async (dispatch, category) => {
    let res = await axiosClient.get(`/product/query?category=${category})`);
    dispatch(getAllProducts(res));
};

export const getAllProductApi = async (dispatch) => {
    let res = await axiosClient.get('/product/all');
    dispatch(getLocationProduct(res));
};

export const getProductDetailApi = async (dispatch, slug) => {
    let res = await axiosClient.get(`/product/getbyslug/${slug}`);
    console.log(res);
    // let resRating = await ratingService.getRating(res[0].id);
    // dispatch(getProductDetail({ ...res[0], rating: resRating }));
    dispatch(getProductDetail(res));
};

export const getLocation = async (dispatch, location) => {
    let res = await productService.getProductByLocation(location);
    dispatch(getLocationProduct(res));
};

export const HandleFilter = async (dispatch, data) => {
    dispatch(handleFilter(data));
};

export const updateAllProducts = async (dispatch, data) => {
    dispatch(updateAllProduct(data));
};

export const searchProduct = async (dispatch, name) => {
    let res = await axiosClient.get(`/product/search/${name})`);
    dispatch(getAllProducts(res));
};
