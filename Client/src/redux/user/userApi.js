import axiosClient from '../../api/axios.config';
import { login, logout } from './userSlice';

export const _loginPass = async (data, dispatch, navigate) => {
    let res = await axiosClient.post('/auth/login', data);
    dispatch(login(res));
    if (res) {
        navigate('/');
    }
};

export const _getSuccess = async (dispatch, navigate) => {
    const res = await axiosClient.get(
        '/auth/login/success',
        { withCredentials: true },
        {
            headers: { 'Access-Control-Allow-Credentials': true },
        },
    );

    dispatch(login(res));
};

export const _loginPhone = async (data, dispatch, navigate) => {
    let res = await axiosClient.post('/auth/phone/signup', data);
};

export const _verifyPhone = async (data, dispatch, navigate) => {
    let res = await axiosClient.post('/auth/phone/verify', data);
    dispatch(login(res));
    if (res) {
        navigate('/');
    }
};

export const _logout = async (dispatch, navigate) => {
    try {
        const b = await axiosClient.post('/auth/logout');
        window.open('http://localhost:8000/auth/logout', '_self');
        dispatch(logout());
        navigate('/');
    } catch (err) {}
};

export const _editUser = async (dispatch, data, id) => {
    try {
        const res = await axiosClient.put(`/user/edit/${id}`, data);
        dispatch(login(res));
    } catch (err) {}
};

export const _editPass = async (dispatch, data, id) => {
    try {
        const res = await axiosClient.put(`/user/editPass/${id}`, data);
        dispatch(login(res));
    } catch (err) {}
};