import productModalReducer from './product-modal/productModalSlice';
import cartItemsReducer from './shopping-cart/cartItemsSlide';
import userReducer from './user/userSlice';
import products from './product/productsSlice';
import searchSlice from './search/searchSlice';
import historyOrdersSlice from './history/historyOrdersSlice';
import orderSlice from './order/orderSlice';
import ratingSlice from './rating/ratingSlice';

import { configureStore, combineReducers } from '@reduxjs/toolkit';

import { persistStore, persistReducer, FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const persistConfig = {
    key: 'root',
    version: 1,
    storage,
};
const appReducer = combineReducers({
    user: userReducer,
    productModal: productModalReducer,
    cartItems: cartItemsReducer,
    user: userReducer,
    products: products,
    search: searchSlice,
    historyOrders: historyOrdersSlice,
    order: orderSlice,
    rating: ratingSlice,
});
const rootReducer = (state, action) => {
    if (action.type === 'auth/logOutSuccess') {
        return appReducer(undefined, action);
    }
    return appReducer(state, action);
};
const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
    reducer: persistedReducer,
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: {
                ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
            },
        }),
});

export let persistor = persistStore(store);
