import { searchService } from '../../services/search.service';
import { getResultSearch, removeResultSearch } from './searchSlice';
import { axiosClient } from '~/api/';
export const getResult = async (dispatch, value) => {
    let res = await axiosClient.get(`/product/search/${value})`);
    dispatch(getResultSearch(res));
};
export const removeResult = (dispatch) => {
    dispatch(removeResultSearch());
};
