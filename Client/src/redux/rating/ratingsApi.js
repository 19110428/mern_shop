import axiosClient from '../../api/axios.config';
import { addRating } from './RatingSlice';

export const _newRating = async (dispatch, data) => {
    let res = await axiosClient.post('rating/new', data);
    dispatch(addRating(res));
};

export const _getAllRatingProduct = async (product_id) => {
    let res = await axiosClient.get(`rating/get/${product_id}`);
    return res;
};

export const _addDiscussRating = async (id, data) => {
    let res = await axiosClient.put(`/rating/${id}/addDiscuss`, data);
    console.log({ data });
    return res;
};
