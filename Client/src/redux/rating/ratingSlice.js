import { createSlice } from '@reduxjs/toolkit';

export const ratings = createSlice({
    name: 'ratings',
    initialState: {
        rating: {
            data: null,
        },
        all: {
            data: null,
        },
    },
    reducers: {
        Rating: (state, action) => {
            state.rating.data = action.payload;
        },
        allRating: (state, action) => {
            state.all.data = action.payload;
        },
        addRating: (state, action) => {
            state.all.data.push(action.payload);
        },
        // updateRating: (state, action) => {
        //     return state.all.data.map((rating) => {
        //         if (rating.id !== action.payload.id) {
        //             return rating;
        //         }
        //         return {
        //             ...rating,
        //             action.payload,
        //         };
        //     });
        // },
    },
});
export const { Rating, allRating, addRating } = ratings.actions;
export default ratings.reducer;
