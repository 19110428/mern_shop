import axiosClient from '../api/axios.config';

export const searchService = {
    getResultSearchApi(value) {
        return axiosClient.get(`/products?q=${value}`);
    },
};
