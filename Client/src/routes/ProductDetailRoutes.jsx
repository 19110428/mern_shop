import { lazy, Suspense } from 'react';
import LoadingSkeleton from '../components/Loading/LoadingSkeleton';
const ProductDetail = lazy(() => import('../pages/ProductDetail'));
const urls = ['phone/:productSlug', 'laptop/:productSlug', 'tablet/:productSlug'];

export const productDetailRoutes = urls.map((url) => ({
    path: url,
    element: (
        <Suspense fallback={<LoadingSkeleton />}>
            <ProductDetail />
        </Suspense>
    ),
}));
