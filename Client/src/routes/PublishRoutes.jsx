import { lazy, Suspense } from 'react';
import LoadingSkeleton from '../components/Loading/LoadingSkeleton';
import Manage from '../pages/Manage';
import Order from '../pages/Order/Order';
import Search from '../pages/Search/Search';
const Home = lazy(() => import('../pages/Home'));
const Cart = lazy(() => import('../pages/Cart'));
const Device = lazy(() => import('../pages/Device'));
const phone = 'Phone';
const tablet = 'Tablet';
const laptop = 'Laptop';
export const publishRoutes = [
    {
        index: true,
        element: (
            <Suspense fallback={<LoadingSkeleton />}>
                <Home title="KPShop.com - Điện thoại, Laptop chính hãng" />
            </Suspense>
        ),
    },
    {
        path: 'cart',
        element: (
            <Suspense fallback={<LoadingSkeleton />}>
                <Cart title="Giỏ hàng - KPshop.com" />
            </Suspense>
        ),
    },
    {
        path: phone,
        element: (
            <Suspense fallback={<LoadingSkeleton />}>
                <Device device={phone} title="Sản phẩm - KPshop.com" />
            </Suspense>
        ),
    },
    {
        path: tablet,
        element: (
            <Suspense fallback={<LoadingSkeleton />}>
                <Device device={tablet} title="Sản phẩm  - KPshop.com" />
            </Suspense>
        ),
    },
    {
        path: laptop,
        element: (
            <Suspense fallback={<LoadingSkeleton />}>
                <Device device={laptop} title="Sản phẩm  - KPshop.com" />
            </Suspense>
        ),
    },
    {
        path: 'tim-kiem/:name',
        element: (
            <Suspense fallback={<LoadingSkeleton />}>
                <Search title="Sản phẩm  - KPshop.com" />
            </Suspense>
        ),
    },
    {
        path: 'account',
        element: (
            <Suspense fallback={<LoadingSkeleton />}>
                <Manage path="account" title="Thông tin cá nhân - KPshop.com" />
            </Suspense>
        ),
    },

    {
        path: 'history',
        element: (
            <Suspense fallback={<LoadingSkeleton />}>
                <Manage path="history" title="Lịch sử đơn hàng  - KPshop.com" />
            </Suspense>
        ),
    },
    {
        path: 'address',
        element: (
            <Suspense fallback={<LoadingSkeleton />}>
                <Manage path="address" title="Số địa chỉ  - KPshop.com" />
            </Suspense>
        ),
    },
    {
        path: 'account/edit/password',
        element: (
            <Suspense fallback={<LoadingSkeleton />}>
                <Manage path="password" title="Cập nhật mật khẩu - KPshop.com" />
            </Suspense>
        ),
    },
    {
        path: 'order',
        element: (
            <Suspense fallback={<LoadingSkeleton />}>
                <Order title="Xác nhận đơn hàng  - KPshop.com" />
            </Suspense>
        ),
    },
    {
        path: 'order/:id',
        element: (
            <Suspense fallback={<LoadingSkeleton />}>
                <Manage path="orderdetail" title="Chi tiết đơn hàng - KPshop.com" />
            </Suspense>
        ),
    },
];
