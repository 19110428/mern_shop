import { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import ProductItem from './ProductItem';
import { numberWithCommas } from '../../utils';
import { LocationForm } from '../../components/LocationForm';
import moment from 'moment';
import { useCart } from '../../hooks';
import { useDispatch, useSelector } from 'react-redux';
import Input from '../../components/Input';
import { saveOrders } from '../../redux/order/ordersApi';
import { _editUser } from '../../redux/user/userApi';
import { BsChevronLeft } from 'react-icons/bs';

function CartInfo() {
    const currentUser = useSelector((state) => state.user?.currentUser);
    // Cart
    const cartData = useCart();
    const [addressOption, setAddresOption] = useState();
    const [change, setChange] = useState(false);
    const { cartItems, totalPrice, totalQuantity } = cartData;
    const dispatch = useDispatch();
    const navigate = useNavigate();

    // User info
    const [fullname, setfullName] = useState(currentUser?.address[0]?.fullname);
    const [phone, setPhone] = useState(currentUser?.address[0]?.phone);
    const [address, setAddress] = useState(currentUser?.address[0]?.address);

    const handleSubmit = async (e) => {
        e.preventDefault();

        const dataPostOrder = {
            _id: Date.now(),
            customer_id: currentUser?.userId,
            fullname,
            phone,
            address,
            receiver: '',
            payment: {
                name: '',
                paid: 'false',
            },
            totalPrice: totalPrice,
            totalQuantity: totalQuantity,
            status: 'Đang xử lý',
            order_items: cartItems,

            time: moment().format('HH:MM MM/DD/YYYY'),
        };

        saveOrders(dispatch, dataPostOrder);

        if (!currentUser?.address[0]) {
            const dataUser = {
                address: [
                    {
                        fullname,
                        phone,
                        address,
                    },
                ],
            };
            await _editUser(dispatch, dataUser, currentUser?._id);
        }
        navigate('/order');
    };
    useEffect(() => {
        if (!currentUser?.address[0]) {
            setChange(true);
        }
    }, []);

    const handleChange = () => {
        setChange((pre) => !pre);
    };

    const handleAddress = (e) => {
        if (e.city) {
            setAddresOption(e);
        }
        const homeAdd = document.getElementById('homeAddress').value;
        const { ward, district, city } = addressOption;
        const string = `${homeAdd}, ${ward}, ${district}, ${city}`;
        setAddress(string);
    };
    return (
        <div className="text-lg">
            <div className="flex justify-between">
                <p className="text-2xl font-semibold">Giỏ hàng</p>
                <Link to="/" className="text-blue-500 flex items-center">
                    <BsChevronLeft />
                    Mua thêm sản phẩm khác
                </Link>
            </div>
            <div className="flex space-x-10 mt-2">
                <form className="bg-white rounded-xl border p-6 shadow-xl grow">
                    <div className="">
                        {cartItems.map((product, index) => (
                            <ProductItem key={index} {...product} />
                        ))}
                    </div>
                </form>
                <div className="w-[350px] flex flex-col space-y-4 text-base">
                    <div className="bg-white rounded-xl p-6 shadow-xl flex flex-col space-y-4 border">
                        <div className="flex items-center justify-between">
                            <h3 className="font-semibold text-xl">Giao tới</h3>
                            <a onClick={handleChange}>Thay đổi</a>
                        </div>
                        <div class="flex items-center space-x-8 font-semibold">
                            <p>{fullname}</p>
                            <p>{phone}</p>
                        </div>
                        <div class="inline-flex text-xl">{address}</div>
                    </div>

                    {change && (
                        <div className="bg-white rounded-xl p-6 shadow-xl flex flex-col space-y-4 border">
                            <h4>THÔNG TIN KHÁCH HÀNG</h4>
                            <div className="space-x-4">
                                <input id="male" type="radio" name="sex" value="Anh" defaultChecked />
                                <label htmlFor="male">Anh</label>
                                <input id="female" type="radio" name="sex" value="Chị" />
                                <label htmlFor="female">Chị</label>
                            </div>
                            <div className="flex space-x-4">
                                <Input
                                    placeholder="Họ và Tên"
                                    id="fullname"
                                    required={true}
                                    value={fullname}
                                    onChange={(e) => setfullName(e.target.value)}
                                />
                                <Input
                                    value={phone}
                                    onChange={(e) => setPhone(e.target.value)}
                                    placeholder="Số điện thoại"
                                    id="phone"
                                    type="tel"
                                    required={true}
                                    pattern="(84|0[3|5|7|8|9])+([0-9]{8})\b"
                                />
                            </div>
                            <h4>ĐỊA CHỈ NHẬN HÀNG</h4>
                            <div>
                                <div className="flex flex-col space-y-4 border border-blue-400 p-4 rounded-xl">
                                    <p>Vui lòng chọn địa chỉ nhận hàng</p>
                                    <LocationForm
                                        onChange={(e) => {
                                            handleAddress(e);
                                        }}
                                    />
                                    <Input
                                        placeholder="Số nhà, tên đường"
                                        id="homeAddress"
                                        required={true}
                                        onChange={(e) => {
                                            handleAddress(e);
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                    )}

                    <div className="bg-white rounded-xl p-6 shadow-xl flex flex-col space-y-4 border">
                        <div className="flex justify-between">
                            <span className="text-xl font-semibold">Tổng tiền:</span>
                            <span className="font-bold text-red-600">{numberWithCommas(totalPrice)}₫</span>
                        </div>
                        <div>
                            <p className="text-green-600 float-right">Miễn phí giao hàng</p>
                        </div>
                        <button
                            onClick={handleSubmit}
                            type="submit"
                            className="bg-yellow-300 py-2 rounded-lg w-full text-white font-bold"
                        >
                            ĐẶT HÀNG
                        </button>
                        <p className="text-justify text-sm">
                            Bạn có thể chọn hình thức thanh toán sau khi đặt hàng. Bằng cách đặt hàng, bạn đồng ý với
                            Điều khoản sử dụng của KPShop
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CartInfo;
