import { Link } from 'react-router-dom';
import { numberWithCommas } from '../../utils';
import { useDispatch } from 'react-redux';
import { removeItem, updateItem } from '../../redux/shopping-cart/cartItemsSlide';
import { useSelector } from 'react-redux';
import { CounterQuantity } from '../../components/Quantity';
import { AiFillDelete } from 'react-icons/ai';

function ProductItem(props) {
    const cartItems = useSelector((state) => state.cartItems.value);
    const dispatch = useDispatch();

    const removeCartItem = () => {
        cartItems.forEach((item) => {
            if (item.slug === props.slug) {
                dispatch(removeItem(item));
            }
        });
    };
    const updateCartItem = (value) => {
        cartItems.forEach((item) => {
            if (item.slug === props.slug) {
                dispatch(updateItem({ ...item, quantity: value }));
            }
        });
    };
    return (
        <div className="flex justify-between items-center space-x-4 border-b pb-4 text-base">
            <img src={props.img} className="h-20" alt="" />
            <div className="w-40">
                <Link to={`/${props.category}/${props.slug}`} className="font-semibold">
                    {props.title}
                </Link>
            </div>
            <div>
                <p className="text-red-500">{numberWithCommas(props.price * (1 - props.discount))}₫</p>
                <p className="line-through">{numberWithCommas(props.price)}₫</p>
            </div>
            <span>
                <CounterQuantity value={props.quantity} size="sm" />
            </span>
            <div className="w-32">
                <b className="text-red-500">{numberWithCommas(props.price * (1 - props.discount) * props.quantity)}₫</b>
            </div>
            <button
                className="text-red-600"
                onClick={() => {
                    removeCartItem();
                }}
            >
                <AiFillDelete size={24} />
            </button>
        </div>
    );
}

export default ProductItem;
