import { useSelector } from 'react-redux';
import RatingTotal from '../../components/Rating/RatingTotal';
function Head() {
    const initProductDetail = useSelector((state) => state.products?.productDetail?.data);
    const ratingProduct = useSelector((state) => state.rating?.all?.data);

    const { title, brand } = initProductDetail;

    return (
        <div className="">
            <p className="capitalize text-base">
                Thương hiệu: <span className="text-blue-500">{brand}</span>
            </p>
            <h1 className="text-3xl font-semibold">{title}</h1>
            <RatingTotal ratingProduct={ratingProduct} />
        </div>
    );
}

export default Head;
