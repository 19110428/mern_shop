import { useSelector } from 'react-redux';
import { twMerge } from 'tailwind-merge';
function Parameter(props) {
    const initProductDetail = useSelector((state) => state.products?.productDetail?.data);

    const { parameter, title } = initProductDetail;
    return (
        <div className="my-8 text-base">
            <p className="font-bold text-2xl text-gray-800 mb-4">Cấu hình {title}</p>
            <table className="w-full">
                <tbody>
                    {parameter &&
                        Object.entries(parameter).map((param, index) => {
                            if (index != 0) {
                                return (
                                    <tr className={twMerge(index % 2 === 0 && 'bg-gray-100')} key={index}>
                                        <td colSpan="4" className="p-4">
                                            {param[0]}
                                        </td>
                                        <td colSpan="6" className="p-4">
                                            {param[1]}
                                        </td>
                                    </tr>
                                );
                            }
                        })}
                </tbody>
            </table>
        </div>
    );
}

export default Parameter;
