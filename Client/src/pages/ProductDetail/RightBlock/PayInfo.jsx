import { useState } from 'react';
import { CounterQuantity } from '../../../components/Quantity';
import { discountInfo, discountMore } from './DiscountContent';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { BsCheck } from 'react-icons/bs';
import { addItem } from '../../../redux/shopping-cart/cartItemsSlide';

function PayInfo() {
    const initProductDetail = useSelector((state) => state.products?.productDetail?.data);
    const [quantity, setQuantity] = useState(1);
    const notify = () => toast.success('Thêm hàng vào giỏ thành công!');
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const handleClickPay = () => {
        const product = { ...initProductDetail, quantity: quantity };
        dispatch(addItem(product));
        window.scrollTo({
            top: 0,
            behavior: 'smooth',
        });
        setQuantity(1);
        notify();
        navigate('/cart');
    };
    return (
        <div className="text-lg">
            <div className="flex items-center">
                <span className="block w-28">Số lượng:</span>
                <CounterQuantity
                    value={1}
                    size="lg"
                    onChange={(e) => {
                        setQuantity(e);
                    }}
                />
            </div>

            <button
                className="bg-red-500 my-6 block py-3 px-5 rounded-lg text-white font-semibold hover:bg-red-700"
                onClick={() => {
                    handleClickPay();
                }}
            >
                Chọn mua
            </button>
            <div className="border border-gray-400 my-8">
                <div className="bg-gray-100 p-4 border-b border-gray-400 font-bold text-xl">Khuyến mãi</div>
                <ul className="p-4 text-base">
                    {discountInfo.map((item, index) => {
                        return (
                            <li key={index}>
                                <span className="bg-blue-500 rounded-full h-7 w-7 inline-block text-center text-white mr-4">
                                    {index + 1}
                                </span>
                                <span>{item}</span>
                            </li>
                        );
                    })}
                </ul>
            </div>

            <div className="border border-gray-400 my-8">
                <div className="bg-gray-100 p-4 border-b border-gray-400 font-bold text-xl">4 ưu đãi thêm</div>
                <ul className="p-4 text-base">
                    {discountMore.map((item, index) => {
                        return (
                            <li key={index}>
                                <span className="bg-blue-500 rounded-full h-7 w-7 inline-block text-center text-white mr-4 font-bold">
                                    <BsCheck className="inline" size={26} />
                                </span>
                                <span>{item}</span>
                            </li>
                        );
                    })}
                </ul>
            </div>
        </div>
    );
}

export default PayInfo;
