import { useSelector } from 'react-redux';

function Tag() {
    const initProductDetail = useSelector((state) => state.products?.productDetail?.data);

    const { RAM } = initProductDetail;

    return (
        <div className="mb-4">
            <div className="flex flex-wrap mt-6 items-center ">
                {RAM?.map((tag, index) => {
                    return (
                        <button
                            className={twMerge(
                                'border border-gray-400 px-8 py-4 text-xl rounded text-blue-400 hover:border-blue-400 mr-6',
                            )}
                            key={index}
                        >
                            {tag}
                        </button>
                    );
                })}
            </div>
        </div>
    );
}

export default Tag;
