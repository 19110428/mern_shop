import { numberWithCommas } from '../../../utils';
import { useSelector } from 'react-redux';

function DiscountBox() {
    const initProductDetail = useSelector((state) => state.products?.productDetail?.data);

    const { price, discount } = initProductDetail;
    return (
        <>
            <div className="flex items-center text-lg">
                <span className="block w-28">Giá:</span>
                <div className="space-x-4">
                    <span className="text-red-600 font-semibold text-4xl">
                        {numberWithCommas(Math.round(price * (1 - parseFloat(discount))))}₫
                    </span>
                    <i className="line-through text-gray-600">{numberWithCommas(price)}₫</i>

                    <span className="text-red-600">(-{discount * 100}%)</span>
                </div>
            </div>
        </>
    );
}

export default DiscountBox;
