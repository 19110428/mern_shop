import { useEffect } from 'react';
import Section from '../../components/Section';
import { useDispatch, useSelector } from 'react-redux';
import { getProductQuery } from '../../redux/product/productsApi';
import Carousel from '../../components/Carousel';

function OtherProduct(props) {
    const dispatch = useDispatch();
    useEffect(() => {
        const data = {
            category: props.initProductDetail.category,
            brand: props.initProductDetail.brand,
            limit: '',
        };
        console.log(data);

        getProductQuery(dispatch, data);
    }, []);
    const products = useSelector((state) => state?.products?.allProducts?.data);
    console.log(products);
    return (
        <Section styles="border border-solid border-gray-300 px-4">
            <div className="flex justify-between items-center text-black w-full">
                <p className="text-xl font-semibold">Xem thêm điện thoại khác</p>
            </div>
            <Carousel data={products} />
        </Section>
    );
}

export default OtherProduct;
