import { Link } from 'react-router-dom';
import Head from './Head';
import OtherProduct from './OtherProduct';
import { Parameter, DiscountBox, PayInfo, Tag } from './RightBlock';
import { Article, Policy, SlickBlock } from './LeftBlock';
import { useDispatch, useSelector } from 'react-redux';
import RatingProduct from '../../components/Rating';
import { BsChevronRight } from 'react-icons/bs';
import { ProductHistory } from '../../components/DisplayProduct';
import { _getAllRatingProduct } from '../../redux/rating/ratingsApi';

function ProductDetail() {
    const initProductDetail = useSelector((state) => state.products?.productDetail?.data);
    const { title, slug, category, _id } = initProductDetail;

    const data = {
        breadcrumb: [
            { name: category, path: `/${category}` },
            { name: title, path: `/${category}/${slug}` },
        ],
    };

    return (
        <div className="w-[1200px] mx-auto">
            <div className="flex text-gray-500 text-lg list-none m-auto mt-6">
                {data.breadcrumb.map((item, index) => (
                    <div className="flex items-center" key={index}>
                        <Link to={item.path || '/'}>{item.name}</Link>
                        <BsChevronRight />
                    </div>
                ))}
            </div>
            <div className="flex flex-col space-y-6 py-6">
                <div className="flex gap-8">
                    <div className="w-1/2">
                        <SlickBlock />
                        <Policy />
                        <Article />
                        <RatingProduct />
                    </div>
                    <div className="w-1/2">
                        <Head />
                        <DiscountBox />
                        <Tag />
                        <PayInfo />
                        <Parameter />
                    </div>
                </div>
                <OtherProduct initProductDetail={initProductDetail} />
                <ProductHistory />
            </div>
        </div>
    );
}

export default ProductDetail;
