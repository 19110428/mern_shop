import { TbBrandDaysCounter, TbShieldCheckFilled, TbTruckDelivery } from 'react-icons/tb';

const Policy = () => {
    return (
        <ul className="flex flex-wrap m-4 text-base">
            <li className="w-1/2 p-2 border-b">
                <div className="text-blue-700 inline-block mr-2 translate-y-1">
                    <TbBrandDaysCounter size={20} />
                </div>
                Bảo hành có cam kết trong 12 tháng
                <a className="text-blue-700"> Xem chi tiết</a>
            </li>
            <li className="w-1/2 p-2 border-b">
                <div className="text-blue-700 inline-block mr-2 translate-y-1">
                    <TbShieldCheckFilled size={20} />
                </div>
                Bảo hành chính hãng 1 năm tại các trung tâm bảo hành hãng
                <a className="text-blue-700"> Xem địa chỉ bảo hành</a>
            </li>
            <li className="w-1/2 p-2">
                <div className="text-blue-700 inline-block mr-2 translate-y-1">
                    <TbTruckDelivery size={20} />
                </div>
                Giao hàng tận nhà nhanh chóng <a className="text-blue-700"> Tìm hiểu</a>
            </li>
        </ul>
    );
};

export default Policy;
