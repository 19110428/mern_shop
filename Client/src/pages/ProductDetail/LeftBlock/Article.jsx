import { useSelector } from 'react-redux';

function Article(props) {
    const initProductDetail = useSelector((state) => state.products?.productDetail?.data);

    const { article, info } = initProductDetail;

    const data = article ? article : info;
    const Art = () => {
        return <div className="text-lg" dangerouslySetInnerHTML={{ __html: data }} />;
    };
    return (
        <div className="text-2xl mb-6">
            <img src={''} alt={''} />
            <h3 className="text-2xl font-bold pb-4">Thông tin sản phẩm</h3>
            <Art />
        </div>
    );
}

export default Article;
