import { Mousewheel, Navigation } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import '../../../assets/swiper.scss';
import { useSelector } from 'react-redux';
function SlickBlock() {
    const initProductDetail = useSelector((state) => state.products?.productDetail?.data);

    const { img, gallery } = initProductDetail;

    const all = [img, ...gallery];

    return (
        <div className="border px-4 py-8">
            <Swiper
                slidesPerView={1}
                spaceBetween={16}
                navigation
                mousewheel={{
                    sensitivity: 0,
                }}
                loop={true}
                modules={[Navigation, Mousewheel]}
            >
                {all?.map((image, index) => (
                    <SwiperSlide key={index}>
                        <img src={image} alt="" className="w-full h-96 pb-8 object-scale-down" />
                    </SwiperSlide>
                ))}
            </Swiper>
            <div className="flex justify-between">
                {all?.slice(0, 5).map((image, index) => (
                    <img key={index} src={image} alt="" className="object-cover w-20 h-20" />
                ))}
            </div>
        </div>
    );
}

export default SlickBlock;
