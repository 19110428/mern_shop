
import DeviceBanner from './DeviceBanner';
import ListDevice from './ListDevice';
import { useState } from 'react';
import { useEffect } from 'react';
const Search = (props) => {
    useEffect(() => {
        document.title = props.title;
    });
    const [chose, setChose] = useState('');
   
    return (
        <div className="flex flex-col space-y-6 my-6">
            <DeviceBanner/>
            <ListDevice  chose={chose}></ListDevice>
        </div>
    );
};
export default Search;
