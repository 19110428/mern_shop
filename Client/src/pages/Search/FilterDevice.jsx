import Filter from '../../components/Filter/Filter';

const FilterDevice = (props) => {
    // let category = [];
    // props.product.map((item) => {
    //     if (!category.includes(item.brand)) category.push(item.brand);
    // });
    // console.log(category);
    const data = [
        {
            id: 1,
            title: 'Giá',
            detail:
                props.category === 'Laptop'
                    ? ['Từ 7-13 triệu', 'Từ 13-20 triệu', 'Trên 20 triệu']
                    : [
                          'Dưới 2 triệu',
                          'Từ 2-4 triệu',
                          'Từ 4-7 triệu',
                          'Từ 7-13 triệu',
                          'Từ 13-20 triệu',
                          'Trên 20 triệu',
                      ],
                      query:"price",
        },
        {
            id: 2,
            title: 'Thương hiệu',
            detail:
                props.category === 'Phone'
                    ? ['iPhone', 'Samsung', 'Vivo', 'Xiaomi']
                    : props.category === 'Tablet'
                    ? ['iPad', 'Lenovo', 'Huawei', 'Samsung']
                    : ['MacBook', 'Asus', 'HP', 'Acer'],
            query:"brand",
        },
        {
            id: 3,
            title: 'RAM',
            detail:
                props.category === 'Laptop'
                    ? ['4 GB', '8 GB', '16 GB', '64 GB']
                    : ['2 GB', '3 GB', '4 GB', '6 GB', '8 GB', '16 GB'],
            query:"parameter.RAM",
        },
        // {
        //     id: 3,
        //     title: props.category === 'Phone' ? 'ROM' : props.category === 'Tablet' ? 'Dung lượng lưu trữ' : 'Ổ cứng',
        //     detail:
        //         props.category === 'Phone'
        //             ? ['2 GB', '3 GB', '4 GB', '6 GB', '8 GB', '16 GB']
        //             : props.category === 'Tablet'
        //             ? ['32 GB', '64 GB', '128 GB', '256 GB', '1 TB']
        //             : ['128 GB', '256 GB', '512 GB', '1 TB'],
        // },
        {
            id: 4,
            title: 'SALE',
            detail: ['Giảm giá', 'Góp 0%', 'Độc quyền', 'Mới'],
            query:"parameter.RAM",
        },
    ];
    return (
        <div className="w-[1200px] mx-auto">
            <Filter handle={props.handle} data={data} />
        </div>
    );
};

export default FilterDevice;
