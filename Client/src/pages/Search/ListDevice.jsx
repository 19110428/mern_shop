import ListProduct from '../../components/ListProduct/ListProduct';
import { useEffect, useState } from 'react';
import { productService } from '../../services/product.service';
import { useSelector, useDispatch } from 'react-redux';
import { getAllProductByCategory, getProductQuery, searchProduct } from '../../redux/product/productsApi';
import handleData from '../../components/Filter/handleData';
import FilterDevice from './FilterDevice';
import { useParams } from 'react-router-dom';

const dataSelected = ['% Giảm giá cao', 'Giá cao đến thấp', 'Giá thấp đến cao'];

const ListDevice = (props) => {
    const [chose, setChose] = useState(0);
    const [checked, setChecked] = useState([]);
    const [isOpen, setisOpen] = useState(false);

    const title = useParams('name')
    console.log(title)
    const dispatch = useDispatch();
    useEffect(() => {
        
        // searchProduct(dispatch,title.name)
    }, []);
    const products = useSelector((state) => state?.products?.allProducts?.data);
    let resultSearch = useSelector((state) => state.search.search.data);



    // const [getDataFilter, setGetDataFilter] = useState(products);

    const handleClick = (index) => {
        setChose(index);
    };

    const handle = (bolen) => {
        setisOpen(bolen);
    };

   
    // const handleSort = () => {
    //     if (chose === 2) {
    //         setGetDataFilter([...getDataFilter].sort((a, b) => a.price - b.price));
    //     } else if (chose === 1) {
    //         setGetDataFilter([...getDataFilter].sort((a, b) => b.price - a.price));
    //     } else if (chose === 0) {
    //         setGetDataFilter([...getDataFilter].sort((a, b) => b.discount - a.discount));
    //     }
    // };
    const getDataFilter = products;

    return (
        <div className="flex max-w-[1200px] mx-auto space-x-6">
            <div className="w-1/6 bg-white p-4 border shadow-xl rounded-xl">
                <FilterDevice handle={handle} products={products} category={props.device} />
            </div>
            <div className="w-5/6">
                <div className="bg-white p-4 border shadow-xl rounded-xl mb-6 text-base">
                    <span className="font-bold text-xl">Sắp xếp: </span>
                    {dataSelected.map((item, index) => (
                        <button
                            className="py-2 px-5 mx-4 border-2 rounded-full hover:text-blue-700 hover:border-2 hover:border-blue-500"
                            onClick={() => {
                                handleClick(index);
                                handleSort();
                            }}
                            key={index}
                        >
                            {item}
                        </button>
                    ))}
                </div>

                <div className="bg-white p-4 border shadow-xl rounded-xl">
                    {isOpen === false ? (
                        <ListProduct products={resultSearch} isSlide={false}></ListProduct>
                    ) : (
                        <ListProduct products={resultSearch} isSlide={false}></ListProduct>
                    )}
                </div>
            </div>
        </div>
    );
};
export default ListDevice;
