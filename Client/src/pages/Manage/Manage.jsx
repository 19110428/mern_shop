import { _getAllOrders } from '../../redux/order/ordersApi';
import Address from './Address';
import History from './History/History';
import OrderDetail from './OrderDetail';
import Profile from './Profile/Profile';
import Sidebar from './Sidebar';
function Manage(props) {
    let path = props.path;
    if (path === 'orderdetail') {
        path = 'history';
    }
    return (
        <div className="w-[1200px] mx-auto my-6 flex space-x-6">
            <div className="w-1/5 flex flex-col border shadow-xl rounded-xl text-base text-black">
                <Sidebar path={path} />
            </div>
            <div className="w-4/5 border shadow-xl rounded-xl p-6">
                {props?.path === 'account' && <Profile title={props.title} />}
                {props?.path === 'history' && <History title={props.title} />}
                {props?.path === 'address' && <Address title={props.title} />}
                {props?.path === 'orderdetail' && <OrderDetail title={props.title} />}
            </div>
        </div>
    );
}

export default Manage;
