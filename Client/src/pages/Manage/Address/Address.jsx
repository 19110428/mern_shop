import { AiOutlinePlus } from 'react-icons/ai';

function Address() {
    return (
        <div className="flex flex-col space-y-4 text-base">
            <p className="text-2xl font-semibold mb-4">Số địa chỉ</p>
            <div class="flex justify-center items-center border-2 border-dashed py-6 rounded-xl text-blue-500 hover:text-blue-700 cursor-pointer">
                <AiOutlinePlus size={20} />
                <span>Thêm địa chỉ mới</span>
            </div>
            <div className="flex justify-between">
                <div class="flex flex-col space-y-4">
                    <div>Mặc định</div>
                    <div>Địa chỉ: 74/8, đường Trần Nguyên Hãn, Phường 13, Quận 8, Hồ Chí Minh</div>
                    <div>Điện thoại: 0964293499</div>
                </div>
                <div>Chỉnh sửa</div>
            </div>
        </div>
    );
}

export default Address;
