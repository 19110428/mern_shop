import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { _editUser } from '../../../../redux/user/userApi';

function LeftProfile(props) {
    let { currentUser } = props;
    const [fullname, setFullname] = useState(currentUser?.fullname);
    const [gender, setGender] = useState(currentUser?.gender);
    const [date, setDate] = useState(currentUser?.birthdate);

    const dispatch = useDispatch();
    const onChangeSex = (event) => {
        setGender(event.target.value);
    };
    if (gender === '') {
        setGender('Anh');
    }
    const handleChane = () => {
        const data = {
            fullname: fullname,
            gender: gender,
            birthdate: date,
        };
        _editUser(dispatch, data, currentUser?._id);
        toast.success('Update Success!', {});
    };
    return (
        <div>
            <div className="flex justify-between space-x-8 text-base">
                <button>
                    <img
                        src={currentUser.image}
                        className="w-24 h-24 align-text-bottom rounded-full cursor-pointer"
                        alt=""
                        onClick={() => {}}
                    />
                </button>

                <div className="w-3/4 space-y-6">
                    <div className="flex items-center">
                        <span htmlFor="fullname" className="font-semibold w-24">
                            Họ và tên:
                        </span>
                        <input
                            type="text"
                            id="fullname"
                            name="fullname"
                            value={fullname}
                            className="grow"
                            onChange={(e) => setFullname(e.target.value)}
                        />
                    </div>
                    <div className="flex">
                        <span className="font-semibold w-24">Giới tính:</span>
                        <div className="flex items-center space-x-4" onChange={onChangeSex}>
                            <input id="male" type="radio" name="sex" className="w-4 h-4" value="Anh" defaultChecked />
                            <span htmlFor="male">Anh</span>
                            <input
                                id="female"
                                type="radio"
                                name="sex"
                                className="w-4 h-4"
                                value="Chị"
                                checked={gender === 'Chị'}
                            />
                            <span htmlFor="female">Chị</span>
                            <input
                                id="other"
                                type="radio"
                                name="sex"
                                className="w-4 h-4"
                                value="Khác"
                                checked={gender === 'Khác'}
                            />
                            <span htmlFor="other">Khác</span>
                        </div>
                    </div>
                    <div className="flex items-center">
                        <span htmlFor="birthday" className="font-semibold w-24">
                            Ngày sinh:
                        </span>
                        <input
                            type="date"
                            id="birthday"
                            name="birthday"
                            min="1900-01-01"
                            max="2100-12-31"
                            value={date}
                            onChange={(e) => setDate(e.target.value)}
                        />
                    </div>
                </div>
            </div>

            <div className="text-white text-center mt-6">
                <button
                    className="bg-blue-500 border-2 hover:bg-blue-700 rounded-xl px-6 py-3 text-base"
                    onClick={handleChane}
                >
                    Lưu thay đổi
                </button>
            </div>
        </div>
    );
}

export default LeftProfile;
