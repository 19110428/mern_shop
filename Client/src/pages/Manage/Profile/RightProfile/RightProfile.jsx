import { MdEmail, MdPhone } from 'react-icons/md';
import { useSelector } from 'react-redux';

function RightProfile() {
    const currentUser = useSelector((state) => state.user?.currentUser);

    return (
        <div className="flex flex-col text-base space-y-4">
            <span className="font-semibold">Số điện thoại và Email</span>
            <div className="flex items-center w-full justify-between">
                <div className="flex space-x-4 items-center">
                    <MdPhone size={30} />
                    <div className="flex flex-col">
                        <span>Số điện thoại</span>
                        <span>{currentUser?.phone ? currentUser?.phone : 'Thêm số điện thoại'}</span>
                    </div>
                </div>
                <button className="text-blue-500 border-blue-500 border-2 hover:text-blue-700 hover:border-blue-700 rounded-xl px-3 py-2">
                    Cập nhật
                </button>
            </div>

            <div className="flex items-center w-full justify-between">
                <div className="flex space-x-4 items-center">
                    <MdEmail size={30} />
                    <div className="flex flex-col">
                        <span>Địa chỉ email</span>
                        <span>{currentUser?.username ? currentUser?.username : 'Thêm địa chỉ email'}</span>
                    </div>
                </div>
                <button className="text-blue-500 border-blue-500 border-2 hover:text-blue-700 hover:border-blue-700 rounded-xl px-3 py-2">
                    Cập nhật
                </button>
            </div>

            <span className="font-semibold ">Liên kết mạng xã hội</span>
            <div className="flex items-center w-full justify-between">
                <div className="flex">
                    <img
                        src="https://frontend.tikicdn.com/_desktop-next/static/img/account/facebook.png"
                        className="w-10 h-10 align-text-bottom"
                        alt=""
                    />
                    <div className="flex flex-col ml-4">
                        <span>Facebook</span>
                    </div>
                </div>
                <div className="text-blue-500 hover:text-blue-700">
                    {currentUser?.fbId ? (
                        <button className="border-blue-500 border-2 hover:border-blue-700 rounded-xl px-3 py-2 bg-[#f0f0f0]">
                            Đã Liên kết
                        </button>
                    ) : (
                        <button className="border-blue-500 border-2 hover:border-blue-700 rounded-xl px-3 py-2">
                            Liên kết
                        </button>
                    )}
                </div>
            </div>

            <div className="flex items-center w-full justify-between">
                <div className="flex">
                    <img
                        src="https://frontend.tikicdn.com/_desktop-next/static/img/account/google.png"
                        className="w-10 h-10 align-text-bottom"
                        alt=""
                    />
                    <div className="flex flex-col ml-4">
                        <span>Google</span>
                    </div>
                </div>
                <div className="text-blue-500 hover:text-blue-700">
                    {currentUser?.googleId ? (
                        <button className=" hover:border-blue-700 rounded-xl px-5 py-2 bg-[#f0f0f0]">
                            Đã Liên kết
                        </button>
                    ) : (
                        <button className="border-blue-500  border-2 hover:border-blue-700 rounded-xl px-3 py-2">
                            Liên kết
                        </button>
                    )}
                </div>
            </div>
        </div>
    );
}

export default RightProfile;
