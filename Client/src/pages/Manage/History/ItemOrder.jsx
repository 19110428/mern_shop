import { Link } from 'react-router-dom';
import { numberWithCommas } from '../../../utils';

function ItemOrder({ data }) {
    let sum = 0;

    return (
        <>
            <table className="w-full">
                <thead className="border-b text-center">
                    <tr>
                        <th className="text-gray-900 px-6 py-4 font-semibold">Hình ảnh</th>
                        <th className="text-gray-900 px-6 py-4 font-semibold">Tên</th>
                        <th className="text-gray-900 px-6 py-4 font-semibold">Giá</th>
                        <th className="text-gray-900 px-6 py-4 font-semibold">Số lượng</th>
                        <th className="text-gray-900 px-6 py-4 font-semibold">Tổng tiền</th>
                    </tr>
                </thead>
                <tbody className="text-center">
                    {data.order_items.map((item, index) => {
                        sum += item.price * (1 - item.discount) * item.quantity;
                        return (
                            <tr key={index} className="border-b">
                                <td className="text-gray-900 font-light px-6 py-4 flex justify-center">
                                    <img src={item.img} className="h-20 mr-4" alt="" />
                                </td>
                                <td className="text-gray-900 font-light px-6 py-4 text-left">
                                    <Link to={`/${item.category}/${item.slug}`} className="font-semibold">
                                        {item.title}
                                        <p className="font-normal">Hãng: {item.category}</p>
                                    </Link>
                                </td>
                                <td className="text-gray-900 px-6 py-4">
                                    <p className="text-red-500">
                                        {numberWithCommas(item.price * (1 - item.discount))}₫
                                    </p>
                                    <p className="line-through">{numberWithCommas(item.price)}₫</p>
                                </td>
                                <td className="text-gray-900 font-light px-6 py-4">
                                    <b className="">{item.quantity}</b>
                                </td>
                                <td className="text-gray-900 px-6 py-4 font-semibold">
                                    <p className="text-red-500">
                                        {numberWithCommas(item.price * (1 - item.discount) * item.quantity)}₫
                                    </p>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div className="text-right mt-8 font-bold">
                Tổng:
                <span className="text-red-500"> {numberWithCommas(sum)}đ</span>
            </div>
        </>
    );
}

export default ItemOrder;
