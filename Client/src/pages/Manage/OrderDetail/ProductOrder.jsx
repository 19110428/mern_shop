import { _editOrder, _getAllOrders, _getOrderDetail } from '../../../redux/order/ordersApi';
import { _newRating } from '../../../redux/rating/ratingsApi';
import ItemOrder from '../History/ItemOrder';
function ProductOrder({ detail }) {
    return (
        <div className="bg-white rounded-xl mt-6">
            <ItemOrder data={detail} />
        </div>
    );
}

export default ProductOrder;
