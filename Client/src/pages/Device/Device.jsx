import ListDevice from './ListDevice';
import { useState } from 'react';
import { useEffect } from 'react';
import DeviceBanner from './DeviceBanner';
const Device = (props) => {
    const [chose, setChose] = useState('');
    useEffect(() => {
        document.title = props.title;
    });
    return (
        <div className="flex flex-col space-y-6 my-6">
            <DeviceBanner />
            <ListDevice device={props.device} chose={chose}></ListDevice>
        </div>
    );
};
export default Device;
