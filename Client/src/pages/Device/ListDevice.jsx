
import { useEffect, useState } from 'react';
import { productService } from '../../services/product.service';
import { useSelector, useDispatch } from 'react-redux';
import { HandleFilter, getAllProductByCategory, getProductQuery, getProductQueryVer2 } from '../../redux/product/productsApi';
import handleData from '../../components/Filter/handleData';
import FilterDevice from './FilterDevice';
import { GridProduct } from '../../components/DisplayProduct';
import ListProduct from '../../components/ListProduct/ListProduct';

const dataSelected = ['% Giảm giá cao', 'Giá cao đến thấp', 'Giá thấp đến cao'];

const ListDevice = (props) => {
    const [chose, setChose] = useState(0);
    const [checked, setChecked] = useState([]);
    const [isOpen, setisOpen] = useState(false);
    const filter = useSelector((state) => state.products.filter.data);
    const dispatch = useDispatch();
    useEffect(() => {
        HandleFilter(dispatch, {});
        const obj = {["category"]: [props.device]};
        const temp = { ...obj};
        HandleFilter(dispatch, temp);
        const data = {
            category: props.device,
            brand: '',
            limit: '',
        };
        getProductQuery(dispatch, data);
        // getProductQueryVer2(dispatch, filter);
    }, []);
    useEffect(() => {
        
       if(isOpen)
        getProductQueryVer2(dispatch, filter);
    }, [filter]);
    const products = useSelector((state) => state?.products?.allProducts?.data);

    // const dataFilter = useSelector((state) => state?.products?.allProducts?.data);
    // // const filter = useSelector((state) => state.products.filter.data);

    // let dataAfter = dataFilter;
    // if (filter.length !== 0) {
    //     // dataAfter = handleData(dataFilter, filter);
    // }
    // // const [getDataFilter, setGetDataFilter] = useState(products);

    const handleClick = (index) => {
        setChose(index);
    };

    const handle = (bolen) => {
        setisOpen(bolen);
    };

   
    // const handleSort = () => {
    //     if (chose === 2) {
    //         setGetDataFilter([...getDataFilter].sort((a, b) => a.price - b.price));
    //     } else if (chose === 1) {
    //         setGetDataFilter([...getDataFilter].sort((a, b) => b.price - a.price));
    //     } else if (chose === 0) {
    //         setGetDataFilter([...getDataFilter].sort((a, b) => b.discount - a.discount));
    //     }
    // };
    const getDataFilter = products;
    console.log(products)
    return (
        <div className="flex max-w-[1200px] mx-auto space-x-6">
            <div className="w-1/6 bg-white p-4 border shadow-xl rounded-xl">
                <FilterDevice handle={handle} products={products} category={props.device} />
            </div>
            <div className="w-5/6">
                <div className="bg-white p-4 border shadow-xl rounded-xl mb-6 text-base">
                    <span className="font-bold text-xl">Sắp xếp: </span>
                    {dataSelected.map((item, index) => (
                        <button
                            className="py-2 px-5 mx-4 border-2 rounded-full hover:text-blue-700 hover:border-2 hover:border-blue-500"
                            onClick={() => {
                                handleClick(index);
                                handleSort();
                            }}
                            key={index}
                        >
                            {item}
                        </button>
                    ))}
                </div>

                <div className="bg-white p-4 border shadow-xl rounded-xl">
                    {isOpen === false ? (
                        <ListProduct products={products} isSlide={false}></ListProduct>
                    ) : (
                        <ListProduct products={products} isSlide={false}></ListProduct>
                    )}
                </div>
            </div>
        </div>
    );
};
export default ListDevice;
