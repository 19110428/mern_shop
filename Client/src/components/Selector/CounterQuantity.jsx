import { useCallback, useState } from 'react';
import { BiMinus, BiPlus } from 'react-icons/bi';
import { twMerge } from 'tailwind-merge';
function CounterQuantity({ value }) {
    const [quantity, setQuantity] = useState(value);

    const handleIncreaseQuantity = useCallback(() => {
        setQuantity((pre) => pre + 1);
    }, [setQuantity]);

    const handleDecreaseQuantity = useCallback(() => {
        quantity > 1 && setQuantity((pre) => pre - 1);
    }, [setQuantity, quantity]);

    return (
        <div className="flex h-10 text-lg">
            <span
                className={twMerge(
                    quantity === 1 ? 'text-gray-100' : 'text-blue-400',
                    'cursor-pointer p-4 flex items-center border-2',
                )}
                onClick={handleDecreaseQuantity}
            >
                <BiMinus />
            </span>
            <input value={quantity} className="text-center border-y-2 w-20" disabled />
            <span
                className="cursor-pointer p-4 text-blue-400 flex items-center border-2"
                onClick={handleIncreaseQuantity}
            >
                <BiPlus />
            </span>
        </div>
    );
}

export default CounterQuantity;
