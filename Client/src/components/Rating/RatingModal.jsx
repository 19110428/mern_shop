import { useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { _newRating } from '../../redux/rating/ratingsApi';
import RatingStar from './RatingStar';

const RatingModal = ({ data, handleCloseModal, fetchRatingProductData }) => {
    const initProductDetail = useSelector((state) => state.products?.productDetail?.data);
    const currentUser = useSelector((state) => state?.user?.currentUser);

    const dispatch = useDispatch();

    const [indexStar, setIndexStar] = useState(0);
    const [contentInput, setContentInput] = useState('');

    const handleSetIndexStar = useCallback(
        (index) => {
            setIndexStar(index);
        },
        [setIndexStar],
    );

    const handleSubmit = async () => {
        await _newRating(dispatch, {
            product_id: initProductDetail._id,
            product: initProductDetail,
            user_id: currentUser._id,
            user: currentUser,
            content: contentInput,
            star: indexStar,
        });
        handleCloseModal();
        fetchRatingProductData();
    };

    return (
        <>
            <div
                onClick={handleCloseModal}
                className="fixed inset-0 w-full h-full z-10 bg-gray-800 bg-opacity-80"
            ></div>
            <div className="fixed z-10 top-1/2 -translate-y-1/2 left-1/2 -translate-x-1/2 bg-white rounded-xl border shadow-xl text-gray-800 text-base h-fit p-6">
                <p className="text-2xl font-bold mb-6">Đánh giá</p>
                <div className="flex flex-col space-y-6 items-center">
                    <div className="font-bold text-xl flex items-center">
                        <div className="w-28">
                            <img src={data.img} alt="" />
                        </div>
                        <p>{data.title}</p>
                    </div>

                    <RatingStar indexStar={indexStar} handleSetIndexStar={handleSetIndexStar} />

                    <textarea
                        className="w-full rounded-xl border p-4 border-solid focus:outline-none focus:border-blue-500"
                        value={contentInput}
                        cols="30"
                        rows="5"
                        onChange={(e) => {
                            setContentInput(e.target.value);
                        }}
                        placeholder="Mời bạn chia sẻ thêm một số cảm nhận về sản phẩm ..."
                    />

                    <button onClick={handleSubmit} className="bg-blue-500 rounded-xl text-white px-6 py-2 text-lg">
                        Gửi đánh giá ngay
                    </button>
                    <div>Để đánh giá được duyệt, quý khách vui lòng tham khảo Quy định duyệt đánh giá</div>
                </div>
            </div>
        </>
    );
};

export default RatingModal;
