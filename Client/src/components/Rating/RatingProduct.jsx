import { useState, useCallback, useLayoutEffect } from 'react';
import { useSelector } from 'react-redux';
import { AiFillStar } from 'react-icons/ai';
import RatingModal from './RatingModal';
import Comment from './RatingComment';
import RatingTotal from './RatingTotal';
import { _getAllRatingProduct } from '../../redux/rating/ratingsApi';
import { toast } from 'react-toastify';

const images = [
    'https://cdn.tgdd.vn/comment/51982240/7230F870-6211-4567-A752-EAF2DCD900E0ITETG.jpeg',
    'https://cdn.tgdd.vn/comment/51690516/imageA70I9.jpg',
    'https://cdn.tgdd.vn/comment/51690511/imageJI9W2.jpg',
    'https://cdn.tgdd.vn/comment/51341098/IMG_UPLOAD_20220503_162905-20220503162907.jpg',
    'https://cdn.tgdd.vn/comment/51341098/IMG_UPLOAD_20220503_162905-20220503162907.jpg',
    'https://cdn.tgdd.vn/comment/51690511/imageJI9W2.jpg',
    'https://cdn.tgdd.vn/comment/51690516/imageA70I9.jpg',
    'https://cdn.tgdd.vn/comment/51982240/7230F870-6211-4567-A752-EAF2DCD900E0ITETG.jpeg',
];

const RatingProduct = () => {
    const initProductDetail = useSelector((state) => state.products?.productDetail?.data);

    const [isOpenModal, setIsOpenModal] = useState(false);
    const [ratingProduct, setRatingProduct] = useState();

    const handleUpdateDiscussRating = useCallback((dataRating) => {
        setRatingProduct(dataRating);
    }, []);

    const fetchRatingProductData = useCallback(async () => {
        try {
            const dataRating = await _getAllRatingProduct(initProductDetail._id);
            handleUpdateDiscussRating(dataRating);
        } catch (error) {
            toast.error(error.response.dataRating);
        }
    }, [toast, _getAllRatingProduct, initProductDetail._id]);

    useLayoutEffect(() => {
        fetchRatingProductData();
    }, [fetchRatingProductData]);

    const handleToggleModal = useCallback(() => {
        setIsOpenModal((pre) => !pre);
    }, [setIsOpenModal]);

    return (
        <div>
            <p className="text-2xl font-bold mb-4">Đánh giá {initProductDetail.title}</p>
            <div className="border rounded-lg px-4 w-fit py-4">
                <div className="flex items-center justify-center text-base space-x-4 py-4">
                    <RatingTotal ratingProduct={ratingProduct} process />
                    <div className="grid grid-cols-4 gap-4">
                        {images.map((image, index) => {
                            return (
                                <div className="h-16 w-16 rounded-xl overflow-hidden" key={index}>
                                    <img src={image} alt="" className="h-full w-full object-cover" />
                                </div>
                            );
                        })}
                    </div>
                </div>

                <Comment ratingProduct={ratingProduct} fetchRatingProductData={fetchRatingProductData} />

                <div className="flex space-x-4 w-full text-white text-lg">
                    <button
                        className="bg-blue-500 rounded-lg w-1/2 py-2 flex items-center justify-center space-x-2"
                        onClick={handleToggleModal}
                    >
                        <AiFillStar size={20} />
                        <span>Viết đánh giá</span>
                    </button>

                    <button className="bg-blue-500 rounded-lg w-1/2 py-2">Xem đánh giá</button>
                </div>
            </div>

            {isOpenModal && (
                <RatingModal
                    data={initProductDetail}
                    handleCloseModal={handleToggleModal}
                    fetchRatingProductData={fetchRatingProductData}
                />
            )}
        </div>
    );
};

export default RatingProduct;
