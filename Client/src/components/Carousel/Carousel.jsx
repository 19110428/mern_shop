import { Mousewheel, Navigation } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import '../../assets/swiper.scss';
import { Link } from 'react-router-dom';
import ProductCard from '../DisplayProduct/ProductCard';

const Carousel = ({ data, slidesPerView = 5, spaceBetween = 16, isProduct = true }) => {
    return (
        <div className="w-full">
            <Swiper
                slidesPerView={slidesPerView}
                spaceBetween={spaceBetween}
                navigation
                mousewheel={{
                    sensitivity: 0,
                }}
                loop={true}
                modules={[Navigation, Mousewheel]}
            >
                {data.map((item, index) => (
                    <SwiperSlide key={index}>
                        {isProduct ? (
                            <div className="w-full bg-white h-[440px] p-4 rounded-xl">
                                <ProductCard data={item} />
                            </div>
                        ) : (
                            <Link to="/">
                                <img src={item} alt="" className="w-full h-[200px] object-fill rounded-xl" />
                            </Link>
                        )}
                    </SwiperSlide>
                ))}
            </Swiper>
        </div>
    );
};

export default Carousel;
