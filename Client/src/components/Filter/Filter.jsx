import FilterItem from './FilterItem';
import { useEffect } from 'react';
import { HandleFilter } from '../../redux/product/productsApi';
import { useDispatch } from 'react-redux';
export default function Filter({ handle, data }) {
    const dispatch = useDispatch();
    useEffect(() => {
        HandleFilter(dispatch, {});
    }, []);
    return (
        <div>
            {data.map((data, index) => {
                return (
                    <div className="mt-4" key={index}>
                        <FilterItem data={data} key={data.id} handle={handle} />
                    </div>
                );
            })}
        </div>
    );
}
