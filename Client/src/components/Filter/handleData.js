const handleData = (data, filter) => {
    let gia = 0;
    // let price = 0;
    // Lọc qua từng product trong mảng
    const dataAfter = data?.filter((e) => {
        if (e.parameter) {
            // Lọc qua từng cặp key value trong parameter
            for (const [key, value] of Object?.entries(e?.parameter)) {
                // Lọc qua từng phần tử trong
                const checkTitle = filter.some((element) => {
                    let keyM = Object.keys(element);
                    let valueM = Object.values(element);

                    if (keyM == 'Giá') {
                        gia = valueM[0];
                        let temp = [0, 1000000000];
                        if (gia.search('Dưới') != -1) {
                            let split_str = gia.match(/[0-9]+/)[0];
                            temp[1] = Number(split_str + '000000');
                        } else if (gia.search('Trên') != -1) {
                            let split_str = gia.match(/[0-9]+/)[0];
                            temp[0] = Number(split_str + '000000');
                        } else if (gia.search('Từ') != -1) {
                            let split_str = gia.match(/[0-9]+/)[0];
                            temp[0] = Number(split_str + '000000');
                            const res = gia.match(/\d+/g)?.[1];
                            temp[1] = Number(res + '000000');
                        }
                        return e.price >= temp[0] && e.price <= temp[1];
                    }
                    if (keyM == 'Loại') {
                        return e.brand == valueM;
                    }
                    if (keyM == 'RAM') {
                        return (e.parameter.RAM + '').includes(valueM + '');
                    }
                    if (keyM == 'SALE') {
                        if (valueM == 'Giảm giá') return e.discount !== 0;
                        if (valueM == 'Góp 0%') return e.promotion.includes('Trả góp 0%');
                        if (valueM == 'Độc quyền') return e.docquyen === true;
                        if (valueM == 'Mới') return e.new === true;
                    }
                    return false;
                });
                if (checkTitle) return e;
            }
        }
    });
    return dataAfter;
};

export default handleData;
