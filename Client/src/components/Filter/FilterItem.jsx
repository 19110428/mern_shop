import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { HandleFilter } from '../../redux/product/productsApi';
import BoxSort from '../BoxSort/BoxSort';
function FilterItem({ data, handle }) {
    const filter = useSelector((state) => state.products.filter.data); // Lấy tất cả
    const [arrayTemp, setArrayTemp] = useState([]); // Lấy giá trị trong một khung
    const dispatch = useDispatch();

    const handleAppear = (e) => {
        // Lấy keyword
        let newKeyword = {
            [data.query]: [e.target.innerText],
        };
      

        // Lấy element theo tên
        const element = Array.from(document.getElementsByName(data.title));

    
        //------------------------------------
        // kiểm tra key có tồn tại chưa trong filter chưa
        const checkKeyFilter = ()=>  {
            // let value = Object.values(element);
            // let key = Object.keys(element);
            // value === e.target.innerText 
            if(Object.keys(filter).length === 0)
                 return false;
            for (const [key, value] of Object?.entries(filter)) {
                if ( key === data.query) return true;
        };};
        var checkKey= checkKeyFilter();

        // console.log("aaaaaaaaaaaaaaaaaaaaaaaaaa",);
        
        if (checkKey) {
        
            // const temp = filter.filter((element) => {
            //     let value = Object.values(element);
            //     let key = Object.keys(element);

            //     if (key[0] === data.title && value[0] === e.target.innerText);
            //     else {
            //         return element;
            //     }
            // });

            // Nếu đã có sẵn keyword
            if( filter[data.query].includes(e.target.innerText)){
                // Xóa đi
                    const value =filter[data.query].filter((element)=>{
                        return element!= e.target.innerText
                    })
                    if(value.length==0){
                        var temp = {...filter};
                         delete temp[data.query]
                        
                         HandleFilter(dispatch, temp);
                    }
                    else{
                        const obj = {[data.query]: value};
                        const temp = {...filter, ...obj};
                        HandleFilter(dispatch, temp);
                    }
                    
            }
            else{
                // Thêm vào
                const array = [...filter[data.query],e.target.innerText];
                const obj = {[data.query]: array};
                    const temp = {...filter, ...obj};
                    HandleFilter(dispatch, temp);
            }

            // HandleFilter(dispatch, temp);
        }
         // Nếu chưa có keyWord thì thêm vô
        else {
            
            const temp = {...filter, ...newKeyword};
            HandleFilter(dispatch, temp);
        }
        // Hiện nút filter
        handle(true);
    };
    const handleClick = (index) => {
        setChose(index);
    };
    const [selected, setSelected] = useState(false);
    const [chose, setChose] = useState(0);
    const [checked, setChecked] = useState([]);
    return (
        <ul>
            <li onClick={handleAppear}>
                <BoxSort
                    data={data.detail}
                    onclick={handleClick}
                    selected={selected}
                    setSelected={setSelected}
                    chose={chose}
                    checked={checked}
                    setChecked={setChecked}
                    category={data.title}
                ></BoxSort>
            </li>
        </ul>
    );
}
export default FilterItem;
