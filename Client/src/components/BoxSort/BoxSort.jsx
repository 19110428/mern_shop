import styles from './boxsort.module.scss';
import { twMerge } from 'tailwind-merge';

const BoxSort = (props) => {
    const data = props.data;
    const handleChecked = (id) => {
        props.setChecked((prev) => {
            const isCheck = props.checked.includes(id);
            if (isCheck) {
                return props.checked.filter((item) => item !== id);
            } else {
                return [...prev, id];
            }
        });
    };
    return (
        <div className={styles.boxsort}>
            <div className={styles.boxsort__body}>
                <p className={styles.category}>{props.category}</p>
                <ul className={styles.checkbox}>
                    {data.map((item, index) => (
                        <li className={styles.checkboxItem} key={index} onClick={() => handleChecked(item)}>
                            <span
                                className={twMerge(styles.tickCheckbox, props.checked.includes(item) && styles.active)}
                            ></span>
                            <span>{item}</span>
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    );
};
export default BoxSort;
