import ProductCard from '../ProductCard/ProductCard.jsx';

import { useState } from 'react';

const ListProduct = (props) => {
    let { products } = props;
    const [isExpand, setIsExpand] = useState(false);

    return (
        <>
            <div className="grid grid-cols-4 gap-6">
                {isExpand
                    ? products.map((product, index) => <ProductCard key={index} {...product} />)
                    : products.slice(0, 8).map((product, index) => <ProductCard key={index} {...product} />)}
            </div>
            <div className="flex justify-center mt-6">
                {isExpand ? (
                    <button
                        className="outline-none text-base border bg-blue-500 text-white font-semibold px-16 py-3 rounded-xl mx-auto"
                        onClick={() => {
                            setIsExpand(false);
                            window.scrollTo({
                                top: 0,
                                behavior: 'smooth',
                            });
                        }}
                    >
                        Thu nhỏ
                    </button>
                ) : (
                    <button
                        className="outline-none text-base border bg-blue-500 text-white font-semibold px-16 py-3 rounded-xl"
                        onClick={() => {
                            setIsExpand(true);
                        }}
                    >
                        Xem tất cả sản phẩm
                    </button>
                )}
            </div>
        </>
    );
};
export default ListProduct;
