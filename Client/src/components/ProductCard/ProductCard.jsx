import { Link } from 'react-router-dom';
import { AiFillStar } from 'react-icons/ai';
import { useState } from 'react';
import numberWithCommas from '../../utils/numberWithCommas';
import { productHistory } from '~/helpers/localStorage';
import { useDispatch } from 'react-redux';
import { getProductDetailApi } from '~/redux/product/productsApi';
import { twMerge } from 'tailwind-merge';

function ProductCard(props) {
    const dispatch = useDispatch();
    const handleClickDisable = (e) => {
        e.stopPropagation();
        e.nativeEvent.stopImmediatePropagation();
    };
    const [checked, setChecked] = useState(0);

    const handleProductClick = () => {
        productHistory.saveItems(props);
        getProductDetailApi(dispatch, props.slug);
    };
    return (
        <Link to={`/${props.category}/${props.slug}`} onClick={handleProductClick}>
            <div className="flex flex-col space-y-2 w-[200px]">
                <div
                    className={twMerge(
                        'absolute z-10 w-fit text-xs rounded-md bg-gradient-to-r from-[#1746a2] to-[#5f9df7] text-white px-2 py-1',
                        props.promotion == '' && 'invisible',
                    )}
                >
                    <p>{props.promotion}</p>
                </div>
                <div className="w-full relative object-contain">
                    <img src={props.img}></img>
                    {props.docquyen && (
                        <img
                            className="w-10 h-w-10 absolute bottom-0 left-0"
                            src="https://cdn.tgdd.vn/ValueIcons/Label_01-05.png"
                        ></img>
                    )}
                    {props.baohanh === '18T' && (
                        <img
                            className="w-10 h-w-10 absolute bottom-0 left-0"
                            src="https://cdn.tgdd.vn/ValueIcons/Label_01-02.png"
                        ></img>
                    )}
                </div>
                {props.tag && (
                    <p className="text-white font-medium text-xs uppercase text-center w-[160px] rounded-3xl p-2 bg-[#db2562] mx-autơ">
                        {props.tag}
                    </p>
                )}
                <span className="font-semibold text-base">{props.title}</span>
                {props.category === 'Phone' || props.category === 'Tablet' ? (
                    <div>
                        {props.parameter.RAM?.map((item, index) => (
                            <span
                                key={index}
                                className={twMerge(
                                    'text-[#2f80ed] border border-solid border-[#2f80ed] rounded-md text-xs p-1 mr-2',
                                )}
                                onClick={(e) => {
                                    handleClickDisable(e);
                                    setChecked(index);
                                }}
                            >
                                RAM {item}
                            </span>
                        ))}
                    </div>
                ) : (
                    <div>
                        {props?.parameter?.RAM && (
                            <span className="text-[#2f80ed] border border-solid border-[#2f80ed] rounded-md text-xs p-1">
                                {'RAM ' + props?.parameter.RAM.indexOf(',') === -1
                                    ? props?.parameter.RAM
                                    : props?.parameter.RAM.slice(0, props?.parameter.RAM.indexOf(','))}
                            </span>
                        )}
                    </div>
                )}
                <div className="text-[#d0021c] flex space-x-1 font-medium text-sm items-center">
                    <div className="font-bold text-base">{numberWithCommas(props?.price * (1 - props?.discount))}</div>
                    <div className="line-through text-gray-800">{numberWithCommas(props?.price)}</div>
                    <div className="">-{props?.discount * 100}%</div>
                </div>
                <div className="flex items-center text-yellow-400 font-bold space-x-2">
                    <div>{props.star}</div>
                    <AiFillStar />
                    <span className="text-gray-700 font-normal">({props.totalVote})</span>
                </div>
            </div>
        </Link>
    );
}

export default ProductCard;
