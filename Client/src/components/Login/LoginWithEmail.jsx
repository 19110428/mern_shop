import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { useState } from 'react';
import { _loginPass, _verifyPhone } from '../../redux/user/userApi';
import { BsChevronLeft } from 'react-icons/bs';
import { toast } from 'react-toastify';

const LoginWithEmail = ({ handleToggleLogin, handleCloseModal }) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleLogin = (e) => {
        e.preventDefault();
        const newUser = {
            username: email,
            password: password,
        };
        _loginPass(newUser, dispatch, navigate)
            .then(() => {
                toast.success('Đăng nhập thành công');
                handleCloseModal();
            })
            .catch((error) => toast.error(error.response.data));
    };

    return (
        <>
            <div className="text-left cursor-pointer">
                <BsChevronLeft onClick={handleToggleLogin} />

                <p className="text-2xl font-semibold">Đăng nhập bằng email</p>
                <p>Nhập email và mật khẩu tài khoản KPShop</p>
            </div>
            <div>
                <input
                    type="text"
                    className="form-control block w-full px-6 py-3 font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded-xl"
                    placeholder="Email"
                    onChange={(e) => setEmail(e.target.value)}
                />
            </div>
            <div>
                <input
                    type="password"
                    className="form-control block w-full px-6 py-3 font-normal text-gray-700 bg-white border border-solid border-gray-300 rounded-xl"
                    placeholder="Mật khẩu"
                    onChange={(e) => setPassword(e.target.value)}
                />
            </div>
            <button
                onClick={handleLogin}
                className="py-3 w-full bg-red-600 text-white font-medium text-lg rounded-xl hover:bg-red-700"
            >
                Đăng nhập
            </button>
            <div>
                <a onClick={() => {}} className="text-blue-500 hover:text-blue-700">
                    Quên mật khẩu
                </a>
                <p className="font-semibold">
                    Bạn mới biết đến KPShop?
                    <a onClick={handleToggleLogin} className="text-blue-500 hover:text-blue-700 ml-2 transition">
                        Tạo tài khoản
                    </a>
                </p>
            </div>
        </>
    );
};

export default LoginWithEmail;
