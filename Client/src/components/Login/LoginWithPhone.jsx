import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { useCallback, useState } from 'react';
import { _loginPass, _loginPhone, _verifyPhone } from '../../redux/user/userApi';
import { BsChevronLeft, BsFacebook } from 'react-icons/bs';
import { FcGoogle } from 'react-icons/fc';
import { toast } from 'react-toastify';

const LoginWithPhone = ({ handleToggleLogin, handleCloseModal }) => {
    const [phone, setPhone] = useState('');
    const [otp, setOtp] = useState('');

    const [isContinueLogin, setIsContinueLogin] = useState(false);

    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleContinueLogin = () => {
        setIsContinueLogin((pre) => !pre);
    };

    const handleLogin = useCallback(
        (e) => {
            e.preventDefault();

            _verifyPhone({ phone, otp }, dispatch, navigate)
                .then(() => {
                    toast.success('Đăng nhập thành công');
                    handleCloseModal();
                })
                .catch((error) => {
                    toast.error(error.response.data);
                });
        },
        [_verifyPhone, phone, otp, dispatch, navigate, toast, handleCloseModal],
    );

    const handlePhone = useCallback(() => {
        _loginPhone(phone, dispatch, navigate)
            .then(() => {
                handleContinueLogin();
            })
            .catch((error) => {
                toast.error(error.response.data);
            });
    }, [_loginPhone, phone, dispatch, navigate, setIsContinueLogin, toast]);

    const handleGoolge = () => {
        window.open('http://localhost:8000/auth/google', '_self');
    };

    return (
        <>
            {!isContinueLogin ? (
                <>
                    <div className="flex flex-col space-y-6">
                        <div className="text-left">
                            <p className="font-bold text-2xl">Xin chào</p>
                            <p>Đăng nhập hoặc Tạo tài khoản</p>
                        </div>
                        <div>
                            <input
                                className="w-full px-6 py-3 font-normal text-gray-700 bg-white border border-solid border-gray-300 rounded"
                                placeholder="Số điện thoại"
                                onChange={(e) => setPhone(e.target.value)}
                            />
                        </div>
                        <button
                            onClick={(e) => {
                                handlePhone(e);
                            }}
                            className="py-3 w-full bg-red-600 text-white font-medium text-lg rounded-xl hover:bg-red-700"
                        >
                            Tiếp tục
                        </button>
                        <a
                            onClick={() => {
                                handleToggleLogin();
                            }}
                            className="text-blue-500 hover:text-blue-700"
                        >
                            Đăng nhập bằng Email
                        </a>
                    </div>
                    <div>
                        <p className="text-center">Hoặc tiếp tục bằng</p>
                        <div className="flex flex-row items-center justify-center">
                            <p className="mr-6">Sign in with</p>
                            <button className="text-blue-500">
                                <BsFacebook size={30} />
                            </button>
                            <button onClick={handleGoolge}>
                                <FcGoogle size={36} />
                            </button>
                        </div>
                    </div>
                </>
            ) : (
                <>
                    <div className="text-left cursor-pointer">
                        <BsChevronLeft onClick={handleContinueLogin} />
                    </div>
                    <div className="text-left">
                        <p className="text-2xl font-semibold">Nhập mã xác minh</p>
                        <p>Nhập mã xác minh gồm 6 số vừa được gửi đến {phone}</p>
                    </div>

                    <div>
                        <input
                            className="form-control block w-full px-6 py-3 font-normal text-gray-700 bg-white border border-solid border-gray-300 rounded-xl"
                            placeholder="OTP"
                            onChange={(e) => setOtp(e.target.value)}
                        />
                    </div>
                    <button
                        onClick={handleLogin}
                        className="py-3 text-lg w-full bg-red-600 text-white font-medium rounded-xl hover:bg-red-700"
                    >
                        Đăng nhập
                    </button>
                    <div>
                        Chưa nhận được.
                        <a onClick={() => {}} className="text-blue-500 hover:text-blue-700 ml-2">
                            Gửi lại
                        </a>
                    </div>
                </>
            )}
        </>
    );
};

export default LoginWithPhone;
