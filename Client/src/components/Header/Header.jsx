import { Link, useNavigate } from 'react-router-dom';
import SearchInput from './SearchInput';
import HeaderTheme from './HeaderTheme';
import HeaderMenu from './HeaderMenu';
import { useEffect } from 'react';
import { _getSuccess } from '../../redux/user/userApi';
import { useDispatch } from 'react-redux';
function Header() {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    useEffect(() => {
        _getSuccess(dispatch, navigate);
    }, []);
    return (
        <header className="bg-[#1a94ff] text-base">
            <div className="flex justify-center h-24">
                <div className="w-[1200px] h-24 flex justify-between items-center space-x-4">
                    <Link to="/">
                        <img src="../../assets/logo.png" className="w-24 h-12" />
                    </Link>
                    <div className="flex-grow">
                        <SearchInput />
                        <HeaderMenu />
                    </div>
                    <HeaderTheme />
                </div>
            </div>
        </header>
    );
}

export default Header;
