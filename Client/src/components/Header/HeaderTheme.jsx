import { Fragment } from 'react';
import { Transition, Menu } from '@headlessui/react';
import CartButton from './CartButton';
import { Link, useNavigate } from 'react-router-dom';
import Login from '../Login/Login';
import { MdLogout, MdPerson, MdOutlineListAlt } from 'react-icons/md';
import { useDispatch, useSelector } from 'react-redux';
import { _getSuccess, _logout } from '../../redux/user/userApi';
import { twMerge } from 'tailwind-merge';
import { RxAvatar } from 'react-icons/rx';

export default function HeaderTheme() {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleLogOut = async () => {
        _logout(dispatch, navigate);
    };
    const currentUser = useSelector((state) => state.user?.currentUser);

    return (
        <>
            {currentUser ? (
                <span className="flex items-center space-x-3 text-white text-sm font-semibold hover:text-gray-200">
                    <Menu as="div" className="relative inline-block">
                        <div className="flex items-center">
                            <img src={currentUser?.image} className="rounded-full w-[40px] h-[40px]"></img>
                            <Menu.Button className="text-left ml-4">
                                <h3 className="text-white">Tài khoản</h3>
                                <div className="flex justify-start items-center mt-1 space-x-2">
                                    <b>{currentUser?.fullname || currentUser?.phone}</b>
                                    <i className="fa fa-sort-down -mt-2"></i>
                                </div>
                            </Menu.Button>
                        </div>

                        <Transition
                            as={Fragment}
                            enter="transition ease-out duration-100"
                            enterFrom="transform opacity-0 scale-95"
                            enterTo="transform opacity-100 scale-100"
                            leave="transition ease-in duration-75"
                            leaveFrom="transform opacity-100 scale-100"
                            leaveTo="transform opacity-0 scale-95"
                        >
                            <Menu.Items className="absolute right-0 z-10 mt-2 w-[200px] rounded-md bg-white border-2">
                                <Menu.Item>
                                    {({ active }) => (
                                        <Link
                                            to="/account"
                                            className={twMerge(
                                                active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                                                'flex items-center space-x-2 px-4 py-3',
                                            )}
                                        >
                                            <MdPerson size={24} />
                                            <span>Thông tin cá nhân</span>
                                        </Link>
                                    )}
                                </Menu.Item>
                                <Menu.Item>
                                    {({ active }) => (
                                        <Link
                                            to="/history"
                                            className={twMerge(
                                                active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                                                'flex items-center space-x-2 px-4 py-3',
                                            )}
                                        >
                                            <MdOutlineListAlt size={24} />
                                            <span>Quản lý đơn hàng</span>
                                        </Link>
                                    )}
                                </Menu.Item>
                                <Menu.Item>
                                    {({ active }) => (
                                        <a
                                            className={twMerge(
                                                active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                                                'flex items-center space-x-2 px-4 py-3',
                                            )}
                                            onClick={handleLogOut}
                                        >
                                            <MdLogout size={24} />
                                            <span>Đăng xuất</span>
                                        </a>
                                    )}
                                </Menu.Item>
                            </Menu.Items>
                        </Transition>
                    </Menu>
                </span>
            ) : (
                <span className="flex items-center space-x-2 text-white text-sm font-semibold hover:text-gray-200">
                    {currentUser ? (
                        <span className="flex items-center space-x-2 text-white text-sm font-semibold hover:text-gray-100">
                            <img src={currentUser?.image}></img>
                            <button data-bs-toggle="modal" data-bs-target="#exampleModal">
                                <h3>Tài khoản</h3>
                                <div className="flex justify-start items-center space-x-2">
                                    <span>{currentUser?.fullname || currentUser?.phone}</span>
                                    <i className="fa fa-sort-down -mt-2"></i>
                                </div>
                            </button>
                            <Login />
                        </span>
                    ) : (
                        <span className="flex items-center space-x-2 text-white text-sm font-semibold hover:text-gray-100">
                            <RxAvatar size={30} />
                            <button data-bs-toggle="modal" data-bs-target="#exampleModal">
                                <h3 className="text-white">Đăng nhập/ Đăng ký</h3>
                                <div className="flex justify-start items-center space-x-2">
                                    <span>Tài khoản</span> <i className="fa fa-sort-down -mt-2"></i>
                                </div>
                            </button>
                            <Login />
                        </span>
                    )}
                </span>
            )}

            <div className="flex text-white text-sm font-semibold hover:text-gray-200 styleCart">
                <Link to="/cart" className="hover:text-gray-200">
                    <CartButton />
                </Link>
            </div>
        </>
    );
}
