
import Tippy from '@tippyjs/react';
import 'tippy.js/dist/tippy.css';
import { useState, useEffect, useRef } from 'react';
import { getResult } from '../../redux/search/searchApi';
import { useDispatch, useSelector } from 'react-redux';
import numberWithCommas from '../../utils/numberWithCommas';
import useDebound from './../../hooks/useDebound';
import { Link, useNavigate } from 'react-router-dom';
import { BiSearchAlt2 } from 'react-icons/bi';

function SearchInput(props) {
    const [value, setValue] = useState('');
    const [showResult, setShowResult] = useState(false);
    const [checknull, setChecknull] = useState(false);
    const dispatch = useDispatch();
    let keySearch = useDebound(value, 500);
    const inputRef = useRef();
    const navigate = useNavigate();
    useEffect(() => {
        if (keySearch.length === 0) {
            // removeResult(dispatch)
            setShowResult(false);
            return;
        }
        setShowResult(true);
        setChecknull(false);
        console.log('aaaaaaaaaaaaa')
        getResult(dispatch, keySearch);
    }, [keySearch]);
    let resultSearch = useSelector((state) => state.search.search.data);

    const handleText = (e) => {
        setValue(e.target.value);
        if (value.length < 1) {
            setShowResult(false);
        } else {
            setShowResult(true);
        }
    };
    const hideResultSearch = () => {
        setValue('');
        setShowResult(false);
    };
    const config = {
        dienthoai: 'dienthoai',
        điệnthoại: 'dienthoai',
        maytinhbang: 'tablet',
        máytínhbảng: 'tablet',
        tablet: 'tablet',
        phukien: 'accessory',
        phụkiện: 'accessory',
        accessory: 'accessory',
        dongho: 'watch',
        đồnghồ: 'watch',
        watch: 'watch',
        laptop: 'laptop',
        donghothongminh: 'smartwatch',
        đồnghồthôngminh: 'smartwatch',
        smartwatch: 'smartwatch',
    };

    function match(input, obj) {
        var matched = Object.keys(obj).find((key) => input.toLowerCase().search(key) > -1);
        return obj[matched] || null;
    }

    const hanleClickSearch = (e) => {
        e.preventDefault();
        
        let link= value;
        let getValue = value.replace(/\s/g, '');
        let url = match(getValue, config);
        let uri = encodeURI(link)
        if (url === null) {
            getResult(dispatch, value);
            hideResultSearch();
            navigate(`tim-kiem/${uri}`);
        }
        console.log(value);
        if (url !== null) {
            hideResultSearch();
            navigate(value);
            return;
        }
    };

    return (
        <div className="">
          
                
            <form className="relative outline-none" onSubmit={hanleClickSearch}>
                <div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none outline-none">
                    <BiSearchAlt2 size={24} inputRef={inputRef} />
                </div>
                <Tippy
                    interactive
                    visible={showResult && resultSearch.length > 0}
                    duration={50}
                    onClickOutside={() => setShowResult(false)}
                    placement="bottom"
                    content={
                        <div className="bg-white min-h-auto max-h-[265px] rounded-lg flex flex-col gap-2 z-10 overflow-y-auto">
                            {resultSearch.length === 0 && <h2>Không có sản phẩm trong hệ thống chúng tôi</h2>}

                            {resultSearch.map((item, index) => (
                                <Link
                                    key={index}
                                    to={`${item.category}/${item.slug}`}
                                    className="flex items-center justify-between gap-5 p-3"
                                    onClick={hideResultSearch}
                                >
                                    <div className="w-10 h-10 rounded-lg">
                                        <img src={item.img}></img>
                                    </div>
                                    <div className="flex flex-col w-full text-xs">
                                        <div className="text-lg font-semibold align-middle text-black">
                                            {item.title}
                                        </div>
                                        <div className="flex gap-3 items-end">
                                            <div className="text-red-400">
                                                {numberWithCommas(item.price * (1 - item.discount))}đ
                                            </div>
                                            <span className="line-through">{numberWithCommas(item.price)}đ</span>
                                        </div>
                                        <div className="text-gray-500">Quà 100.000đ</div>
                                    </div>
                                </Link>
                            ))}
                        </div>
                    }
                >
                    <input
                        // type="search"
                        id="default-search"
                        className="block p-2 pl-16 w-full text-base text-gray-900 rounded-lg outline-none border-none"
                        placeholder="Bạn tìm gì..."
                        required
                        autoComplete="off"
                        value={value}
                        onChange={handleText}
                        onFocus={() => {
                            setShowResult(true);
                        }}
                    />
                </Tippy>
                <button
                    type="submit"
                    className="absolute right-1 bottom-1 text-white bg-[#1a94ff] hover:bg-[#0d5cb6] font-sm rounded-lg text-base px-4 py-1"
                    onClick={(e)=>hanleClickSearch()}
                >
                    Tìm kiếm
                </button>
            </form>
        </div>

        

        //             { resultSearch.map(item=>(
        //                   <Link to={item.url} className='flex items-center justify-between gap-5 p-3' onClick={hideResultSearch}>
        //                      <div className='w-[45px] h-[45px] rounded-lg'>
        //                          <img src={item.img}></img>
        //                      </div>
        //                      <div className='flex flex-col w-full'>
        //                          <div className='text-[13px] font-semibold align-middle '>{item.title}</div>
        //                          <div className='flex gap-3 items-end'>
        //                              <div className='text-[12px] text-red-400'>{numberWithCommas(item.price*(1-item.discount))}đ</div>
        //                              <span className='line-through text-[11px] '>{numberWithCommas(item.price)}đ</span>
        //                          </div>
        //                          <div className='text-[11px]'>Quà 100.000đ</div>
        //                      </div>
        //                    </Link>
        //             ))}
        //        </div>
        //  )}
    );
}

export default SearchInput;
