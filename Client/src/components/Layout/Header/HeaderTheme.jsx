import { useCallback, useEffect, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Login from '../../Login/Login';
import { MdLogout, MdPerson, MdOutlineListAlt } from 'react-icons/md';
import { useDispatch, useSelector } from 'react-redux';
import { _getSuccess, _logout } from '../../../redux/user/userApi';
import { RxAvatar } from 'react-icons/rx';
import { IoMdArrowDropdown } from 'react-icons/io';

const dropdownOption = [
    {
        href: '/account',
        name: 'Thông tin cá nhân',
        icon: MdPerson,
    },
    {
        href: '/history',
        name: 'Quản lý đơn hàng',
        icon: MdOutlineListAlt,
    },
    {
        href: '',
        name: 'Đăng xuất',
        icon: MdLogout,
    },
];

export default function HeaderTheme() {
    const currentUser = useSelector((state) => state.user?.currentUser);
    const ref = useRef(null);

    const dispatch = useDispatch();
    const navigate = useNavigate();

    const [isOpenModal, setIsOpenModal] = useState(false);
    const [isOpenDropdown, setIsOpenDropdown] = useState(false);

    const handleToggleShowModal = useCallback(() => {
        setIsOpenModal((pre) => !pre);
    }, [setIsOpenModal]);

    const handleToggleShowDropdown = useCallback(() => {
        setIsOpenDropdown((pre) => !pre);
    }, [setIsOpenDropdown]);

    const handleDropdownModal = useCallback(() => {
        setIsOpenDropdown(false);
    }, [setIsOpenModal]);

    const handleClickDropdown = useCallback(
        (event, href) => {
            event.preventDefault();
            event.stopPropagation();

            handleToggleShowDropdown();
            if (href) {
                navigate(href);
                return;
            }
            _logout(dispatch, navigate);
        },
        [navigate, _logout, dispatch],
    );

    useEffect(() => {
        const handleClickOutside = (event) => {
            if (ref.current && !ref.current.contains(event.target)) {
                handleDropdownModal();
            }
        };
        document.addEventListener('click', handleClickOutside);
        return () => {
            document.removeEventListener('click', handleClickOutside);
        };
    }, [handleDropdownModal, ref]);

    return (
        <>
            {currentUser ? (
                <span ref={ref} className="relative flex items-center space-x-3 text-white text-sm font-semibold">
                    <img src={currentUser?.image} className="rounded-full w-[40px] h-[40px]"></img>
                    <button className="text-left" onClick={handleToggleShowDropdown}>
                        <span>Tài khoản</span>
                        <span className="flex justify-start items-center space-x-2">
                            <div>{currentUser?.fullname || currentUser?.phone}</div>
                            <div className="translate-y-0.5">
                                <IoMdArrowDropdown size={20} />
                            </div>
                        </span>
                    </button>
                    {isOpenDropdown && (
                        <div className="absolute top-10 right-0 z-10 mt-2 rounded-md bg-white border-2 py-2">
                            {dropdownOption.map((dropdown) => (
                                <button
                                    className="text-gray-700 flex items-center space-x-2 px-4 py-3 hover:bg-gray-100 w-[200px]"
                                    onClick={(event) => {
                                        handleClickDropdown(event, dropdown.href);
                                    }}
                                >
                                    <dropdown.icon size={24} />
                                    <span>{dropdown.name}</span>
                                </button>
                            ))}
                        </div>
                    )}
                </span>
            ) : (
                <span className="flex items-center text-white text-sm font-semibold hover:text-gray-100">
                    <RxAvatar size={30} />
                    <button className="ml-2" onClick={handleToggleShowModal}>
                        <h3 className="text-white">Đăng nhập/ Đăng ký</h3>
                        <div className="flex justify-start items-center space-x-2">
                            <span>Tài khoản</span>
                            <div className="translate-y-0.5">
                                <IoMdArrowDropdown size={20} />
                            </div>
                        </div>
                    </button>

                    {isOpenModal && <Login handleCloseModal={handleToggleShowModal} />}
                </span>
            )}
        </>
    );
}
