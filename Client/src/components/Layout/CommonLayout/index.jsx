import Header from '../../Header';
import Footer from '../../Footer';
import { Outlet } from 'react-router-dom';
function CommonLayout() {
    return (
        <>
            <Header />
            <div>
                <Outlet />
            </div>
            <Footer />
        </>
    );
}

export default CommonLayout;
