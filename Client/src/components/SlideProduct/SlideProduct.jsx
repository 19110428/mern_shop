import ProductCard from '../ProductCard';
import { Mousewheel, Navigation } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import '../../assets/swiper.scss';

const SlideProduct = (props) => {
    const products = props.products;
    return (
        <div className="w-full">
            <Swiper
                slidesPerView={5}
                spaceBetween={16}
                navigation
                mousewheel={{
                    sensitivity: 0,
                }}
                loop={true}
                modules={[Navigation, Mousewheel]}
            >
                {products.map((product) => (
                    <SwiperSlide key={product.title}>
                        <div className="w-full bg-white h-[440px] p-4 rounded-xl">
                            <ProductCard {...product} data={product} />
                        </div>
                    </SwiperSlide>
                ))}
            </Swiper>
        </div>
    );
};
export default SlideProduct;
