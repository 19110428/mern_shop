import { Link } from 'react-router-dom';
import { AiFillStar } from 'react-icons/ai';
import numberWithCommas from '../../utils/numberWithCommas';
import { useDispatch } from 'react-redux';
import { twMerge } from 'tailwind-merge';
import { getProductDetailApi } from '../../redux/product/productsApi';
import { productHistory } from '../../helpers/localStorage';

function ProductCard({ data }) {
    const dispatch = useDispatch();

    const handleProductClick = () => {
        productHistory.saveItems(data);
        getProductDetailApi(dispatch, data.slug);
        window.scrollTo({
            top: 0,
            behavior: 'smooth',
        });
    };
    return (
        <Link to={`/${data.category}/${data.slug}`} onClick={handleProductClick}>
            <div className="flex flex-col space-y-2 w-full">
                <div
                    className={twMerge(
                        'absolute z-10 w-fit text-xs rounded-md bg-gradient-to-r from-[#1746a2] to-[#5f9df7] text-white px-2 py-1',
                        !data.promotion && 'hidden',
                    )}
                >
                    {data.promotion}
                </div>
                <div className="w-full relative">
                    <img src={data.img} className="object-contain"></img>
                    {data.docquyen && (
                        <img
                            className="w-10 h-w-10 absolute bottom-0 left-0"
                            src="https://cdn.tgdd.vn/ValueIcons/Label_01-05.png"
                        ></img>
                    )}
                    {data.baohanh && (
                        <img
                            className="w-10 h-w-10 absolute bottom-0 left-0"
                            src="https://cdn.tgdd.vn/ValueIcons/Label_01-02.png"
                        ></img>
                    )}
                </div>
                {data.tag && (
                    <p className="text-white font-medium text-xs uppercase text-center w-[160px] rounded-3xl p-2 bg-[#db2562] mx-autơ">
                        {data.tag}
                    </p>
                )}
                <span className="font-semibold text-base">{data.title}</span>
                {data.category === 'Phone' || data.category === 'Tablet' ? (
                    <div>
                        {data.parameter.RAM?.map((item, index) => (
                            <span
                                key={index}
                                className="text-[#2f80ed] border border-solid border-[#2f80ed] rounded-md text-xs p-1 mr-2"
                            >
                                RAM {item}
                            </span>
                        ))}
                    </div>
                ) : (
                    <div>
                        {data?.parameter?.RAM && (
                            <span className="text-[#2f80ed] border border-solid border-[#2f80ed] rounded-md text-xs p-1">
                                {'RAM ' + data?.parameter.RAM.indexOf(',') === -1
                                    ? data?.parameter.RAM
                                    : data?.parameter.RAM.slice(0, data?.parameter.RAM.indexOf(','))}
                            </span>
                        )}
                    </div>
                )}
                <div className="text-[#d0021c] flex space-x-1 font-medium text-sm items-center">
                    <div className="font-bold text-base">{numberWithCommas(data?.price * (1 - data?.discount))}</div>
                    <div className="line-through text-gray-800">{numberWithCommas(data?.price)}</div>
                    <div className="">-{data?.discount * 100}%</div>
                </div>
                <div className="flex items-center text-yellow-400 font-bold space-x-2">
                    <div>{data.star}</div>
                    <AiFillStar />
                    <span className="text-gray-700 font-normal">({data.totalVote})</span>
                </div>
            </div>
        </Link>
    );
}

export default ProductCard;
