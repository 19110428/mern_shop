import { useState, useEffect, useRef } from 'react';
import Section from '../Section';
import { productHistory } from '../../helpers/localStorage';
import { AiOutlineClose } from 'react-icons/ai';
import Carousel from '../Carousel';

function ProductHistory() {
    const section = useRef();
    const [products, setProducts] = useState([]);
    const handleClick = () => {
        section.current.remove();
        productHistory.clearProductHistory();
    };

    useEffect(() => {
        const data = productHistory.getItems();
        setProducts(data);
    }, []);
    return (
        <Section styles="border border-solid border-gray-300 px-4 mt-6" ref={section}>
            <div className="flex justify-between items-center text-black w-full">
                <p className="text-xl font-semibold">Sản phẩm bạn đã xem</p>
                <span onClick={handleClick} className="cursor-pointer text-xl">
                    <AiOutlineClose />
                </span>
            </div>
            <Carousel data={products} />
        </Section>
    );
}

export default ProductHistory;
