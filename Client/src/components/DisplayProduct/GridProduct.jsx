import ProductCard from './ProductCard';
import { useCallback, useState } from 'react';

const GridProduct = (props) => {
    let { products } = props;
    const [isExpand, setIsExpand] = useState(false);

    const handleToggleExpand = useCallback(() => {
        setIsExpand((pre) => !pre);
    }, [setIsExpand]);

    return (
        <>
            <div className="grid grid-cols-4 gap-6">
                {isExpand
                    ? products.map((product, index) => <ProductCard key={index} data={product} />)
                    : products.slice(0, 8).map((product, index) => <ProductCard key={index} data={product} />)}
            </div>
            <div className="flex justify-center mt-6">
                <button
                    className="outline-none text-base border bg-blue-500 text-white font-semibold px-16 py-3 rounded-xl"
                    onClick={handleToggleExpand}
                >
                    {isExpand ? 'Thu nhỏ' : 'Xem tất cả sản phẩm'}
                </button>
            </div>
        </>
    );
};
export default GridProduct;
