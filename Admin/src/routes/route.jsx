import Home from "../pages/home/Home";
import Login from "../pages/login/Login";
import Comment from "../pages/comment/Comment";
import NotFound from "../pages/notfound/NotFound";
import { Routes, Route } from "react-router-dom";
import { ProtectedRoute } from "./protected.route";
import CommentView from "./../pages/comment/CommentView";
import ReviewsList from "../pages/review/ReviewsList";
import ReviewsView from "../pages/review/ReviewsView";
import Layout from "../components/Layout/Layout";
import Ratings from "../pages/rating/Ratings.jsx";
import ProductManagement from "../pages/Product/ProductManagement";
import UserManagement from "../pages/User/UserManagement";
import OrderManagement from "../pages/Order/OrderManagement";
import RatingManagement from "../pages/Ratingnew/RatingManagement";
import VoucherManagement from "../pages/Voucher/VoucherManagement";

export const AdminRoutes = () => {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <ProtectedRoute>
            <Layout />
          </ProtectedRoute>
        }
      >
        <Route path="/" element={<Home />} />
        <Route path="users" element={<UserManagement />} />
        <Route path="products" element={<ProductManagement />} />
        <Route path="orders" element={<OrderManagement />} />
        <Route path="ratings" element={<RatingManagement />} />
        <Route path="vouchers" element={<VoucherManagement />} />
        <Route path="reviews">
          <Route index element={<ReviewsList />} />
          <Route path=":reviewId" element={<ReviewsView />} />
        </Route>
        <Route path="comments">
          <Route index element={<Comment />} />
          <Route path=":commentId" element={<CommentView />} />
        </Route>
        <Route path="*" element={<NotFound />} />
      </Route>
      <Route path="/login" element={<Login />} />
    </Routes>
  );
};
