import dayjs from "dayjs";
import { Toggle } from "./components/Form/index";
import OrderStatus from "./pages/Order/Components/OrderStatus";

export const productColumns = [
  {
    field: "_id",
    headerName: "ID",
    // width: 100
  },
  {
    field: "img",
    headerName: "Image",
    // width: 150,
    renderCell: (params) => {
      return (
        <div className="flex justify-center items-center">
          <img
            className="w-10 h-10 rounded-full object-contain"
            src={params.row.img}
            alt="avatar"
          />
        </div>
      );
    },
  },
  {
    field: "title",
    headerName: "Name",
    // width: 300,
  },
  {
    field: "price",
    headerName: "Price",
    // width: 150,
  },
  {
    field: "category",
    headerName: "Category",
    // width: 100
  },
  {
    field: "brand",
    headerName: "Brand",
    //  width: 100
  },
  {
    field: "discount",
    headerName: "Discount",
    // width: 100
  },
  {
    field: "star",
    headerName: "Rating",
    // width: 100,
  },
];

export const orderColumns = [
  {
    field: "_id",
    headerName: "ID",
    // width: 150
  },
  {
    field: "totalQuantity",
    headerName: "Tổng số lượng",
    // width: 150,
  },
  {
    field: "totalPrice",
    headerName: "Tổng giá",
    // width: 150,
  },
  {
    field: "status",
    headerName: "Trạng thái",
    // width: 200,
    renderCell: (params) => {
      return (
        <OrderStatus type={params.row.status} message={params.row.status} />
      );
    },
  },
  {
    field: "createdAt",
    headerName: "Ngày mua",
    // width: 250,
    renderCell: (params) => {
      return (
        <div>{dayjs(params.row.createdAt).format("h:mm A, M/D/YYYY")}</div>
      );
    },
  },
];

export const commentColumns = [
  { field: "id", headerName: "IdComment", width: 70 },

  {
    field: "creator",
    headerName: "Username",
    width: 150,
    valueGetter: (params) => {
      let result = [];
      if (params.row.creator) {
        if (params.row.creator.username) {
          result.push(params.row.creator.username);
        }
      } else {
        result = ["Unknown"];
      }
      return result.join(", ");
    },
  },
  {
    field: "content",
    headerName: "Content",
    width: 230,
  },

  {
    field: "create_date",
    headerName: "Date",
    width: 100,
  },
  {
    field: "replyforId",
    headerName: "IdParent",
    width: 130,
    valueGetter: (params) => {
      let result = [];
      if (params.row.creator) {
        if (params.row.creator.replyforId) {
          result.push(params.row.creator.replyforId);
        }
      } else {
        result = ["Unknown"];
      }
      return result.join(", ");
    },
  },
  ,
  {
    field: "productId",
    headerName: "Product",
    width: 100,
  },
];

export const userColumns = [
  {
    field: "userId",
    headerName: "ID",
    //  width: 70
  },
  {
    field: "avatar",
    headerName: "Avatar",
    // width: 70,
    renderCell: (params) => {
      return (
        <div className="flex justify-center items-center">
          <img
            className="w-10 h-10 rounded-full object-contain"
            src={params.row.image}
            alt="avatar"
          />
        </div>
      );
    },
  },
  {
    field: "fullname",
    headerName: "Full Name",
    // width: 200,
  },
  {
    field: "username",
    headerName: "Username",
    // width: 200,
  },
  {
    field: "phone",
    headerName: "Phone",
    // width: 150,
  },
  {
    field: "role",
    headerName: "Role",
    // width: 150,
    renderCell: (params) => {
      return <span>{params.row?.role === "0" ? "Admin" : "User"}</span>;
    },
  },
  // {
  //   field: "address",
  //   headerName: "Address",
  //   width: 450,
  //   renderCell: (params) => {
  //     return (
  //       <div className="cellWithImg">{params.row?.address[0]?.address}</div>
  //     );
  //   },
  //   // valueGetter: (params) => {
  //   //     let result = [];
  //   //     if (params.row.address) {
  //   //         result.push(
  //   //             params.row.address.homeAdd +
  //   //                 ", " +
  //   //                 params.row.address.ward +
  //   //                 ", " +
  //   //                 params.row.address.district +
  //   //                 ", " +
  //   //                 params.row.address.city
  //   //         );
  //   //     } else {
  //   //         result = ["Unknown"];
  //   //     }
  //   //     return result.join(", ");
  //   // },
  // },
  {
    field: "verifyMail",
    headerName: "Verify Mail",
    // width: 100,
    renderCell: (params) => {
      return <Toggle isOn={params.row.verifyMail} disable />;
    },
  },
  {
    field: "verifyPhone",
    headerName: "Verify Phone",
    // width: 100,
    renderCell: (params) => {
      return <Toggle isOn={params.row.verifyPhone} disable />;
    },
  },
];

export const reviewProductColumns = [
  {
    field: "id",
    headerName: "ID",
    width: 50,
    renderCell: (params) => {
      return <div className="cellWithImg">{params.row.id}</div>;
    },
  },
  {
    field: "product",
    headerName: "Product",
    width: 550,
    renderCell: (params) => {
      return (
        <div className="cellWithImg">
          <img className="cellImg" src={params.row.img} alt="avatar" />
          {params.row.title}
        </div>
      );
    },
  },

  {
    field: "star",
    headerName: "Stars",
    width: 80,
    renderCell: (params) => {
      return <div className="cellWithImg">{params.row.star}</div>;
    },
  },
  {
    field: "totalVote",
    headerName: "Total Vote",
    width: 90,
    renderCell: (params) => {
      return <div className="cellWithImg">{params.row.totalVote}</div>;
    },
  },
];

export const reviewDetailColumns = [
  {
    field: "id",
    headerName: "ID",
    width: 400,
    renderCell: (params) => {
      return <div className="cellWithImg">{params.row.id}</div>;
    },
  },

  {
    field: "user",
    headerName: "User",
    width: 100,
    renderCell: (params) => {
      return <div className="cellWithImg">{params.row.user.username}</div>;
    },
  },

  {
    field: "star",
    headerName: "Stars",
    width: 80,
    renderCell: (params) => {
      return <div className="cellWithImg">{params.row.star}</div>;
    },
  },
  {
    field: "content",
    headerName: "Content",
    width: 230,
  },
];
