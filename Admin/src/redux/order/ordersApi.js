import { getAllOrder } from "./ordersSlice";
import { axiosInstance, baseURL } from "~/api/axios.config";

export const getAllOrders = async (dispatch) => {
  let res = await axiosInstance.get("/order/all");
  dispatch(getAllOrder(res));
  return res;
};

export const editOrder = async (data) => {
  let res = await axiosInstance.put(`/order/edit/${data._id}`, data);
  return res;
};

export const _getAllOrdersById = async (dispatch, data) => {
  let res = await axiosInstance.get(`order/getbyid/${data}`);
  dispatch(getAllOrder(res));
};
