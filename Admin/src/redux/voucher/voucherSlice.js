import { createSlice } from '@reduxjs/toolkit';
export const vouchers = createSlice({
    name: 'vouchers',
    initialState: {
        voucher: {
            data: null
        },
       
    },
    reducers: {
        allVoucher:(state,action)=>{
            state.all.data = action.payload;
        },
        getVoucherById: (state, action) => {
            state.voucher.data = action.payload;
        },
        removeVoucherById: (state, action) => {
            state.voucher.data = action.payload;
        },
        editVoucherById: (state, action) => {
            state.voucher.data = action.payload;
        },
        
    },
  },
);
export const {
  allVoucher,
  getVoucherById,
  removeVoucherById,
  editVoucherById,
} = vouchers.actions;
export default vouchers.reducer;
