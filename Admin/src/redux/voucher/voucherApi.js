import { getVoucherById } from "./voucherSlice";
import { axiosInstance } from "../../api/axios.config.jsx";

export const _getVouchers = async () => {
  let res = await axiosInstance.get("/voucher/all");
  return res;
};

export const _getVoucher = async (dispatch, id) => {
  let res = await axiosInstance.get(`/voucher/get/${id}`);
  dispatch(getVoucherById(res));
};

export const removeVoucher = async (id) => {
  let res = await axiosInstance.delete(`/voucher/delete/${id}`);
  return res;
};

export const editVoucher = async (id, data) => {
  let res = await axiosInstance.put(`/voucher/edit/${id}`, data);
  return res;
};

export const newVoucher = async (data) => {
  let res = await axiosInstance.post("/voucher/new", data);
  return res;
};

