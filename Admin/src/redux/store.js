// import orderReducer from './order/ordersSlice';

import orderSlice from "./order/ordersSlice";
import commentSlice from "./comment/commentSlice";
import reviewSlice from "./review/reviewSlice";
import userSlice from "./user/userSlice";
import showModalSlice from "./modal/showModalSlice";
import productSlice from "./product/productSlice";
import voucherSlice from "./voucher/voucherSlice";
// import ratingSlice from './rating/ratingSlice'
//khoi tao store
import { configureStore, combineReducers } from "@reduxjs/toolkit";

import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from "redux-persist";
import storage from "redux-persist/lib/storage";

const persistConfig = {
  key: "root",
  version: 1,
  storage,
};
const appReducer = combineReducers({
  orders: orderSlice,
  comments: commentSlice,
  reviews: reviewSlice,
  users: userSlice,
  modal: showModalSlice,
  products: productSlice,
  // ratings: ratingSlice,
  vouchers: voucherSlice,
});
const rootReducer = (state, action) => {
  if (action.type === "auth/logOutSuccess") {
    // for all keys defined in your persistConfig(s)
    //storage.removeItem('persist:root')
    // storage.removeItem('persist:otherKey')

    return appReducer(undefined, action);
  }
  return appReducer(state, action);
};
const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
});

export let persistor = persistStore(store);
