import { createSlice } from "@reduxjs/toolkit";
export const products = createSlice({
  name: "products",
  initialState: {
    product: {
      data: null,
    },
    all: {
      data: [],
    },
  },
  reducers: {
    allProduct: (state, action) => {
      state.all.data = action.payload;
    },
    getProductById: (state, action) => {
      state.product.data = action.payload;
    },
    removeProductById: (state, action) => {
      state.product.data = action.payload;
    },
    editProductById: (state, action) => {
      state.product.data = action.payload;
    },
  },
});
export const {
  allProduct,
  getProductById,
  removeProductById,
  editProductById,
} = products.actions;
export default products.reducer;
