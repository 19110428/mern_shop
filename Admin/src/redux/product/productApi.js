import {
  getProductById,
  removeProductById,
  editProductById,
  allProduct,
} from "./productSlice";
import { axiosInstance } from "../../api/axios.config.jsx";

export const _getProducts = async () => {
  let res = await axiosInstance.get("/product/all");
  return res;
};

export const _getProduct = async (dispatch, id) => {
  let res = await axiosInstance.get(`/product/get/${id}`);
  dispatch(getProductById(res));
};

export const removeProduct = async (id) => {
  let res = await axiosInstance.delete(`/product/delete/${id}`);
  return res;
};

export const editProduct = async (id, data) => {
  let res = await axiosInstance.put(`/product/edit/${id}`, data);
  return res;
};

export const newProduct = async (data) => {
  let res = await axiosInstance.post("/product/new", data);
  return res;
};
