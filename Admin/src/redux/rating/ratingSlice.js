import { createSlice } from "@reduxjs/toolkit";

export const ratings = createSlice({
  name: "ratings",
  initialState: {
    rating: {
      data: null,
    },
    all: {
      data: [],
    },
  },
  reducers: {
    Rating: (state, action) => {
      state.rating.data = action.payload;
    },
    allRating: (state, action) => {
      state.all.data = action.payload;
    },
  },
});
export const { Rating, allRating } = ratings.actions;
export default ratings.reducer;
