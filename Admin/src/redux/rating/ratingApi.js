import { Rating, allRating } from "./RatingSlice";
import { axiosInstance } from "~/api/axios.config";

export const _getAllRating = async () => {
  let res = await axiosInstance.get("rating/all");
  console.log(res);

  return res;
};

export const _newRating = async (dispatch, data) => {
  let res = await axiosInstance.post("rating/new", data);
  dispatch(Rating(res));
};

export const _getAllRatingProduct = async (dispatch, id) => {
  let res = await axiosInstance.get(`rating/get/${id}`);
  dispatch(allRating(res));
};

export const _editRating = async (id, data) => {
  let res = await axiosInstance.put(`/rating/edit/${id}`, data);
};

export const removeRating = async (id) => {
  let res = await axiosInstance.delete(`/rating/delete/${id}`);
  return res;
};

export const _getAllRatingPage = async (dispatch, page) => {
  let res = await axiosInstance.get(`rating/paging/${page}`);
  dispatch(allRating(res));
};
