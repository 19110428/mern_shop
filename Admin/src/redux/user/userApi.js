import { login, logout } from "./userSlice";
import { axiosInstance } from "~/api/axios.config";

export const _getUsers = async (axiotJWT) => {
  let res = await axiotJWT.get("/user/all");
  return res;
};
export const _getUser = async (id, axiotJWT) => {
  let res = await axiotJWT.get(`/user/get/${id}`);
  return res;
};
export const removeUser = async (id) => {
  let res = await axiosInstance.delete(`/user/delete/${id}`);
  return res;
};
export const editUser = async (id, data) => {
  let res = await axiosInstance.put(`/user/edit/${id}`, data);
  return res;
};

export const newUser = async (data) => {
  let res = await axiosInstance.post("/user/new", data);
  return res;
};

export const setAccessToken = (accessToken) => {
  localStorage.setItem("accessToken", JSON.stringify(accessToken));
};

export const getAccessToken = () => {
  const accessToken = localStorage.getItem("accessToken");

  if (!accessToken) {
    return null;
  }

  return JSON.parse(accessToken);
};

export const removeAccessToken = () => {
  localStorage.removeItem("accessToken");
};

export const _login = async (data, dispatch, navigate) => {
  let res = await axiosInstance.post("/auth/login", data);
  dispatch(login(res));
  setAccessToken(res.accessToken);
  if (res) {
    navigate("/");
  }
};

export const _logout = async (dispatch, navigate) => {
  new Promise(() => {
    setTimeout(() => {
      dispatch(logout());
      removeAccessToken();
      navigate("/");
    }, 1000);
  });
};
