import { DataGrid } from "@mui/x-data-grid";
import { Link } from "react-router-dom";
import { removeComments } from "../../redux/comment/commentApi";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { removeUser } from "../../redux/user/userApi";
import { removeReviews } from "../../redux/review/reviewApi";
import CreatePostModal from "../CreateModal";
import { removeProduct } from "../../redux/product/productApi";
import { toast } from "react-toastify";
import { useState } from "react";
import { FiTrash2 } from "react-icons/fi";
import { MdEdit } from "react-icons/md";
import TableRowAction from "../../Components/TableNew/TableRowAction";

const Datatable = ({
  rows,
  title,
  productColumns,
  type = "",
  reply = false,
  onClickEdit,
  onClickDelete,
}) => {
  // const dispatch = useDispatch();
  // const idPro = useParams();

  // const handleDelete = async (id, repply) => {
  //   // setData(data.filter((item) => item.id !== id));
  //   if (type === "comment") {
  //     const getCommentDel = rows.find((item) => item.id === id);
  //     if (repply === null) {
  //       //Tìm phần tử con
  //       let commentsDel = rows.filter((item) => item.creator.replyforId === id);
  //       if (commentsDel.length === 0) {
  //         await removeComments(dispatch, id, idPro.commentId);
  //         console.log("idDel1", id);
  //       } else {
  //         //khi xóa cha thì sẽ xóa các con comment
  //         for (let index = 0; index < commentsDel.length; index++) {
  //           console.log("idDel2", commentsDel[index].id);
  //           await removeComments(
  //             dispatch,
  //             commentsDel[index].id,
  //             idPro.commentId
  //           );
  //         }
  //         await removeComments(dispatch, id, idPro.commentId);
  //       }
  //     } else {
  //       await removeComments(dispatch, id, idPro.commentId);
  //       console.log("idDel3", id);
  //     }
  //   }
  //   if (type === "review") {
  //     await removeReviews(dispatch, id, idPro.commentId);
  //   }

  //   if (type === "products") {
  //     await removeProduct(id);
  //   }

  //   toast.success("Delete Success!", {});
  //   window.location.reload(false);
  // };
  // const hanleReplly = (id)=>{
  //   repplyComments(dispatch,id,idPro.commentId)
  // }
  // const handleAddPostModal = (repllyforId) => {
  //   showModal(dispatch, repllyforId);
  // };

  const actionColumn = [
    {
      field: "action",
      headerName: "Action",
      width: 200,
      renderCell: (params) => {
        return (
          <div className="flex items-center justify-end space-x-2">
            <TableRowAction
              id={params.row._id}
              title="Thay đổi"
              onClick={onClickEdit}
            >
              <MdEdit />
            </TableRowAction>
            <TableRowAction
              id={params.row._id}
              status="danger"
              title="Xóa"
              onClick={onClickDelete}
            >
              <FiTrash2 />
            </TableRowAction>
          </div>
          // <div className="cellAction">
          //   {type === "comment" && !reply && (
          //     <Link
          //       to={`/comments/${params.row.id}`}
          //       style={{ textDecoration: "none" }}
          //     >
          //       <div className="viewButton">View</div>
          //     </Link>
          //   )}
          //   {type === "comment" && reply && (
          //     <button
          //       className=""
          //       onClick={() => {
          //         if (params.row.creator.replyforId) {
          //           handleAddPostModal(params.row.creator.replyforId);
          //         } else {
          //           handleAddPostModal(params.row.id);
          //         }
          //       }}
          //     >
          //       Replly
          //     </button>
          //   )}
          //   {type === "review" && (
          //     <Link
          //       to={`/reviews/${params.row.id}`}
          //       style={{ textDecoration: "none" }}
          //     >
          //       <div className="viewButton">View</div>
          //     </Link>
          //   )}
          //   {type === "user" && (
          //     <Link
          //       to={`/users/${params.row.id}`}
          //       style={{ textDecoration: "none" }}
          //     >
          //       <div className="viewButton">View</div>
          //     </Link>
          //   )}
          //   {type === "products" && (
          //     <Link
          //       to={`/products/${params.row._id}`}
          //       style={{ textDecoration: "none" }}
          //     >
          //       <div className="viewButton">View</div>
          //     </Link>
          //   )}
          //   <Popconfirm
          //     title="Bạn có chắc xóa sản phẩm?"
          //     onConfirm={() => {
          //       if (type === "comment") {
          //         handleDelete(params.row._id, params.row.creator.replyforId);
          //       } else {
          //         handleDelete(params.row._id);
          //       }
          //     }}
          //     okText={<span className="text-black">Có</span>}
          //     cancelText={<span className="text-black">Không</span>}
          //   >
          //     <button className="deleteButton text-gray-400">Delete</button>
          //   </Popconfirm>
          // </div>
        );
      },
    },
  ];
  return (
    <div className="datatable">
      <DataGrid
        sx={{
          border: 0,
          padding: "0px",
        }}
        className="datagrid"
        rows={rows}
        columns={productColumns?.concat(actionColumn)}
        pageSize={10}
        rowsPerPageOptions={[10]}
        checkboxSelection
        getRowId={(row) => row._id}
      />
      <CreatePostModal></CreatePostModal>
    </div>
  );
};

export default Datatable;
