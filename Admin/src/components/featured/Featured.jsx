import "./featured.scss";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import { CircularProgressbar } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpOutlinedIcon from "@mui/icons-material/KeyboardArrowUpOutlined";
import { getAllOrders } from "../../redux/order/ordersApi";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import numberWithCommas from "../../utils/numberWithCommas";
const Featured = () => {
  const dispatch = useDispatch();
  const orders = useSelector((state) => state.orders?.order?.data);

  const isSameDay = (d1, d2) => {
    return (
      d1.getFullYear() === d2.getFullYear() &&
      d1.getDate() === d2.getDate() &&
      d1.getMonth() === d2.getMonth()
    );
  };

  const now = new Date();
  const previousWeek = new Date(now);
  previousWeek.setDate(previousWeek.getDate() - 7);
  const previousMonth = new Date(now);
  previousMonth.setDate(previousMonth.getDate() - 30);

  const totalToday = orders.reduce((total, item) => {
    const date = new Date(item?.createdAt);
    return isSameDay(now, date) && item.status === "Đã giao"
      ? total + item?.totalPrice
      : total;
  }, 0);

  const totalWeek = orders.reduce((total, item) => {
    const date = new Date(item?.createdAt);
    return date <= now && date >= previousWeek && item.status === "Đã giao"
      ? total + item?.totalPrice
      : total;
  }, 0);

  const totalMonth = orders.reduce((total, item) => {
    const date = new Date(item?.createdAt);
    return date <= now && date >= previousMonth && item.status === "Đã giao"
      ? total + item?.totalPrice
      : total;
  }, 0);
  console.log(totalToday);

  return (
    <div className="featured">
      <div className="top">
        <h1 className="title">Tổng doanh thu</h1>
        <MoreVertIcon fontSize="small" />
      </div>
      <div className="bottom">
        <div className="featuredChart">
          <CircularProgressbar value={54} text={"54%"} strokeWidth={6} />
        </div>
        <p className="title">Total sales made today</p>
        <p className="amount"> {numberWithCommas(totalToday)} đ</p>
        <p className="desc">
          Previous transactions processing. Last payments may not be included.
        </p>
        <div className="summary">
          <div className="item">
            <div className="itemTitle">Last Week</div>
            <div className="itemResult positive">
              <KeyboardArrowUpOutlinedIcon fontSize="small" />
              <div className="resultAmount">
                {numberWithCommas(totalWeek)} đ
              </div>
            </div>
          </div>
          <div className="item">
            <div className="itemTitle">Last Month</div>
            <div className="itemResult positive">
              <KeyboardArrowUpOutlinedIcon fontSize="small" />
              <div className="resultAmount">
                {numberWithCommas(totalMonth)} đ
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Featured;
