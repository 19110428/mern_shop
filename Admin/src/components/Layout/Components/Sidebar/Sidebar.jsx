import { twMerge } from "tailwind-merge";
import React from "react";

const Sidebar = ({ children }) => {
  return (
    <div
      className={twMerge("fixed left-0 top-0 h-full w-72 pt-20 duration-100")}
    >
      <div
        className={twMerge(
          "h-full w-full overflow-hidden border-r-2 border-gray-100 bg-gray-50 px-4 py-5 hover:overflow-clip hover:overflow-y-auto"
        )}
      >
        {children}
      </div>
    </div>
  );
};
export default Sidebar;
