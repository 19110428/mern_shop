import { useState } from "react";
import { TbLayoutDashboard } from "react-icons/tb";
import { BsPhone, BsFillPersonFill } from "react-icons/bs";
import { FiUsers } from "react-icons/fi";
import { GiStabbedNote } from "react-icons/gi";
import { RiCoupon3Line, RiDiscussLine } from "react-icons/ri";
import Sidebar from "./Sidebar";
import SidebarGroup from "./SidebarGroup";
import SidebarItem from "./SidebarItem";
import React from "react";

const AdminSidebar = () => {
  const [openingSidebarGroup, setOpeningSidebarGroup] = useState(["list"]);

  return (
    <Sidebar>
      <SidebarItem to="/" icon={<TbLayoutDashboard />} text="Dashboard" />
      <SidebarGroup
        id="management"
        openingIds={openingSidebarGroup}
        title="Management"
        onOpen={setOpeningSidebarGroup}
      >
        <SidebarItem to="/users" icon={<FiUsers />} text="Users" />
        <SidebarItem to="/products" icon={<BsPhone />} text="Products" />
        <SidebarItem to="/orders" icon={<GiStabbedNote />} text="Orders" />
        <SidebarItem to="/ratings" icon={<RiDiscussLine />} text="Ratings" />
        <SidebarItem to="/vouchers" icon={<RiCoupon3Line />} text="Vouchers" />
      </SidebarGroup>
      <SidebarGroup
        id="user"
        openingIds={openingSidebarGroup}
        title="User"
        onOpen={setOpeningSidebarGroup}
      >
        <SidebarItem to="/profile" icon={<BsFillPersonFill />} text="Profile" />
      </SidebarGroup>
    </Sidebar>
  );
};

export default AdminSidebar;
