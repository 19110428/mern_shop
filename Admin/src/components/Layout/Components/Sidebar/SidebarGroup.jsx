import _ from "lodash";
import {
  useCallback,
  useEffect,
  useLayoutEffect,
  useMemo,
  useState,
} from "react";
import { BiChevronRight } from "react-icons/bi";
import { useLocation } from "react-router-dom";
import { twMerge } from "tailwind-merge";

const SidebarGroup = ({
  className,
  children,
  id,
  openingIds,
  title,
  onOpen,
}) => {
  const [isShow, setIsShow] = useState(false);

  const childrenPath = useMemo(() => {
    const newChildren = Array.isArray(children) ? children : [children];

    return newChildren?.map((item) => {
      if (
        typeof item === "string" ||
        typeof item === "number" ||
        typeof item === "boolean"
      ) {
        return item;
      }

      if (item && "props" in item && "to" in item.props) {
        return item.props.to;
      }

      return item;
    });
  }, [children]);

  const location = useLocation();

  const handleOpenSidebarGroup = useCallback(() => {
    onOpen?.((prev) => {
      if (_.includes(prev, id)) {
        return prev;
      }

      return [id];
    });
  }, [id, onOpen]);

  useEffect(() => {
    if (_.includes(openingIds, id)) {
      setIsShow(true);
      return;
    }

    setIsShow(false);
  }, [id, openingIds]);

  useLayoutEffect(() => {
    if (_.includes(childrenPath, location.pathname)) {
      handleOpenSidebarGroup();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className={twMerge("h-fit w-full py-4", isShow && "pb-0")}>
      <div
        className={twMerge(
          "flex h-4 w-full items-center text-xs font-semibold text-gray-500 hover:text-primary-600",
          isShow && "mb-4",
          className
        )}
        role="button"
        tabIndex={0}
        onClick={handleOpenSidebarGroup}
      >
        <div>
          <BiChevronRight
            className={twMerge(
              "icon -ml-0.5 mr-[3px] text-sm duration-200",
              isShow && "rotate-90"
            )}
          />
        </div>
        <div className="ml-2 break-all uppercase line-clamp-1">{title}</div>
      </div>
      {isShow && children}
    </div>
  );
};

export default SidebarGroup;
