import React from "react";
import { cloneElement, useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import { twMerge } from "tailwind-merge";

const SidebarItem = ({ to, icon, text, textColor, className }) => {
  const [isActivated, setIsActivated] = useState(false);

  const { pathname } = useLocation();

  useEffect(() => {
    const lastPath = pathname.split("/").pop();
    setIsActivated(lastPath ? to.includes(lastPath) : false);
  }, [pathname, to]);

  return (
    <Link
      to={to}
      className={twMerge(
        "group relative my-1 flex items-center rounded-md px-4 py-3 font-[450] before:absolute before:left-0 before:top-1/2 before:h-6 before:w-1 before:-translate-y-1/2 before:rounded-sm before:bg-primary-800 hover:bg-gray-200 hover:text-primary-600 xs:px-7 md:px-6",
        isActivated
          ? "bg-gray-200 text-primary-600 before:block"
          : "text-slate-700 before:hidden"
      )}
    >
      {cloneElement(icon, {
        className: twMerge(
          "flex-shrink-0 w-5 mr-4 group-hover:text-primary-600 ml-0.5 xs:ml-0",
          textColor,
          className
        ),
        size: 20,
      })}
      <div
        className={twMerge("hidden !line-clamp-1 md:inline-block", textColor)}
      >
        {text}
      </div>
    </Link>
  );
};

export default SidebarItem;
