import { COMMON_PATH } from "../../../Constants/URLs";
import paypalImage from "../../../assets/images/footer/paypal.png";
import shape01Image from "../../../assets/images/footer/shape_01.png";
// import Logo from "../../Components/Logo/Logo";
import FooterLinkItem from "./FooterLinkItem";
import FooterSocialItems from "./FooterSocialItems";

const Footer = () => {
  return (
    <div
      className="relative z-10 mx-auto overflow-hidden border-t-2 border-gray-100 bg-white px-3 sm:px-6 lg:px-8 xl:px-36"
      id="footer"
    >
      <div className="absolute left-0 top-4 z-0 h-full w-full">
        <div className="absolute flex h-full w-full">
          <img
            src={shape01Image}
            alt="LinkStar"
            className="h-full w-full object-contain object-center"
          />
        </div>
      </div>
      <div className="relative z-10 grid gap-x-10 py-4 sm:grid-cols-2 sm:py-6 md:grid-cols-3 md:py-10">
        <div className="">
          <div className="mt-1.5">
            {/* <Logo imageClassName="h-10" /> */}
          </div>
          <div className="mt-6 max-w-xs font-semibold">
            KPShop has everything you need
          </div>
          <div className="mt-6 sm:mt-8">
            <img src={paypalImage} alt="Paypal" />
          </div>
        </div>
        <div className="relative z-10 mt-6 grid grid-cols-1 xs:grid-cols-2 sm:mt-0 md:col-span-2 md:grid-cols-3">
          <div className="my-2 flex flex-col sm:my-4">
            <div className="mb-3 font-bold">Company</div>
            <div className="flex flex-col ">
              <FooterLinkItem to={COMMON_PATH.ABOUT_PATH}>About</FooterLinkItem>
              <FooterLinkItem to={COMMON_PATH.BLOG_PATH}>Blog</FooterLinkItem>
              <FooterLinkItem to={COMMON_PATH.PARTNER_PATH}>
                Partners
              </FooterLinkItem>
              <FooterLinkItem to={COMMON_PATH.CONTACT_PATH}>
                Contact
              </FooterLinkItem>
            </div>
          </div>
          <div className="my-2 flex flex-col sm:my-4">
            <div className="mb-3 font-bold">Support</div>
            <div className="flex flex-col ">
              <FooterLinkItem to={COMMON_PATH.GETTING_STARTED_PATH}>
                Getting Started
              </FooterLinkItem>
              <FooterLinkItem to={COMMON_PATH.DOCUMENTATION_PATH}>
                Documentation
              </FooterLinkItem>
              <FooterLinkItem to={COMMON_PATH.GUIDE_PATH}>
                Guides
              </FooterLinkItem>
              <FooterLinkItem to={COMMON_PATH.FAQ_PATH}>FAQs</FooterLinkItem>
            </div>
          </div>
          <div className="my-2 flex flex-col sm:my-4">
            <div className="mb-3 font-bold">Trust & Legal</div>
            <div className="flex flex-col ">
              <FooterLinkItem to={COMMON_PATH.TERM_PATH}>
                Terms & Conditions
              </FooterLinkItem>
              <FooterLinkItem to={COMMON_PATH.NOTICE_PATH}>
                Privacy Notice
              </FooterLinkItem>
              <FooterLinkItem to={COMMON_PATH.CLAIM_PATH}>Claim</FooterLinkItem>
            </div>
          </div>
        </div>
      </div>
      <div className="relative z-10 mt-2 items-center justify-between border-t-2 border-gray-100 py-4 md:flex">
        <div className="text-center md:text-left">
          &copy; All rights reserved.
        </div>
        <FooterSocialItems className="mt-4 flex justify-center md:mt-0 md:justify-start" />
      </div>
    </div>
  );
};

export default Footer;
