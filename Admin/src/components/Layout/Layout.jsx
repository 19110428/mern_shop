import { Outlet } from "react-router-dom";
import AdminSidebar from "./Components/Sidebar/AdminSidebar";

function Layout() {
  return (
    <div className="min-h-fit-layout w-full">
      <AdminSidebar />
      <div className="ml-72 w-fit-layout items-center">
        <Outlet />
      </div>
    </div>
  );
}

export default Layout;
