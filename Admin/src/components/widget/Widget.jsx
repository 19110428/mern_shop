import { Link } from "react-router-dom";
import "./widget.scss";
import PersonOutlinedIcon from "@mui/icons-material/PersonOutlined";
import ShoppingCartOutlinedIcon from "@mui/icons-material/ShoppingCartOutlined";
import MonetizationOnOutlinedIcon from "@mui/icons-material/MonetizationOnOutlined";
import { _getUsers } from "../../redux/user/userApi";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { createAxios } from "../../api/createInstance.js";
import { useEffect } from "react";
import { login } from "../../redux/user/userSlice";
import { _getProducts } from "../../redux/product/productApi";
import { getAllOrders } from "../../redux/order/ordersApi";
const Widget = ({ type }) => {
  let data;
  console.log(type);
  let amount = 100;
  const currentUser = useSelector((state) => state.users?.current?.data);

  const dispatch = useDispatch();
  const navigate = useNavigate();
  let axiosJWT = createAxios(currentUser, dispatch, login);
  useEffect(() => {
    _getUsers(dispatch, axiosJWT);
    _getProducts();
    getAllOrders(dispatch);
  }, []);

  const products = useSelector((state) => state.products?.all?.data);
  const orders = useSelector((state) => state.orders?.order?.data);
  const users = useSelector((state) => state.users?.all?.data);
  if (type === "user") {
    amount = users?.length;
  } else if (type === "product") {
    amount = products?.length;
  } else if (type === "order") {
    amount = orders?.length;
  }

  switch (type) {
    case "user":
      data = {
        title: "USERS",
        link: "See all users",
        icon: (
          <PersonOutlinedIcon
            className="icon"
            style={{
              color: "crimson",
              backgroundColor: "rgba(255, 0, 0, 0.2)",
            }}
          />
        ),
      };
      break;
    case "product":
      data = {
        title: "PRODUCTS",
        link: "View all products",
        icon: (
          <MonetizationOnOutlinedIcon
            className="icon"
            style={{
              backgroundColor: "rgba(0, 128, 0, 0.2)",
              color: "green",
            }}
          />
        ),
      };
      break;
    case "order":
      data = {
        title: "ORDERS",
        link: "View all orders",
        icon: (
          <ShoppingCartOutlinedIcon
            className="icon"
            style={{
              backgroundColor: "rgba(218, 165, 32, 0.2)",
              color: "goldenrod",
            }}
          />
        ),
      };
      break;
  }

  return (
    <div className="widget">
      <div className="left">
        <span className="title">{data?.title}</span>
        <span className="counter">{amount}</span>
        <Link to={`${type}s`}>
          <span className="link">{data?.link}</span>
        </Link>
      </div>
      <div className="right">{data?.icon}</div>
    </div>
  );
};

export default Widget;
