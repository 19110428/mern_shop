import { string } from "yup";
import { generateFormSchema } from "../Utils/Helpers";

const voucherFormSchema = () =>
  generateFormSchema({
    title: string().required("The voucher's title is required.").nullable(),
    name: string().required("The voucher's name is required.").nullable(),
    sale: string().required("The voucher's sale is required.").nullable(),
  });

export { voucherFormSchema };
