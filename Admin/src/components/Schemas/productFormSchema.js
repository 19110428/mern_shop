import { string } from "yup";
import { generateFormSchema } from "../Utils/Helpers";

const productFormSchema = () =>
  generateFormSchema({
    title: string().required("The product's title is required.").nullable(),
    price: string().required("The product's price is required.").nullable(),
  });

export { productFormSchema };
