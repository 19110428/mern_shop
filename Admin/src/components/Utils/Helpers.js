import { first, isArray, isObject, keys, snakeCase } from "lodash";
import { object } from "yup";

const setDocumentTitle = (title) => {
  window.document.title = `${title} - ${
    process.env.REACT_APP_DOCUMENT_TITLE ?? "TechupZone"
  }`;
  window.scrollTo({
    top: 0,
    behavior: "smooth",
  });
};

const generateFormSchema = (shape) => object().shape(shape);

/* eslint-disable no-param-reassign */
const snakelizeNestedObjectKeys = (obj) => {
  const result = {};

  Object.keys(obj).forEach((key) => {
    const newKey = snakeCase(key);
    const value = obj[key];

    if (isObject(value) && !isArray(value)) {
      result[newKey] = snakelizeNestedObjectKeys(value);
      return;
    }

    result[newKey] = value;
  });

  return result;
};
/* eslint-enable no-param-reassign */

const checkImageURL = (url) => {
  return url?.match(/\.(jpeg|jpg|gif|png)$/);
};

const handleResponseError = (
  error,
  setError,
  handleError,
  unknownErrorMessage
) => {
  const { response } = error;

  if (!response) {
    handleError(unknownErrorMessage);
    return;
  }

  const { data } = response;

  if (!data) {
    handleError(unknownErrorMessage);
    return;
  }

  const { errors } = data.data;

  if (!errors?.length) {
    handleError(unknownErrorMessage);
    return;
  }

  keys(errors).forEach((key) => {
    setError(key, {
      type: "manual",
      message: first(errors[key]),
    });
  });
};

export {
  setDocumentTitle,
  snakelizeNestedObjectKeys,
  checkImageURL,
  generateFormSchema,
  handleResponseError,
};
