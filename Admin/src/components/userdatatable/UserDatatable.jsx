import "./datatable.scss";
import { DataGrid } from "@mui/x-data-grid";
import { Link } from "react-router-dom";
import { Popconfirm } from "antd";
import { removeUser } from "../../redux/user/userApi";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
const UserDatatable = ({ rows, title, userColumns }) => {
  const dispatch = useDispatch();
  const handleDelete = async (id) => {
    console.log(id);
    await removeUser(dispatch, id);
    toast.success("Delete Success!", {});
    window.location.reload(false);
  };

  const actionColumn = [
    {
      field: "action",
      headerName: "Action",
      width: 200,
      renderCell: (params) => {
        return (
          <div className="cellAction">
            <Link
              to={`/users/${params.row.userId}`}
              style={{ textDecoration: "none" }}
            >
              <div className="viewButton">View</div>
            </Link>
            <Popconfirm
              title="Bạn có chắc xóa sản phẩm?"
              onConfirm={() => {
                handleDelete(params.row._id);
              }}
              okText={<span className="text-black">Có</span>}
              cancelText={<span className="text-black">Không</span>}
            >
              <button className="deleteButton text-gray-400">Delete</button>
            </Popconfirm>
          </div>
        );
      },
    },
  ];
  return (
    <div className="datatable">
      <div className="datatableTitle">
        List Users
        <Link to="/users/new" className="link">
          Add New
        </Link>
      </div>
      <DataGrid
        className="datagrid"
        rows={rows}
        columns={userColumns.concat(actionColumn)}
        pageSize={9}
        rowsPerPageOptions={[9]}
        checkboxSelection
        getRowId={(row) => row._id}
      />
    </div>
  );
};

export default UserDatatable;
