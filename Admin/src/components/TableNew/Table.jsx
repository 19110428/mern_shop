import { DataGrid } from "@mui/x-data-grid";

const Table = ({ rows, dataColumns, actionColumn }) => {
  return (
    <div className="h-[650px] relative">
      <DataGrid
        sx={{
          border: 0,
          padding: "0px",
          "& .MuiDataGrid-columnHeaders": {
            backgroundColor: "#e5e7eb",
            borderRadius: "8px 8px 0 0",
          },
          "& .MuiDataGrid-columnHeaderTitleContainer": {
            display: "flex",
            justifyContent: "center",
          },
          "& .MuiDataGrid-columnHeaderTitle": {
            fontWeight: "600",
          },
          "& .MuiDataGrid-row": {},
          "& .MuiDataGrid-cell": {
            display: "flex",
            justifyContent: "center",
          },
        }}
        rows={rows}
        columns={dataColumns?.concat(actionColumn)}
        pageSize={10}
        rowsPerPageOptions={[10]}
        checkboxSelection
        getRowId={(row) => row._id}
      />
    </div>
  );
};

export default Table;
