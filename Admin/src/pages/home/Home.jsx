import "./home.scss";
import Widget from "../../components/widget/Widget";
import Featured from "../../components/featured/Featured";
import Chart from "../../components/chart/Chart";

const Home = () => {
  return (
    <div>
      <div className="widgets">
        <Widget type="user" />
        <Widget type="product" />
        <Widget type="order" />
      </div>
      <div className="charts">
        <Featured />  
        <Chart title="Last 6 Months (Revenue)" aspect={2 / 1} />
      </div>
    </div>
  );
};

export default Home;
