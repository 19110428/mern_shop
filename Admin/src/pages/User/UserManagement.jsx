import { useCallback, useEffect, useMemo, useState } from "react";
import {
  _getUsers,
  editUser,
  newUser,
  removeUser,
} from "../../redux/user/userApi";
import ContentWrapper from "../../components/Layout/Components/ContentWrapper";
import { ConfirmationModal } from "../../components/Modal";
import { toast } from "react-toastify";
import UserModificationModal from "./Components/UserModificationModal";
import UserHeaderActions from "./Components/UserHeaderAction";
import Datatable from "./Components/UserTable";
import { useDispatch, useSelector } from "react-redux";
import { createAxios } from "../../api/createInstance";
import { login } from "../../redux/user/userSlice";

const UserManagement = () => {
  const currentUser = useSelector((state) => state.users?.current?.data);
  const dispatch = useDispatch();
  let axiosJWT = createAxios(currentUser, dispatch, login);

  const [isLoading, setIsLoading] = useState(true);
  const [selectedUserId, setSelectedUserId] = useState(null);
  const [userData, setUserData] = useState([]);
  const [isShowModificationModal, setIsShowModificationModal] = useState(false);
  const [isShowDeleteModal, setIsShowDeleteModal] = useState(false);

  const selectedUser = useMemo(() => {
    return userData.find((item) => item._id === selectedUserId) ?? null;
  }, [selectedUserId, userData]);

  const handleClickAddButton = useCallback(() => {
    setIsShowModificationModal(true);
  }, []);

  const handleClickEditButton = useCallback((id) => {
    setSelectedUserId(id ?? null);
    setIsShowModificationModal(true);
  }, []);

  const handleClickDeleteButton = useCallback((id) => {
    setSelectedUserId(id ?? null);
    setIsShowDeleteModal(true);
  }, []);

  const fetchData = useCallback(async () => {
    setIsLoading(true);

    try {
      const data = await _getUsers(axiosJWT);
      setUserData(data);
      setIsLoading(false);
    } catch (error) {
      toast.error("An unknown error occurred while processing your request.");
    }
  }, []);

  const handleDelete = useCallback(async () => {
    if (!selectedUser) {
      return;
    }

    try {
      await removeUser(selectedUser?._id);

      toast.success("The user has been deleted successfully.");

      fetchData();
    } catch (error) {
      toast.error(
        "An error occurred while deleting the user. Please try again later."
      );
    } finally {
      setIsShowDeleteModal(false);
    }
  }, [selectedUser, toast]);

  const handleCloseModal = useCallback(() => {
    setIsShowModificationModal(false);
    setIsShowDeleteModal(false);
    setSelectedUserId(null);
  }, []);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  return (
    <ContentWrapper
      title="User management"
      actions={<UserHeaderActions onClickAdd={handleClickAddButton} />}
    >
      {!isLoading && (
        <Datatable
          rows={userData}
          onClickEdit={handleClickEditButton}
          onClickDelete={handleClickDeleteButton}
        />
      )}

      <ConfirmationModal
        message="Are you sure you want to delete this user? This action cannot be undone."
        isOpen={isShowDeleteModal}
        status="danger"
        title={
          <div>
            Delete user
            <span className="mx-1 inline-block font-semibold text-red-500">
              {selectedUser?.title}
            </span>
          </div>
        }
        onClose={handleCloseModal}
        onConfirm={handleDelete}
      />

      <UserModificationModal
        isOpen={isShowModificationModal}
        user={selectedUser}
        onCreate={newUser}
        onCreated={fetchData}
        onEdit={editUser}
        onEdited={fetchData}
        onClose={handleCloseModal}
      />
    </ContentWrapper>
  );
};

export default UserManagement;
