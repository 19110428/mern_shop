import UserTableRowAction from "./UserTableRowAction";
import Table from "../../../components/TableNew/Table";
import { Toggle } from "../../../components/Form";

const UserTable = ({ rows, onClickEdit, onClickDelete }) => {
  const userColumns = [
    {
      field: "userId",
      headerName: "ID",
      width: 50,
    },
    {
      field: "avatar",
      headerName: "Avatar",
      renderCell: (params) => {
        return (
          <div className="flex justify-center items-center">
            <img
              className="w-10 h-10 rounded-full object-cover"
              src={params.row.image}
              alt="avatar"
            />
          </div>
        );
      },
    },
    {
      field: "fullname",
      headerName: "Full Name",
      width: 200,
    },
    {
      field: "username",
      headerName: "Username",
      width: 250,
    },
    {
      field: "phone",
      headerName: "Phone",
    },
    {
      field: "role",
      headerName: "Role",
      renderCell: (params) => {
        return <span>{params.row?.role === "0" ? "Admin" : "User"}</span>;
      },
    },
    {
      field: "address",
      headerName: "Address",
      width: 250,
      renderCell: (params) => {
        return (
          <div className="whitespace-normal break-words">
            {params.row?.address[0]?.address}
          </div>
        );
      },
    },
    {
      field: "verifyMail",
      headerName: "Verify Mail",
      width: 150,
      renderCell: (params) => {
        return <Toggle isOn={params.row.verifyMail} disable />;
      },
    },
    {
      field: "verifyPhone",
      headerName: "Verify Phone",
      width: 150,
      renderCell: (params) => {
        return <Toggle isOn={params.row.verifyPhone} disable />;
      },
    },
  ];

  const actionColumn = [
    {
      field: "action",
      headerName: "",
      renderCell: (params) => {
        return (
          <UserTableRowAction
            id={params.row._id}
            onClickEdit={onClickEdit}
            onClickDelete={onClickDelete}
          />
        );
      },
    },
  ];
  return (
    <Table rows={rows} dataColumns={userColumns} actionColumn={actionColumn} />
  );
};

export default UserTable;
