import { yupResolver } from "@hookform/resolvers/yup";
import { useCallback, useEffect, useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import Button from "../../../components/Button/Button";
import { Input } from "../../../components/Form";
import { Modal } from "../../../components/Modal";
import { userFormSchema } from "../../../components/Schemas/userFormSchema";
import { toast } from "react-toastify";

const DEFAULT_VALUE = {
  fullname: "",
  username: "",
  phone: "",
  password: "",
  image: "",
};

const UserModificationModal = ({
  isOpen,
  user,
  onClose,
  onCreate,
  onCreated,
  onEdit,
  onEdited,
  ...props
}) => {
  const [isSubmitting, setIsSubmitting] = useState(false);

  const handleUnknownError = useCallback(() => {
    toast.error("An unknown error occurred while processing your request.");
  }, [toast]);

  const {
    control,
    reset,
    setValue,
    handleSubmit: useFormSubmit,
    ...methods
  } = useForm({
    resolver: yupResolver(userFormSchema()),
    defaultValues: DEFAULT_VALUE,
  });

  console.log({ user });

  const handleCreateUser = useCallback(
    async (formData) => {
      try {
        await onCreate(formData);
        toast.success("The user has been updated successfully.");
        onCreated();
        onClose();
      } catch (error) {
        toast.error("An unknown error occurred while processing your request.");
      } finally {
        setIsSubmitting(false);
      }
    },
    [handleUnknownError, methods.setError, onClose, onCreate, onCreated, toast]
  );

  const handleEditUser = useCallback(
    async (formData) => {
      if (!user) return;
      try {
        await onEdit(user._id, formData);
        toast.success("The user has been updated successfully.");
        onEdited();
        onClose();
      } catch (error) {
        toast.error("An unknown error occurred while processing your request.");
      } finally {
        setIsSubmitting(false);
      }
    },
    [
      handleUnknownError,
      methods.setError,
      onClose,
      onEdit,
      onEdited,
      user,
      toast,
    ]
  );

  const handleSubmit = useFormSubmit(async (formData) => {
    setIsSubmitting(true);

    if (!user) {
      handleCreateUser(formData);
      return;
    }

    handleEditUser(formData);
  });

  useEffect(() => {
    if (!isOpen) {
      return;
    }

    setIsSubmitting(false);

    if (user) {
      reset({
        ...user,
        password: "",
      });
      return;
    }

    reset(DEFAULT_VALUE);
  }, [isOpen, reset, user]);

  return (
    <Modal
      isLoading={isSubmitting}
      isOpen={isOpen}
      title={user ? "Cập nhật người dùng" : "Thêm người dùng"}
      onClose={onClose}
      onConfirm={handleSubmit}
      {...props}
    >
      <FormProvider
        control={control}
        handleSubmit={useFormSubmit}
        setValue={setValue}
        reset={reset}
        {...methods}
      >
        <Input
          label="Fullname"
          name="fullname"
          className="block"
          disabled={isSubmitting}
          control={control}
          isRequired
        />
        <Input
          className="mt-6 block"
          control={control}
          disabled={isSubmitting || !!user}
          label="Email"
          name="username"
          isRequired
        />
        <Input
          className="mt-6 block"
          control={control}
          disabled={isSubmitting}
          label="Phone"
          name="phone"
          isRequired
        />
        {!user && (
          <Input
            className="mt-6 block"
            control={control}
            disabled={isSubmitting}
            label="Password"
            name="password"
            isRequired
          />
        )}
        <Input
          className="mt-6 block"
          control={control}
          disabled={isSubmitting}
          label="Avatar"
          name="image"
        />
        <Button type="submit" className="hidden">
          Confirm
        </Button>
      </FormProvider>
    </Modal>
  );
};

export default UserModificationModal;
