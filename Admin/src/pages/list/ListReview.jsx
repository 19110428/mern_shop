import "./list.scss";
import { useState, useEffect } from "react";
import Datatable from "../../components/datatable/Datatable";
import { useLocation } from "react-router-dom";
import { ProductService } from "~/services";
import { productColumns } from "~/datatablesource";
import AdminSidebar from "../../components/layout/Sidebar/AdminSidebar";


const ListReview = () => {
  const locationUrl = useLocation();
  console.log(locationUrl.pathname);
  const [data, setData] = useState([]);

  useEffect(() => {
    function getProducts() {
      ProductService.getProducts(1, 9).then((res) => setData(res.data));
    }
    getProducts();
  }, []);
  return (
    <div className="list">
      <AdminSidebar />
      <div className="listContainer">
        <Datatable rows={data} title="" productColumns={productColumns} />
      </div>
    </div>
  );
};

export default ListReview;
