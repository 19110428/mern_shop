import "./list.scss";
import { useEffect } from "react";
import Datatable from "~/components/userdatatable/UserDatatable";
import { userColumns } from "~/datatablesource";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { createAxios } from "../../api/createInstance.js";
import { login } from "../../redux/user/userSlice";
import { _getUsers } from "../../redux/user/userApi";
import ContentWrapper from "../../components/layout/ContentWrapper";
const ListUser = () => {
  const currentUser = useSelector((state) => state.users?.current?.data);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  let axiosJWT = createAxios(currentUser, dispatch, login);
  useEffect(() => {
    _getUsers(dispatch, axiosJWT);
  }, []);

  const data = useSelector((state) => state.users?.all?.data);
  return (
    <ContentWrapper
      isBlank
      title="User management"
      // actions={<UserAppHeaderActions onClickAdd={handleClickAddButton} />}
    >
      <Datatable rows={data} title="" userColumns={userColumns} />
    </ContentWrapper>
  );
};

export default ListUser;
