import { yupResolver } from "@hookform/resolvers/yup";
import { useCallback, useEffect, useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import Button from "../../../components/Button/Button";
import { Input } from "../../../components/Form";
import { Modal } from "../../../components/Modal";
import { voucherFormSchema } from "../../../components/Schemas/voucherFormSchema";
import { toast } from "react-toastify";

const DEFAULT_VALUE = {
  title: "",
  name: "",
  sale: "",
};

const VoucherModificationModal = ({
  isOpen,
  voucher,
  onClose,
  onCreate,
  onCreated,
  onEdit,
  onEdited,
  ...props
}) => {
  const [isSubmitting, setIsSubmitting] = useState(false);

  const {
    control,
    reset,
    setValue,
    handleSubmit: useFormSubmit,
    ...methods
  } = useForm({
    resolver: yupResolver(voucherFormSchema()),
    defaultValues: DEFAULT_VALUE,
  });

  const handleCreateVoucher = useCallback(
    async (formData) => {
      try {
        await onCreate(formData);
        toast.success("The voucher has been updated successfully.");
        onCreated();
        onClose();
      } catch (error) {
        toast.error("An unknown error occurred while processing your request.");
      } finally {
        setIsSubmitting(false);
      }
    },
    [methods.setError, onClose, onCreate, onCreated, toast]
  );

  const handleEditVoucher = useCallback(
    async (formData) => {
      if (!voucher) return;
      try {
        await onEdit(voucher._id, formData);
        toast.success("The voucher has been updated successfully.");
        onEdited();
        onClose();
      } catch (error) {
        toast.error("An unknown error occurred while processing your request.");
      } finally {
        setIsSubmitting(false);
      }
    },
    [methods.setError, onClose, onEdit, onEdited, voucher, toast]
  );

  const handleSubmit = useFormSubmit(async (formData) => {
    setIsSubmitting(true);

    if (!voucher) {
      handleCreateVoucher(formData);
      return;
    }

    handleEditVoucher(formData);
  });

  useEffect(() => {
    if (!isOpen) {
      return;
    }

    setIsSubmitting(false);

    if (voucher) {
      reset(voucher);
      return;
    }

    reset(DEFAULT_VALUE);
  }, [isOpen, reset, voucher]);

  return (
    <Modal
      isLoading={isSubmitting}
      isOpen={isOpen}
      title={voucher ? "Cập nhật mã giảm giá" : "Thêm mã giảm giá"}
      onClose={onClose}
      onConfirm={handleSubmit}
      {...props}
    >
      <FormProvider
        control={control}
        handleSubmit={useFormSubmit}
        setValue={setValue}
        reset={reset}
        {...methods}
      >
        <Input
          className="block"
          control={control}
          disabled={isSubmitting}
          label="Title"
          name="title"
        />
        <Input
          className="mt-6 block"
          control={control}
          disabled={isSubmitting}
          label="Name"
          name="name"
        />
        <Input
          className="mt-6 block"
          control={control}
          disabled={isSubmitting}
          label="Sale"
          name="sale"
        />
        <Button type="submit" className="hidden">
          Confirm
        </Button>
      </FormProvider>
    </Modal>
  );
};

export default VoucherModificationModal;
