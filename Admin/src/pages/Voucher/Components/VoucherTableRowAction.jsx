import { FiTrash2 } from "react-icons/fi";
import { MdEdit } from "react-icons/md";
import TableRowAction from "../../../Components/TableNew/TableRowAction";

const VoucherTableRowAction = ({ id, onClickEdit, onClickDelete }) => {
  return (
    <div className="flex items-center justify-end space-x-2">
      <TableRowAction id={id} title="Thay đổi" onClick={onClickEdit}>
        <MdEdit size={16} />
      </TableRowAction>
      <TableRowAction
        id={id}
        status="danger"
        title="Xóa"
        onClick={onClickDelete}
      >
        <FiTrash2 size={16} />
      </TableRowAction>
    </div>
  );
};

export default VoucherTableRowAction;
