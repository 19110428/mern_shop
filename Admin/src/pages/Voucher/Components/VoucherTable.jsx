import VoucherTableRowAction from "./VoucherTableRowAction";
import Table from "../../../components/TableNew/Table";

const VoucherTable = ({ rows, onClickEdit, onClickDelete }) => {
  const voucherColumns = [
    {
      field: "_id",
      headerName: "ID",
      width: 150,
    },
    {
      field: "title",
      headerName: "Title",
      width: 250,
    },
    {
      field: "name",
      headerName: "Name",
      width: 250,
    },
    {
      field: "sale",
      headerName: "Sale",
      width: 150,
    },
  ];
  const actionColumn = [
    {
      field: "action",
      headerName: "",
      renderCell: (params) => {
        return (
          <VoucherTableRowAction
            id={params.row._id}
            onClickEdit={onClickEdit}
            onClickDelete={onClickDelete}
          />
        );
      },
    },
  ];
  return (
    <Table
      rows={rows}
      dataColumns={voucherColumns}
      actionColumn={actionColumn}
    />
  );
};

export default VoucherTable;
