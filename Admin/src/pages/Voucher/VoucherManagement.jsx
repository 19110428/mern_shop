import { useCallback, useEffect, useMemo, useState } from "react";
import {
  _getVouchers,
  editVoucher,
  newVoucher,
  removeVoucher,
} from "../../redux/voucher/voucherApi";
import ContentWrapper from "../../components/Layout/Components/ContentWrapper";
import { ConfirmationModal } from "../../components/Modal";
import { toast } from "react-toastify";
import VoucherModificationModal from "./Components/VoucherModificationModal";
import VoucherHeaderAction from "./Components/VoucherHeaderAction";
import Datatable from "./Components/VoucherTable";

const VoucherManagement = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [selectedVoucherId, setSelectedVoucherId] = useState(null);
  const [voucherData, setVoucherData] = useState([]);
  const [isShowModificationModal, setIsShowModificationModal] = useState(false);
  const [isShowDeleteModal, setIsShowDeleteModal] = useState(false);

  const selectedVoucher = useMemo(() => {
    return voucherData.find((item) => item._id === selectedVoucherId) ?? null;
  }, [selectedVoucherId, voucherData]);

  const handleClickAddButton = useCallback(() => {
    setIsShowModificationModal(true);
  }, []);

  const handleClickEditButton = useCallback((id) => {
    setSelectedVoucherId(id ?? null);
    setIsShowModificationModal(true);
  }, []);

  const handleClickDeleteButton = useCallback((id) => {
    setSelectedVoucherId(id ?? null);
    setIsShowDeleteModal(true);
  }, []);

  const fetchData = useCallback(async () => {
    setIsLoading(true);

    try {
      const data = await _getVouchers();

      setVoucherData(data);
      setIsLoading(false);
    } catch (error) {
      console.log(error.response);
      // toast.error("An unknown error occurred while processing your request.");
    }
  }, []);

  const handleDelete = useCallback(async () => {
    if (!selectedVoucher) {
      return;
    }

    try {
      await removeVoucher(selectedVoucher?._id);

      toast.success("The voucher has been deleted successfully.");

      fetchData();
    } catch (error) {
      toast.error(
        "An error occurred while deleting the voucher. Please try again later."
      );
    } finally {
      setIsShowDeleteModal(false);
    }
  }, [selectedVoucher, toast]);

  const handleCloseModal = useCallback(() => {
    setIsShowModificationModal(false);
    setIsShowDeleteModal(false);
    setSelectedVoucherId(null);
  }, []);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  return (
    <ContentWrapper
      title="Voucher management"
      actions={<VoucherHeaderAction onClickAdd={handleClickAddButton} />}
    >
      {!isLoading && (
        <Datatable
          rows={voucherData}
          onClickEdit={handleClickEditButton}
          onClickDelete={handleClickDeleteButton}
        />
      )}

      <ConfirmationModal
        message="Are you sure you want to delete this voucher? This action cannot be undone."
        isOpen={isShowDeleteModal}
        status="danger"
        title={
          <div>
            Delete voucher
            <span className="mx-1 inline-block font-semibold text-red-500">
              {selectedVoucher?.title}
            </span>
          </div>
        }
        onClose={handleCloseModal}
        onConfirm={handleDelete}
      />

      <VoucherModificationModal
        isOpen={isShowModificationModal}
        voucher={selectedVoucher}
        onCreate={newVoucher}
        onCreated={fetchData}
        onEdit={editVoucher}
        onEdited={fetchData}
        onClose={handleCloseModal}
      />
    </ContentWrapper>
  );
};

export default VoucherManagement;
