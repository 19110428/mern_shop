import { useCallback, useEffect, useMemo, useState } from "react";
import { editOrder, getAllOrders } from "../../redux/order/ordersApi";
import ContentWrapper from "../../components/Layout/Components/ContentWrapper";
import OrderModificationModal from "./Components/OrderModificationModal";
import OrderTable from "./Components/OrderTable";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { InformationModal } from "../../components/Modal";
import OrderDetail from "./Components/OrderDetail";
import OrderStatus from "./Components/OrderStatus";

const OrdeManagement = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [selectedOrderId, setSelectedOrderId] = useState(null);
  const [orderData, setOrderData] = useState([]);
  const [isShowModificationModal, setIsShowModificationModal] = useState(false);
  const [isShowDetailModal, setIsShowDetailModal] = useState(false);

  const dispatch = useDispatch();

  const selectedOrder = useMemo(() => {
    return orderData.find((item) => item._id === selectedOrderId) ?? null;
  }, [selectedOrderId, orderData]);

  const handleClickViewButton = useCallback((id) => {
    setSelectedOrderId(id ?? null);
    setIsShowDetailModal(true);
  }, []);

  const handleClickEditButton = useCallback((id) => {
    setSelectedOrderId(id ?? null);
    setIsShowModificationModal(true);
  }, []);

  const fetchData = useCallback(async () => {
    setIsLoading(true);

    try {
      const data = await getAllOrders(dispatch);

      setOrderData(data);
      setIsLoading(false);
    } catch (error) {
      toast.error("An unknown error occurred while processing your request.");
    }
  }, []);

  const handleCloseModal = useCallback(() => {
    setIsShowModificationModal(false);
    setIsShowDetailModal(false);
    setSelectedOrderId(null);
  }, []);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  return (
    <ContentWrapper title="Order management">
      {!isLoading && (
        <OrderTable
          rows={orderData}
          onClickView={handleClickViewButton}
          onClickEdit={handleClickEditButton}
        />
      )}

      <OrderModificationModal
        isOpen={isShowModificationModal}
        order={selectedOrder}
        onEdit={editOrder}
        onEdited={fetchData}
        onClose={handleCloseModal}
      />

      <InformationModal
        isOpen={isShowDetailModal}
        className="w-fit"
        title={
          <div className="flex space-x-4 items-center">
            <span>Chi tiết đơn hàng</span>
            <span className="font-semibold text-red-500">
              {selectedOrder?._id}
            </span>
            <OrderStatus
              type={selectedOrder?.status}
              size="md"
              message={selectedOrder?.status}
            />
          </div>
        }
        onClose={handleCloseModal}
      >
        <OrderDetail selectedOrder={selectedOrder} />
      </InformationModal>
    </ContentWrapper>
  );
};

export default OrdeManagement;
