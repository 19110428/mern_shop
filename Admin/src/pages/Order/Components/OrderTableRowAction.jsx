import { MdEdit, MdOutlineRemoveRedEye } from "react-icons/md";
import TableRowAction from "../../../Components/TableNew/TableRowAction";

const OrderTableRowAction = ({ id, onClickView, onClickEdit }) => {
  return (
    <div className="flex items-center justify-end space-x-2">
      <TableRowAction id={id} title="Xem chi tiết" onClick={onClickView}>
        <MdOutlineRemoveRedEye size={16} />
      </TableRowAction>
      <TableRowAction id={id} title="Thay đổi" onClick={onClickEdit}>
        <MdEdit size={16} />
      </TableRowAction>
    </div>
  );
};

export default OrderTableRowAction;
