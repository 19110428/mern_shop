import OrderTableRowAction from "./OrderTableRowAction";
import Table from "../../../components/TableNew/Table";
import OrderStatus from "./OrderStatus";
import dayjs from "dayjs";

const OrderTable = ({ rows, onClickView, onClickEdit }) => {
  const orderColumns = [
    {
      field: "_id",
      headerName: "ID",
      width: 150,
    },
    {
      field: "totalQuantity",
      headerName: "Quantity",
      width: 150,
    },
    {
      field: "totalPrice",
      headerName: "Total",
      width: 150,
    },
    {
      field: "status",
      headerName: "Status",
      width: 200,
      renderCell: (params) => {
        return (
          <OrderStatus type={params.row.status} message={params.row.status} />
        );
      },
    },
    {
      field: "createdAt",
      headerName: "Create At",
      width: 250,
      renderCell: (params) => {
        return (
          <div>{dayjs(params.row.createdAt).format("h:mm A, M/D/YYYY")}</div>
        );
      },
    },
  ];

  const actionColumn = [
    {
      field: "action",
      headerName: "",
      width: 100,
      renderCell: (params) => {
        return (
          <OrderTableRowAction
            id={params.row._id}
            onClickView={onClickView}
            onClickEdit={onClickEdit}
          />
        );
      },
    },
  ];
  return (
    <Table rows={rows} dataColumns={orderColumns} actionColumn={actionColumn} />
  );
};

export default OrderTable;
