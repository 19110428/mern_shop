import { useEffect, useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import Button from "../../../components/Button/Button";
import { Input, Select } from "../../../components/Form";
import { Modal } from "../../../components/Modal";
import { toast } from "react-toastify";
import { values } from "lodash";

const status = [
  "Đang xử lý",
  "Chờ thanh toán",
  "Đang vận chuyển",
  "Đã giao",
  "Đã hủy",
];

const OrderModificationModal = ({
  isOpen,
  order,
  onClose,
  onEdit,
  onEdited,
  ...props
}) => {
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [statusOptions, setStatusOptions] = useState([]);

  const {
    control,
    reset,
    setValue,
    handleSubmit: useFormSubmit,
    ...methods
  } = useForm();

  const handleSubmit = useFormSubmit(async (formData) => {
    setIsSubmitting(true);

    try {
      await onEdit({ ...order, status: formData.status });
      toast.success("The status has been updated successfully.");
      onEdited();
      onClose();
    } catch (error) {
      toast.error("An unknown error occurred while processing your request.");
    } finally {
      setIsSubmitting(false);
    }
  });

  useEffect(() => {
    if (!isOpen) {
      return;
    }

    setIsSubmitting(false);

    reset(order);
  }, [isOpen, reset, order]);

  useEffect(() => {
    setStatusOptions(
      values(status).map((group) => ({
        value: group,
        label: group,
      }))
    );
  }, []);

  return (
    <Modal
      isLoading={isSubmitting}
      isOpen={isOpen}
      title={"Cập nhật trạng thái đơn hàng"}
      onClose={onClose}
      onConfirm={handleSubmit}
      {...props}
    >
      <FormProvider
        control={control}
        handleSubmit={useFormSubmit}
        setValue={setValue}
        reset={reset}
        {...methods}
      >
        <Input
          className="block"
          control={control}
          disabled
          label="ID"
          name="_id"
        />
        <div className="mt-6">
          <Select
            className="text-normal"
            control={control}
            isDisabled={isSubmitting}
            name="status"
            options={statusOptions}
            placeholder="Trạng thái"
          />
        </div>
        <Button type="submit" className="hidden">
          Confirm
        </Button>
      </FormProvider>
    </Modal>
  );
};

export default OrderModificationModal;
