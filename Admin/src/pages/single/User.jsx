import "./single.scss";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Chart from "../../components/chart/Chart";
import List from "../../components/table/Table";
import { useDispatch, useSelector } from "react-redux";
import { _getUser, _getUsers } from "../../redux/user/userApi";
import { useNavigate } from "react-router-dom";
import { createAxios } from "../../api/createInstance.js";
import { login } from "../../redux/user/userSlice";
import { _getAllOrdersById } from "../../redux/order/ordersApi";

const User = () => {
  const { userId } = useParams();
  const [data, setData] = useState([]);
  const [editMode, setEditMode] = useState(false);

  const currentUser = useSelector((state) => state.users?.current?.data);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  let axiosJWT = createAxios(currentUser, dispatch, login);
  console.log(userId);

  useEffect(() => {
    _getUser(dispatch, userId, axiosJWT);
    _getAllOrdersById(dispatch, userId);
  }, []);
  const temp = useSelector((state) => state.users?.user?.data);
  let user = temp[0];
  if (temp === null) user = [];
  const rows = useSelector((state) => state.orders?.order?.data);
  console.log(rows);
  const [userName, setUserName] = useState();
  const [userGender, setUserGender] = useState("");
  const [userPhoneNumber, setUserPhoneNumber] = useState("");
  const [userAddress, setUserAddress] = useState("");

  const handleEdit = (e) => {
    e.preventDefault();
    if (editMode === true) {
      setEditMode(false);
    } else setEditMode(true);
  };
  const handleSubmitEdit = (e) => {
    e.preventDefault();
  };
  return (
    <div className="single">
      <div className="singleContainer">
        <div className="top">
          <div className="left">
            <div className="editButton">Edit</div>
            <h1 className="title">Information</h1>
            <div className="item">
              <img src={user?.image} alt="" className="itemImg" />
              <div className="details">
                <h1 className="itemTitle">{user?.fullname}</h1>
                <div className="detailItem">
                  <span className="itemKey">Email:</span>
                  <span className="itemValue">{user?.username}</span>
                </div>
                <div className="detailItem">
                  <span className="itemKey">Phone:</span>
                  <span className="itemValue">{user?.phone}</span>
                </div>
                <div className="detailItem">
                  <span className="itemKey">Address:</span>
                  <span className="itemValue">{user?.address?.address}</span>
                </div>
                <div className="detailItem">
                  <span className="itemKey">Gender:</span>
                  <span className="itemValue">{user?.gender}</span>
                </div>
              </div>
            </div>
          </div>
          <div className="right">
            <Chart aspect={3 / 1} title="User Spending ( Last 6 Months)" />
          </div>
        </div>
        <div className="bottom">
          <h1 className="title">Last Transactions</h1>
          <List rows={rows} />
        </div>
      </div>
    </div>
  );
};

export default User;
