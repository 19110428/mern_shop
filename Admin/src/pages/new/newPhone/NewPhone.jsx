import "../new.scss";
import DriveFolderUploadOutlinedIcon from "@mui/icons-material/DriveFolderUploadOutlined";
import { useState, useEffect } from "react";
import { productInputs, phoneProduct, watchProduct, smartWatchProduct, laptopProduct, typeAccessory } from "../../../formSource";
import {ref, uploadBytes, getDownloadURL,  listAll, list,} from 'firebase/storage';
import { storage } from '../../../utils/firebase';
import { v4 } from 'uuid';
import { newProduct } from "../../../redux/product/productApi";
import { useDispatch } from "react-redux";
import  Axios  from "axios";
import { async } from "@firebase/util";
import {toast} from "react-toastify"
import { useNavigate } from "react-router-dom";
const NewPhone = ({ inputs, title }) => {
    const [file, setFile] = useState(null)
    const [img,setImg] =useState('')
    const [arrFile, setArrFile] = useState([])
    const [type, setType] = useState(0)
    const [typeA,setTypeA] = useState(0)
    const [imageUrls, setImageUrls] = useState([]);
    const imagesListRef = ref(storage, 'images/');
    const dispatch= useDispatch()
    const navigate = useNavigate()
    useEffect(() => {
        listAll(imagesListRef).then((response) => {
            response.items.forEach((item) => {
                getDownloadURL(item).then((url) => {
                    // let curentArr = imageUrls.filter(item => item === url)
                    setImageUrls(url);
                });
            });
        });
    }, []);
    const handleChangeSlug =(titleP, value)=>{
        if(titleP === 'title'){
            document.getElementById('slug').value=parseStringToSlug(value)
        }
    }
    const parseStringToSlug = (string) => {
        return string.toLowerCase().split(' ').join('-')
    }
    const generateString = (length) => {
        const characters = '0123456789';
        let result = ' ';
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        return parseInt(result);
    }

 

    const handleSubmit = async(e) => {
        e.preventDefault()
        if (file == null) return;
        let temp =""
        const formData = new FormData()
        formData.append("file",file)
        formData.append("upload_preset","wd2jb4z6")
        await Axios.post("https://api.cloudinary.com/v1_1/dbumt7w6p/image/upload",formData)
            .then((res)=>{
                // setImg(res.data.secure_url)
                temp=res.data.secure_url
            })
        const title = document.getElementById('title').value
        const price = document.getElementById('price').value
        const brand = document.getElementById('brand').value
        const discount = document.getElementById('discount').value
        const colors = document.getElementById('colors').value
        const info = document.getElementById('info').value
        const slug = document.getElementById('slug').value
        let dataPhone = {
            
        }
        let parameter = {

        }
        if (type === 1) {
            const screen = document.getElementById('screen').value
            const os = document.getElementById('os').value
            const cameraBehind = document.getElementById('cameraBehind').value
            const cameraBefore = document.getElementById('cameraBefore').value
            const chip = document.getElementById('chip').value
            const sim = document.getElementById('sim').value
            const pin = document.getElementById('pin').value
            const ram = document.getElementById('ram').value
            const rom = document.getElementById('rom').value
            parameter = {
                'SCREEN': screen,
                "Hệ điều hành": os,
                "Camera sau": cameraBehind,
                "Camera trước": cameraBefore,
                "Chip": chip,
                "RAM": [
                    ram
                ],
                "ROM": rom,
                "SIM": sim,
                "Pin, Sạc": pin
            }
            dataPhone = {
                "id": generateString(4),
                "img": temp,
                "title": title,
                "price": parseInt(price),
                "url": "/dtdd/"+slug,
                "slug": slug,
                "promotion": "Trả góp 0%",
                "discount": parseInt(discount),
                "tag": "Ưu đãi sinh nhật",
                "gift": "",
                "star": 0,
                "totalVote": 0,
                "brand": brand,
                "category": inputs[type - 1].type,
                "brandId": 1,
                "cateId": 1,
                "baohanh": "18T",
                "new": true,
                "location": "Tỉnh Long Xuyên",
                "gallery": [
                    "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/2-1020x570.jpg",
                    "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/2-1020x570.jpg",
                    "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/3-1020x570.jpg",
                    "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/SamsungGalaxyM53-1020x570.jpg",
                    "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/5-1020x570.jpg",
                    "https://cdn.tgdd.vn/mwgcart/mwgcore/ContentMwg/images/bg_csmh_tgdd-min.png?v=11"
                ],
                "colors": [
                    colors
                ],
                "RAM": [
                    ram
                ],
                "ROM": rom,
                "parameter": {
                    ...parameter
                },
                "info": info
            }
            
        } else if (type === 2) {
            const screen = document.getElementById('screen').value
            const os = document.getElementById('os').value
            const chip = document.getElementById('chip').value
            const pin = document.getElementById('pin').value
            const graphicCard = document.getElementById('graphicCard').value
            const design = document.getElementById('design').value
            const portConect = document.getElementById('portConect').value
            const ram = document.getElementById('ram').value
            const rom = document.getElementById('rom').value
            parameter = {
                
                'SCREEN': screen,
                "Hệ điều hành": os,
                "Card màn hình": graphicCard,
                "Thiết kế": design,
                "Chip": chip,
                "RAM": [
                    ram
                ],
                "ROM": rom,
                "Pin, Sạc": pin,
                "Cổng kết nối": portConect
            }
            dataPhone = {
                "id": generateString(4),
                "img": temp,
                "title": title,
                "price": parseInt(price),
                "url": "/dtdd/"+slug,
                "slug": slug,
                "promotion": "Trả góp 0%",
                "discount": parseFloat(discount),
                "tag": "Ưu đãi sinh nhật",
                "gift": "",
                "star": 0,
                "totalVote": 0,
                "brand": brand,
                "category": "Phone",
                "baohanh": "18T",
                "new": true,
                "location": "Tỉnh Long Xuyên",
                "gallery": [
                    "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/2-1020x570.jpg",
                    "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/2-1020x570.jpg",
                    "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/3-1020x570.jpg",
                    "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/SamsungGalaxyM53-1020x570.jpg",
                    "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/5-1020x570.jpg",
                    "https://cdn.tgdd.vn/mwgcart/mwgcore/ContentMwg/images/bg_csmh_tgdd-min.png?v=11"
                ],
                "colors": [
                    colors
                ],
                "RAM": [
                    ram
                ],
                "ROM": rom,
                "parameter": {
                    ...parameter
                },
                "info": info
            }
        } else if (type === 3) {
            dataPhone = {
                "id": generateString(4),
                "img": temp,
                "title": title,
                "price": parseInt(price),
                "url": "/dtdd/"+slug,
                "slug": slug,
                "promotion": "Trả góp 0%",
                "discount": parseFloat(discount),
                "tag": "Ưu đãi sinh nhật",
                "gift": "",
                "star": 0,
                "totalVote": 0,
                "brand": brand,
                "category": inputs[type - 1].type,
                "gallery": [
                    "https://cdn.tgdd.vn/mwgcart/mwgcore/ContentMwg/images/bg_csmh_tgdd-min.png?v=11"
                ],
                "nameType": typeAccessory[typeA - 1].title,
                "info": info
            }
        }

       
        toast.success("Add product Success!",{})

        await newProduct(dispatch,dataPhone)

        navigate('/products')
    }
    return (
        <div className="new">
            <div className="newContainer">
                <div className="top">
                    <h1>{title}</h1>
                </div>
                <div className="bottom">
                    <div className="left">
                        <img
                            src={
                                file
                                    ? URL.createObjectURL(file)
                                    : "https://icon-library.com/images/no-image-icon/no-image-icon-0.jpg"
                            }
                            alt=""
                        />

                    </div>
                    <div className="right">
                        <form onSubmit={handleSubmit}>
                            <div className="formInput">
                                <label htmlFor="file">
                                    Image: <DriveFolderUploadOutlinedIcon className="icon" />
                                </label>
                                <input
                                    type="file"
                                    id="file"
                                    onChange={(e) => { setFile(e.target.files[0]) }}
                                    style={{ display: "none" }}
                                />
                            </div>

                            {<div className="formContainer">

                                {(productInputs.map((input) => (
                                    <div className="formInput" key={input.id}>
                                        <label>{input.label}</label>
                                        <input type={input.type} placeholder={input.placeholder} id={input.title} onChange={(e)=>handleChangeSlug(input.title, e.target.value)} />
                                    </div>
                                )))}
                                <div className="formInput">
                                    <label htmlFor="file1">
                                        Image: <DriveFolderUploadOutlinedIcon className="icon" />
                                    </label>
                                    <input
                                        type="file"
                                        id="file1"
                                        onChange={(e) => { setArrFile([...arrFile, e.target.files[0]]) }}
                                        style={{ display: "none" }}
                                    />
                                </div>
                                <div className="imgcontent" >
                                    {arrFile.map(img => (
                                        <img
                                            src={
                                                URL.createObjectURL(img)}
                                            alt=""
                                        />
                                    ))}


                                </div>
                                <div style={{
                                    display: 'flex',
                                    width: '100%'
                                }}>
                                    <select className="formInput">
                                        <option onClick={() => setType(0)}>Chọn loại sản phẩm</option>
                                        {inputs.map((type) => (
                                            <option value={type.id} onClick={() => setType(type.id)}>{type.label}</option>
                                        ))}
                                    </select>
                                </div>
                                {(type === 1) && <>
                                    <select className="formInput" id='ram'>
                                        <option >Chọn loại RAM</option>
                                        <option>4 GB</option>
                                        <option>6 GB</option>
                                        <option>8 GB</option>
                                        <option>12 GB</option>
                                        <option>16 GB</option>
                                    </select>
                                    <select className="formInput" id='rom'>
                                        <option >Bộ nhớ trong</option>
                                        <option >32 GB</option>
                                        <option >64 GB</option>
                                        <option >128 GB</option>
                                        <option >256 GB</option>
                                        <option >512 GB</option>
                                        <option >1 TB</option>
                                    </select>
                                    {phoneProduct.map(input => (
                                        <div className="formInput" key={input.id}>
                                            <label>{input.label}</label>
                                            <input type={input.type} placeholder={input.placeholder} id={input.title} />
                                        </div>
                                    ))}
                                </>
                                }
                                {(type === 2) && <>
                                    <select className="formInput" id='ram'>
                                        <option >Chọn loại RAM</option>
                                        <option>4 GB</option>
                                        <option>6 GB</option>
                                        <option>8 GB</option>
                                        <option>12 GB</option>
                                        <option>16 GB</option>
                                    </select>
                                    <select className="formInput" id='rom'>
                                        <option >Bộ nhớ trong</option>
                                        <option >32 GB</option>
                                        <option >64 GB</option>
                                        <option >128 GB</option>
                                        <option >256 GB</option>
                                        <option >512 GB</option>
                                        <option >1 TB</option>
                                    </select>
                                    {laptopProduct.map(input => (
                                        <div className="formInput" key={input.id}>
                                            <label>{input.label}</label>
                                            <input type={input.type} placeholder={input.placeholder} id={input.title} />
                                        </div>
                                    ))}
                                </>
                                }
                                {(type === 3) && <>
                                    <select className="formInput">
                                        <option onClick={() => setTypeA(0)}>Chọn loại phụ kiện</option>
                                        {typeAccessory.map((type) => (
                                            <option value={type.id} onClick={() => setTypeA(type.id)}>{type.label}</option>
                                        ))}
                                    </select>
                                </>
                                }
                                {type === 4 && <>
                                    {watchProduct.map(input => (
                                        <div className="formInput" key={input.id}>
                                            <label>{input.label}</label>
                                            <input type={input.type} placeholder={input.placeholder} id={input.title} />
                                        </div>
                                    ))}
                                </>
                                }
                                {type === 5 && <>
                                    {smartWatchProduct.map(input => (
                                        <div className="formInput" key={input.id}>
                                            <label>{input.label}</label>
                                            <input type={input.type} placeholder={input.placeholder} id={input.title} />
                                        </div>
                                    ))}
                                </>
                                }
                            </div>
                            }

                            <button >Send</button>
                        </form>
                    </div>
                </div>
            </div>
        </div >
    );
};

export default NewPhone;
