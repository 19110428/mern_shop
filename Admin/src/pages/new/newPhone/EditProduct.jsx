import "../new.scss";
import DriveFolderUploadOutlinedIcon from "@mui/icons-material/DriveFolderUploadOutlined";
import { useState, useEffect } from "react";
import {
  productInputs,
  typeProduct,
  userInputs,
  phoneProduct,
  laptopProduct,
  typeAccessory,
  watchProduct,
  smartWatchProduct,
} from "../../../formSource";
import { renderMatches, useParams } from "react-router-dom";
import { ProductService } from "~/services";
import { _getProduct } from "../../../redux/product/productApi";
import { useDispatch, useSelector } from "react-redux";

const EditProduct = ({ inputs, title }) => {
  const param = useParams();
  const [file, setFile] = useState("");
  const [arrFile, setArrFile] = useState([]);
  const [type, setType] = useState(0);
  const [isLoading, setLoad] = useState(true);
  const dispatch = useDispatch()
  
  useEffect(()=>{
    _getProduct(dispatch,param.productId)
  },[])
  const product = useSelector((state) => state.products?.product?.data);

  let dataPhone = {
    id: 1,
    title: "Samsung Galaxy M53",
    price: "12490000",
    url: "/dtdd/samsung-galaxy-m53",
    slug: "samsung-galaxy-m53",
    promotion: "Trả góp 0%",
    discount: 0.2,
    tag: "Ưu đãi sinh nhật",
    gift: "",
    star: 4.1,
    totalVote: 23,
    brand: "samsung",
    category: "dienthoai",
    brandId: 1,
    cateId: 1,
    baohanh: "18T",
    new: true,
    location: "Tỉnh Long Xuyên",
    gallery: [
      "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/2-1020x570.jpg",
      "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/2-1020x570.jpg",
      "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/3-1020x570.jpg",
      "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/SamsungGalaxyM53-1020x570.jpg",
      "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/5-1020x570.jpg",
      "https://cdn.tgdd.vn/mwgcart/mwgcore/ContentMwg/images/bg_csmh_tgdd-min.png?v=11",
    ],
    colors: ["Nâu", "Xanh dương", "Xanh lá"],
    RAM: ["4 GB", "8 GB"],
    ROM: "128 GB",
    nameType: "tainghe",
    parameter: {
      img: "https://cdn.tgdd.vn/Products/Images/42/247364/Kit/samsung-galaxy-m53-note.jpg",
      SCREEN: "IPS LCD6.71 HD+",
      "Hệ điều hành": "Android 11",
      "Camera sau": "Chính 13 MP & Phụ 2 MP",
      "Camera trước": "5 MP",
      Chip: "JLQ JR510 8 nhân",
      RAM: ["4 GB", "8 GB"],
      ROM: "64 GB",
      SIM: "2 Nano SIMHỗ trợ 4G",
      "Pin, Sạc": "6000 mAh18 W",
    },
    info: "<h3>Điện thoại iPhone 13 Pro Max 128 GB - siêu phẩm được mong chờ nhất ở nửa cuối năm 2021 đến từ Apple. Máy có thiết kế không mấy đột phá khi so với người tiền nhiệm, bên trong đây vẫn là một sản phẩm có màn hình siêu đẹp, tần số quét được nâng cấp lên 120 Hz mượt mà, cảm biến camera có kích thước lớn hơn, cùng hiệu năng mạnh mẽ với sức mạnh đến từ Apple A15 Bionic, sẵn sàng cùng bạn chinh phục mọi thử thách.</h3><h3>Thiết kế đẳng cấp hàng đầu</h3><p>iPhone mới kế thừa thiết kế đặc trưng từ iPhone 12 Pro Max khi sở hữu khung viền vuông vức, mặt lưng kính cùng màn hình tai thỏ tràn viền nằm ở phía trước.</p>",
  };
  const [data, setData] = useState([dataPhone]);

  useEffect(() => {
    
      setData(product);
      document.getElementById("title").value = product?.title;
      document.getElementById("price").value = product?.price;
      document.getElementById("brand").value = product?.brand;
      document.getElementById("discount").value = product?.discount;
      document.getElementById("colors").value = product?.colors;
      document.getElementById("info").value = product?.info;
      document.getElementById("typeproduct").value = product?.category;
      document.getElementById("slug").value = product?.slug;

      if (product.category === "dienthoai") {
        document.getElementById("ram").value = product?.RAM;
        document.getElementById("rom").value = product?.ROM;
        document.getElementById("screen").value = product?.parameter.SCREEN;
        document.getElementById("os").value =
          product?.parameter["Hệ điều hành"];
        document.getElementById("cameraBehind").value =
          product?.parameter["Camera sau"];
        document.getElementById("cameraBefore").value =
          product?.parameter["Camera trước"];
        document.getElementById("chip").value = product?.parameter["Chip"];
        document.getElementById("sim").value = product?.parameter["SIM"];
        document.getElementById("pin").value =
          product?.parameter["Pin, Sạc"];
      } else if (product.category === "laptop") {
        console.log(product?.ROM);
        document.getElementById("ram").value = product?.RAM;
        document.getElementById("rom").value = product?.ROM;
        document.getElementById("screen").value = product?.parameter.SCREEN;
        document.getElementById("os").value =
          product?.parameter["Hệ điều hành"];
        document.getElementById("chip").value = product?.parameter["Chip"];
        document.getElementById("pin").value =
          product?.parameter["Pin, Sạc"];
        document.getElementById("graphicCard").value =
          product?.parameter["Card màn hình"];
        document.getElementById("design").value =
          product?.parameter["Thiết kế"];
        document.getElementById("portConect").value =
          product?.parameter["Cổng kết nối"];
      } else if (product.category === "phukien") {
        document.getElementById("typeA").value = product?.nameType;
      } else if (product.category === "watch") {
        document.getElementById("sex").value =
          product?.parameter["Giới tính"];
        document.getElementById("diameter").value =
          product?.parameter["Đường kính mặt"];
        document.getElementById("typeFaceWatch").value =
          product?.parameter["Loại mặt đồng hồ"];
        document.getElementById("material").value =
          product?.parameter["Chất liệu dây"];
        document.getElementById("batery").value =
          product?.parameter["Bộ máy"];
        document.getElementById("waterProof").value =
          product?.parameter["Chống nước"];
      } else if (product.category === "smartwatch") {
        document.getElementById("sex").value =
          product?.parameter["Giới tính"];
        document.getElementById("face").value = product?.parameter["Mặt"];
        document.getElementById("conectOS").value =
          product?.parameter["Kết nối hệ điều hành"];
        document.getElementById("battery").value =
          product?.parameter["Thời lượng pin"];
        document.getElementById("healcare").value =
          product?.parameter["Tính năng cho sức khỏe"];
      }
      setArrFile(product.gallery);
   
    function getProducts() {}
    setLoad(false);
    getProducts();
  }, [isLoading]);

  const handleSubmit = (e) => {
    e.preventDefault();
    const title = document.getElementById("title").value;
    const price = document.getElementById("price").value;
    const brand = document.getElementById("brand").value;
    const discount = document.getElementById("discount").value;
    const colors = document.getElementById("colors").value;
    const info = document.getElementById("info").value;
    const slug = document.getElementById("slug").value;
    let dataPhone = {};
    let parameter = {};
    if (data[0].category === "dienthoai") {
      const screen = document.getElementById("screen").value;
      const os = document.getElementById("os").value;
      const cameraBehind = document.getElementById("cameraBehind").value;
      const cameraBefore = document.getElementById("cameraBefore").value;
      const chip = document.getElementById("chip").value;
      const sim = document.getElementById("sim").value;
      const pin = document.getElementById("pin").value;
      const ram = document.getElementById("ram").value;
      const rom = document.getElementById("rom").value;
      parameter = {
        SCREEN: screen,
        "Hệ điều hành": os,
        "Camera sau": cameraBehind,
        "Camera trước": cameraBefore,
        Chip: chip,
        RAM: [ram],
        ROM: rom,
        SIM: sim,
        "Pin, Sạc": pin,
      };
      dataPhone = {
        title: title,
        price: parseInt(price),
        url: "/dtdd/" + slug,
        slug: slug,
        promotion: "Trả góp 0%",
        discount: parseFloat(discount),
        tag: "Ưu đãi sinh nhật",
        gift: "",
        star: 4.1,
        totalVote: 23,
        brand: brand,
        brandId: 1,
        cateId: 1,
        baohanh: "18T",
        new: true,
        location: "Tỉnh Long Xuyên",
        gallery: [
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/2-1020x570.jpg",
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/2-1020x570.jpg",
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/3-1020x570.jpg",
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/SamsungGalaxyM53-1020x570.jpg",
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/5-1020x570.jpg",
          "https://cdn.tgdd.vn/mwgcart/mwgcore/ContentMwg/images/bg_csmh_tgdd-min.png?v=11",
        ],
        colors: [colors],
        RAM: [ram],
        ROM: rom,
        parameter: {
          ...parameter,
        },
        info: info,
      };
    } else if (data[0].category === "laptop") {
      const screen = document.getElementById("screen").value;
      const os = document.getElementById("os").value;
      const chip = document.getElementById("chip").value;
      const pin = document.getElementById("pin").value;
      const graphicCard = document.getElementById("graphicCard").value;
      const design = document.getElementById("design").value;
      const portConect = document.getElementById("portConect").value;
      const ram = document.getElementById("ram").value;
      const rom = document.getElementById("rom").value;
      parameter = {
        SCREEN: screen,
        "Hệ điều hành": os,
        "Card màn hình": graphicCard,
        "Thiết kế": design,
        Chip: chip,
        RAM: [ram],
        ROM: rom,
        "Pin, Sạc": pin,
        "Cổng kết nối": portConect,
      };
      dataPhone = {
        title: title,
        price: parseInt(price),
        url: "/dtdd/" + slug,
        slug: slug,
        promotion: "Trả góp 0%",
        discount: parseFloat(discount),
        tag: "Ưu đãi sinh nhật",
        gift: "",
        star: 4.1,
        totalVote: 23,
        brand: brand,
        brandId: 1,
        cateId: 1,
        baohanh: "18T",
        new: true,
        location: "Tỉnh Long Xuyên",
        gallery: [
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/2-1020x570.jpg",
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/2-1020x570.jpg",
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/3-1020x570.jpg",
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/SamsungGalaxyM53-1020x570.jpg",
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/5-1020x570.jpg",
          "https://cdn.tgdd.vn/mwgcart/mwgcore/ContentMwg/images/bg_csmh_tgdd-min.png?v=11",
        ],
        colors: [colors],
        RAM: [ram],
        ROM: rom,
        parameter: {
          ...parameter,
        },
        info: info,
      };
    } else if (data[0].category === "phukien") {
      const typeA = document.getElementById("typeA").value;
      dataPhone = {
        title: title,
        price: parseInt(price),
        url: "/dtdd/" + slug,
        slug: slug,
        promotion: "Trả góp 0%",
        discount: parseFloat(discount),
        tag: "Ưu đãi sinh nhật",
        gift: "",
        star: 4.1,
        totalVote: 23,
        brand: brand,
        nameType: typeA,
        baohanh: "18T",
        new: true,
        location: "Tỉnh Long Xuyên",
        gallery: [
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/2-1020x570.jpg",
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/2-1020x570.jpg",
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/3-1020x570.jpg",
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/SamsungGalaxyM53-1020x570.jpg",
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/5-1020x570.jpg",
          "https://cdn.tgdd.vn/mwgcart/mwgcore/ContentMwg/images/bg_csmh_tgdd-min.png?v=11",
        ],
        colors: [colors],
        info: info,
      };
    } else if (data[0].category === "watch") {
      const sex = document.getElementById("sex").value;
      const diameter = document.getElementById("diameter").value;
      const typeFaceWatch = document.getElementById("typeFaceWatch").value;
      const material = document.getElementById("material").value;
      const batery = document.getElementById("batery").value;
      const waterProof = document.getElementById("waterProof").value;
      parameter = {
        "Giới tính": sex,
        "Đường kính mặt": diameter,
        "Loại mặt đồng hồ": typeFaceWatch,
        "Chất liệu dây": material,
        "Bộ máy": batery,
        "Chống nước": waterProof,
      };
      dataPhone = {
        title: title,
        price: parseInt(price),
        url: "/dtdd/" + slug,
        slug: slug,
        promotion: "Trả góp 0%",
        discount: parseFloat(discount),
        tag: "Ưu đãi sinh nhật",
        gift: "",
        star: 0,
        totalVote: 0,
        brand: brand,
        brandId: 1,
        cateId: 1,
        baohanh: "18T",
        new: true,
        location: "Tỉnh Long Xuyên",
        gallery: [
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/2-1020x570.jpg",
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/2-1020x570.jpg",
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/3-1020x570.jpg",
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/SamsungGalaxyM53-1020x570.jpg",
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/5-1020x570.jpg",
          "https://cdn.tgdd.vn/mwgcart/mwgcore/ContentMwg/images/bg_csmh_tgdd-min.png?v=11",
        ],
        colors: [colors],
        parameter: {
          ...parameter,
        },
        info: info,
      };
    } else if (data[0].category === "smartwatch") {
      const sex = document.getElementById("sex").value;
      const face = document.getElementById("face").value;
      const conectOS = document.getElementById("conectOS").value;
      const battery = document.getElementById("battery").value;
      const healcare = document.getElementById("healcare").value;
      parameter = {
        "Giới tính": sex,
        Mặt: face,
        "Kết nối hệ điều hành": conectOS,
        "Thời lượng pin": battery,
        "Tính năng cho sức khỏe": healcare,
      };
      dataPhone = {
        title: title,
        price: parseInt(price),
        url: "/dtdd/" + slug,
        slug: slug,
        promotion: "Trả góp 0%",
        discount: parseFloat(discount),
        tag: "Ưu đãi sinh nhật",
        gift: "",
        star: 0,
        totalVote: 0,
        brand: brand,
        brandId: 1,
        cateId: 1,
        baohanh: "18T",
        new: true,
        location: "Tỉnh Long Xuyên",
        gallery: [
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/2-1020x570.jpg",
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/2-1020x570.jpg",
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/3-1020x570.jpg",
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/SamsungGalaxyM53-1020x570.jpg",
          "https://cdn.tgdd.vn/Products/Images/42/247364/Slider/5-1020x570.jpg",
          "https://cdn.tgdd.vn/mwgcart/mwgcore/ContentMwg/images/bg_csmh_tgdd-min.png?v=11",
        ],
        colors: [colors],
        parameter: {
          ...parameter,
        },
        info: info,
      };
    }

    async function postData(url = "", data = {}) {
      // Default options are marked with *
      const response = await fetch(url, {
        method: "PATCH", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, *cors, same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
          "Content-Type": "application/json",
          // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: "follow", // manual, *follow, error
        referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data), // body data type must match "Content-Type" header
      });
      return response.json(); // parses JSON response into native JavaScript objects
    }

    postData(
      "https://jsonserv.glitch.me/products/" + data[0].id,
      dataPhone
    ).then((data) => {
      console.log(data); // JSON data parsed by `data.json()` call
    });
    alert("Sửa thành công");
  };
  const handleDeleteImg = (index) => {
    console.log(index);
    let newArr = arrFile.filter((id) => id !== index);
    setArrFile(newArr);
    console.log(newArr);
  };
  return (
    <div className="new">
      <div className="newContainer">
        <div className="top">
          <h1>{title}</h1>
        </div>
        <div className="bottom">
          <div className="left">
            <img src={product.img} alt="" />
          </div>
          <div className="right">
            <form onSubmit={handleSubmit}>
              <div className="formInput">
                <label htmlFor="file">
                  Image: <DriveFolderUploadOutlinedIcon className="icon" />
                </label>
                <input
                  type="file"
                  id="file"
                  onChange={(e) => {
                    setFile(e.target.files[0]);
                  }}
                  style={{ display: "none" }}
                />
              </div>
              {
                <div className="formContainer">
                  {productInputs.map((input) => {
                    return (
                      <div className="formInput" key={input.id}>
                        <label>{input.label}</label>
                        <input
                          type={input.type}
                          placeholder={input.placeholder}
                          id={input.title}
                        />
                      </div>
                    );
                  })}
                  <div className="formInput">
                    <label htmlFor="file1">
                      Image: <DriveFolderUploadOutlinedIcon className="icon" />
                    </label>
                    <input
                      type="file"
                      id="file1"
                      onChange={(e) => {
                        setArrFile([...arrFile, e.target.files[0]]);
                      }}
                      style={{ display: "none" }}
                    />
                  </div>
                  <div className="imgcontent">
                    {arrFile.map((img, index) => (
                      <img
                        src={img}
                        alt=""
                        onClick={() => handleDeleteImg(img)}
                      />
                    ))}
                  </div>
                  <select className="formInput" id="typeproduct">
                    <option onClick={() => setType(0)}>
                      Chọn loại sản phẩm
                    </option>
                    {inputs.map((type) => (
                      <option
                        value={type.type}
                        onChange={() => setType(type.id)}
                      >
                        {type.label}
                      </option>
                    ))}
                  </select>
                  {data[0]?.category === "dienthoai" && (
                    <>
                      <select className="formInput" id="ram">
                        <option id="1">Chọn loại RAM</option>
                        <option value="4 GB">4 GB</option>
                        <option value="6 GB">6 GB</option>
                        <option value="8 GB">8 GB</option>
                        <option value="12 GB">12 GB</option>
                        <option value="16 GB">16 GB</option>
                      </select>
                      <select className="formInput" id="rom">
                        <option>Bộ nhớ trong</option>
                        <option value="32 GB">32 GB</option>
                        <option value="64 GB">64 GB</option>
                        <option value="128 GB">128 GB</option>
                        <option value="256 GB">256 GB</option>
                        <option value="512GB">512 GB</option>
                        <option value="1 TB">1 TB</option>
                      </select>
                      {phoneProduct.map((input) => (
                        <div className="formInput" key={input.id}>
                          <label>{input.label}</label>
                          <input
                            type={input.type}
                            placeholder={input.placeholder}
                            id={input.title}
                          />
                        </div>
                      ))}
                    </>
                  )}
                  {data[0]?.category === "laptop" && (
                    <>
                      <select className="formInput" id="ram">
                        <option id="1">Chọn loại RAM</option>
                        <option value="4 GB">4 GB</option>
                        <option value="6 GB">6 GB</option>
                        <option value="8 GB">8 GB</option>
                        <option value="12 GB">12 GB</option>
                        <option value="16 GB">16 GB</option>
                      </select>
                      <select className="formInput" id="rom">
                        <option>Bộ nhớ trong</option>
                        <option value="32 GB">32 GB</option>
                        <option value="64 GB">64 GB</option>
                        <option value="128 GB">128 GB</option>
                        <option value="256 GB">256 GB</option>
                        <option value="512 GB">512 GB</option>
                        <option value="1 TB">1 TB</option>
                      </select>
                      {laptopProduct.map((input) => (
                        <div className="formInput" key={input.id}>
                          <label>{input.label}</label>
                          <input
                            type={input.type}
                            placeholder={input.placeholder}
                            id={input.title}
                          />
                        </div>
                      ))}
                    </>
                  )}
                  {data[0]?.category === "phukien" && (
                    <>
                      <select className="formInput" id="typeA">
                        <option onClick={() => setTypeA(0)}>
                          Chọn loại phụ kiện
                        </option>
                        {typeAccessory.map((type) => (
                          <option
                            value={type.title}
                            onClick={() => setTypeA(type.id)}
                          >
                            {type.label}
                          </option>
                        ))}
                      </select>
                    </>
                  )}
                  {data[0]?.category === "watch" && (
                    <>
                      {watchProduct.map((input) => (
                        <div className="formInput" key={input.id}>
                          <label>{input.label}</label>
                          <input
                            type={input.type}
                            placeholder={input.placeholder}
                            id={input.title}
                          />
                        </div>
                      ))}
                    </>
                  )}
                  {data[0]?.category === "smartwatch" && (
                    <>
                      {smartWatchProduct.map((input) => (
                        <div className="formInput" key={input.id}>
                          <label>{input.label}</label>
                          <input
                            type={input.type}
                            placeholder={input.placeholder}
                            id={input.title}
                          />
                        </div>
                      ))}
                    </>
                  )}
                </div>
              }

              <button onClick={() => console.log()}>Send</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditProduct;
