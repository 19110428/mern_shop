import { useCallback, useEffect, useMemo, useState } from "react";
import {
  _getProducts,
  editProduct,
  newProduct,
  removeProduct,
} from "../../redux/product/productApi";
import ContentWrapper from "../../components/Layout/Components/ContentWrapper";
import { ConfirmationModal } from "../../components/Modal";
import { toast } from "react-toastify";
import ProductModificationModal from "./Components/ProductModificationModal";
import ProductHeaderActions from "./Components/ProductHeaderAction";
import Datatable from "./Components/ProductTable";
const ProductManagement = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [selectedProductId, setSelectedProductId] = useState(null);
  const [productData, setProductData] = useState([]);
  const [isShowModificationModal, setIsShowModificationModal] = useState(false);
  const [isShowDeleteModal, setIsShowDeleteModal] = useState(false);

  const selectedProduct = useMemo(() => {
    return productData.find((item) => item._id === selectedProductId) ?? null;
  }, [selectedProductId, productData]);

  const handleClickAddButton = useCallback(() => {
    setIsShowModificationModal(true);
  }, []);

  const handleClickEditButton = useCallback((id) => {
    setSelectedProductId(id ?? null);
    setIsShowModificationModal(true);
  }, []);

  const handleClickDeleteButton = useCallback((id) => {
    setSelectedProductId(id ?? null);
    setIsShowDeleteModal(true);
  }, []);

  const fetchData = useCallback(async () => {
    setIsLoading(true);

    try {
      const data = await _getProducts();

      setProductData(data);
      setIsLoading(false);
    } catch (error) {
      toast.error("An unknown error occurred while processing your request.");
    }
  }, []);

  const handleDelete = useCallback(async () => {
    if (!selectedProduct) {
      return;
    }

    try {
      await removeProduct(selectedProduct?._id);

      toast.success("The product has been deleted successfully.");

      fetchData();
    } catch (error) {
      toast.error(
        "An error occurred while deleting the product. Please try again later."
      );
    } finally {
      setIsShowDeleteModal(false);
    }
  }, [selectedProduct, toast]);

  const handleCloseModal = useCallback(() => {
    setIsShowModificationModal(false);
    setIsShowDeleteModal(false);
    setSelectedProductId(null);
  }, []);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  return (
    <ContentWrapper
      title="Product management"
      actions={<ProductHeaderActions onClickAdd={handleClickAddButton} />}
    >
      {!isLoading && (
        <Datatable
          rows={productData}
          onClickEdit={handleClickEditButton}
          onClickDelete={handleClickDeleteButton}
        />
      )}

      <ConfirmationModal
        message="Are you sure you want to delete this product? This action cannot be undone."
        isOpen={isShowDeleteModal}
        status="danger"
        title={
          <div>
            Delete product
            <span className="mx-1 inline-block font-semibold text-red-500">
              {selectedProduct?.title}
            </span>
          </div>
        }
        onClose={handleCloseModal}
        onConfirm={handleDelete}
      />

      <ProductModificationModal
        isOpen={isShowModificationModal}
        product={selectedProduct}
        onCreate={newProduct}
        onCreated={fetchData}
        onEdit={editProduct}
        onEdited={fetchData}
        onClose={handleCloseModal}
      />
    </ContentWrapper>
  );
};

export default ProductManagement;
