import { yupResolver } from "@hookform/resolvers/yup";
import { useCallback, useEffect, useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import Button from "../../../components/Button/Button";
import { Input } from "../../../components/Form";
import { Modal } from "../../../components/Modal";
import { productFormSchema } from "../../../components/Schemas/productFormSchema";
import { toast } from "react-toastify";
import { twMerge } from "tailwind-merge";

const DEFAULT_VALUE = {
  title: "",
  price: "",
  info: "",
  img: "",
  promotion: "",
  discount: "",
  tag: "",
  rating: "",
  category: "",
  brand: "",
};

const ProductModificationModal = ({
  isOpen,
  product,
  onClose,
  onCreate,
  onCreated,
  onEdit,
  onEdited,
  ...props
}) => {
  const [isSubmitting, setIsSubmitting] = useState(false);

  const {
    control,
    reset,
    setValue,
    handleSubmit: useFormSubmit,
    ...methods
  } = useForm({
    resolver: yupResolver(productFormSchema()),
    defaultValues: DEFAULT_VALUE,
  });

  const handleCreateProduct = useCallback(
    async (formData) => {
      try {
        await onCreate(formData);
        toast.success("The product has been updated successfully.");
        onCreated();
        onClose();
      } catch (error) {
        toast.error("An unknown error occurred while processing your request.");
      } finally {
        setIsSubmitting(false);
      }
    },
    [methods.setError, onClose, onCreate, onCreated, toast]
  );

  const handleEditProduct = useCallback(
    async (formData) => {
      if (!product) return;
      try {
        await onEdit(product._id, formData);
        toast.success("The product has been updated successfully.");
        onEdited();
        onClose();
      } catch (error) {
        toast.error("An unknown error occurred while processing your request.");
      } finally {
        setIsSubmitting(false);
      }
    },
    [methods.setError, onClose, onEdit, onEdited, product, toast]
  );

  const handleSubmit = useFormSubmit(async (formData) => {
    setIsSubmitting(true);

    if (!product) {
      handleCreateProduct(formData);
      return;
    }

    handleEditProduct(formData);
  });

  useEffect(() => {
    if (!isOpen) {
      return;
    }

    setIsSubmitting(false);

    if (product) {
      reset(product);
      return;
    }

    reset(DEFAULT_VALUE);
  }, [isOpen, reset, product]);

  return (
    <Modal
      isLoading={isSubmitting}
      isOpen={isOpen}
      title={product ? "Cập nhật sản phẩm" : "Thêm sản phẩm"}
      onClose={onClose}
      onConfirm={handleSubmit}
      {...props}
    >
      <FormProvider
        control={control}
        handleSubmit={useFormSubmit}
        setValue={setValue}
        reset={reset}
        {...methods}
      >
        <div className="grid grid-cols-2 gap-6">
          <Input
            className="block"
            control={control}
            disabled={isSubmitting}
            label="Title"
            name="title"
          />
          <Input
            className="block"
            control={control}
            disabled={isSubmitting}
            label="Price"
            name="price"
          />
        </div>

        <Input
          className="mt-6 block"
          control={control}
          disabled={isSubmitting}
          label="Info"
          name="info"
        />
        <Input
          className="mt-6 block"
          control={control}
          disabled={isSubmitting}
          label="Image"
          name="img"
        />
        <div className="mt-6 grid grid-cols-3 gap-6">
          <Input
            className="block"
            control={control}
            disabled={isSubmitting}
            label="Promotion"
            name="promotion"
          />
          <Input
            className="block"
            control={control}
            disabled={isSubmitting}
            label="Discount"
            name="discount"
          />
          <Input
            className="block"
            control={control}
            disabled={isSubmitting}
            label="Tag"
            name="tag"
          />
        </div>

        <div
          className={twMerge(
            "mt-6 grid gap-6",
            product ? "grid-cols-3" : "grid-cols-2"
          )}
        >
          {product && (
            <Input
              className="block"
              control={control}
              disabled
              label="Rating"
              name="star"
            />
          )}
          <Input
            className="block"
            control={control}
            disabled={isSubmitting}
            label="Category"
            name="category"
          />
          <Input
            className="block"
            control={control}
            disabled={isSubmitting}
            label="Brand"
            name="brand"
          />
        </div>

        <Button type="submit" className="hidden">
          Confirm
        </Button>
      </FormProvider>
    </Modal>
  );
};

export default ProductModificationModal;
