import ProductTableRowAction from "./ProductTableRowAction";
import Table from "../../../components/TableNew/Table";

const ProductTable = ({ rows, onClickEdit, onClickDelete }) => {
  const productColumns = [
    {
      field: "_id",
      headerName: "ID",
      width: 50,
    },
    {
      field: "img",
      headerName: "Image",
      renderCell: (params) => {
        return (
          <div className="flex justify-center items-center">
            <img
              className="w-10 h-10 rounded-full object-contain"
              src={params.row.img}
              alt="avatar"
            />
          </div>
        );
      },
    },
    {
      field: "title",
      headerName: "Name",
      width: 250,
    },
    {
      field: "price",
      headerName: "Price",
      width: 150,
    },
    {
      field: "category",
      headerName: "Category",
      width: 150,
    },
    {
      field: "brand",
      headerName: "Brand",
      width: 150,
    },
    {
      field: "discount",
      headerName: "Discount",
    },
    {
      field: "star",
      headerName: "Rating",
    },
  ];
  const actionColumn = [
    {
      field: "action",
      headerName: "",
      renderCell: (params) => {
        return (
          <ProductTableRowAction
            id={params.row._id}
            onClickEdit={onClickEdit}
            onClickDelete={onClickDelete}
          />
        );
      },
    },
  ];
  return (
    <Table
      rows={rows}
      dataColumns={productColumns}
      actionColumn={actionColumn}
    />
  );
};

export default ProductTable;
