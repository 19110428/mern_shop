import { useState } from "react";
import styles from "./login.module.scss";
import { useUser } from "../../context/UserContext";
import { Navigate, useNavigate } from "react-router-dom";
import { _login } from "../../redux/user/userApi";
import { useDispatch } from "react-redux";
const Login = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { isLoggedIn, setUserState } = useUser();
  const [email, setEmail] = useState("");
  const [redirectToReferrer] = useState(false);
  const [password, setPassword] = useState("");
  const handleEmailChange = () => {
    setEmail(event.target.value);
  };
  const handlePasswordChange = () => {
    setPassword(event.target.value);
  };
  const handleLoginSubmit = async (e) => {
    e.preventDefault();

    const newUser = {
      username: email,
      password: password,
    };
    _login(newUser, dispatch, navigate);
  };

  if (redirectToReferrer) {
    return <Navigate to="/" />;
  }
  if (isLoggedIn) {
    return <Navigate to="/" />;
  }
  return (
    <div className={styles.body}>
      <div className="flex items-center shadow-lg bg-white rounded-2xl w-1/2 h-1/2">
        <div className="w-1/2 text-center">
          <img
            src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.webp"
            className="w-full"
            alt="Sample image"
          />
        </div>
        <div className="w-1/2 text-center">
          <form onSubmit={handleLoginSubmit} className="p-8">
            <h2 className="text-blue-500 text-xl mb-8 uppercase font-bold">
              Trang quản lý
            </h2>
            <input
              type="text"
              name="email"
              value={email}
              onChange={handleEmailChange}
              placeholder="email"
              className="border-none bg-gray-100 w-full mb-4 font-xl block"
            />
            <input
              type="password"
              name="password"
              value={password}
              onChange={handlePasswordChange}
              placeholder="password"
              className="border-none bg-gray-100 w-full mb-4 font-xl block"
            />
            {/* <p
              className="text-right mb-4 text-[14px] text-blue-500 hover:text-blue-700"
              style={{ cursor: "pointer" }}
            >
              Quên mật khẩu?
            </p> */}
            <button
              className="rounded-xl px-5 py-2 text-[14px] text-white bg-blue-500"
              type="submit"
            >
              Đăng nhập
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Login;
