import { useCallback, useEffect, useMemo, useState } from "react";
import { _getAllRating, removeRating } from "../../redux/rating/ratingApi";
import ContentWrapper from "../../components/Layout/Components/ContentWrapper";
import { ConfirmationModal } from "../../components/Modal";
import { toast } from "react-toastify";
import RatingModificationModal from "./Components/RatingModificationModal";
import Datatable from "./Components/RatingTable";

const RatingManagement = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [selectedRatingId, setSelectedRatingId] = useState(null);
  const [ratingData, setRatingData] = useState([]);
  const [isShowModificationModal, setIsShowModificationModal] = useState(false);
  const [isShowDeleteModal, setIsShowDeleteModal] = useState(false);

  const selectedRating = useMemo(() => {
    return ratingData.find((item) => item._id === selectedRatingId) ?? null;
  }, [selectedRatingId, ratingData]);

  const handleClickViewButton = useCallback((id) => {
    setSelectedRatingId(id ?? null);
    setIsShowModificationModal(true);
  }, []);

  const handleClickDeleteButton = useCallback((id) => {
    setSelectedRatingId(id ?? null);
    setIsShowDeleteModal(true);
  }, []);

  const fetchData = useCallback(async () => {
    setIsLoading(true);

    try {
      const data = await _getAllRating();

      setRatingData(data);
      setIsLoading(false);
    } catch (error) {
      toast.error("An unknown error occurred while processing your request.");
    }
  }, []);

  const handleDelete = useCallback(async () => {
    if (!selectedRating) {
      return;
    }

    try {
      await removeRating(selectedRating?._id);

      toast.success("The rating has been deleted successfully.");

      fetchData();
    } catch (error) {
      toast.error(
        "An error occurred while deleting the rating. Please try again later."
      );
    } finally {
      setIsShowDeleteModal(false);
    }
  }, [selectedRating, toast]);

  const handleCloseModal = useCallback(() => {
    setIsShowModificationModal(false);
    setIsShowDeleteModal(false);
    setSelectedRatingId(null);
  }, []);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  return (
    <ContentWrapper title="Rating management">
      {!isLoading && (
        <Datatable
          rows={ratingData}
          onClickView={handleClickViewButton}
          onClickDelete={handleClickDeleteButton}
        />
      )}

      <ConfirmationModal
        message="Are you sure you want to delete this rating? This action cannot be undone."
        isOpen={isShowDeleteModal}
        status="danger"
        title={
          <div>
            Delete rating
            <span className="mx-1 inline-block font-semibold text-red-500">
              {selectedRating?.title}
            </span>
          </div>
        }
        onClose={handleCloseModal}
        onConfirm={handleDelete}
      />

      <RatingModificationModal
        isOpen={isShowModificationModal}
        rating={selectedRating}
        onClose={handleCloseModal}
      />
    </ContentWrapper>
  );
};

export default RatingManagement;
