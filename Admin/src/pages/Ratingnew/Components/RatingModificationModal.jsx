import { useEffect } from "react";
import { FormProvider, useForm } from "react-hook-form";
import Button from "../../../components/Button/Button";
import { Input } from "../../../components/Form";
import { InformationModal } from "../../../components/Modal";

const RatingModificationModal = ({ isOpen, rating, onClose }) => {
  const { control, reset, setValue, ...methods } = useForm({});

  useEffect(() => {
    if (!isOpen) {
      return;
    }

    reset(rating);
  }, [isOpen, reset, rating]);

  return (
    <InformationModal isOpen={isOpen} title="Xem đánh giá" onClose={onClose}>
      <FormProvider
        control={control}
        setValue={setValue}
        reset={reset}
        {...methods}
      >
        <Input
          className="block"
          control={control}
          disabled
          label="Product"
          name="product.title"
        />
        <Input
          className="mt-6 block"
          control={control}
          disabled
          label="User"
          name="user.username"
        />
        <Input
          className="mt-6 block"
          control={control}
          disabled
          label="Star"
          name="star"
        />
        <Input
          className="mt-6 block"
          control={control}
          disabled
          label="Content"
          name="content"
        />
        <Button type="submit" className="hidden">
          Confirm
        </Button>
      </FormProvider>
    </InformationModal>
  );
};

export default RatingModificationModal;
