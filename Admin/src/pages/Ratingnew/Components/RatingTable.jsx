import RatingTableRowAction from "./RatingTableRowAction";
import Table from "../../../components/TableNew/Table";
import RatingDiscuss from "./RatingDiscuss";

const RatingTable = ({ rows, onClickView, onClickDelete }) => {
  const ratingColumns = [
    {
      field: "_id",
      headerName: "ID",
      width: 150,
    },
    {
      field: "product",
      headerName: "Product",
      width: 250,
      renderCell: (params) => {
        return <span>{params.row?.product?.title}</span>;
      },
    },
    {
      field: "user",
      headerName: "User",
      width: 250,
      renderCell: (params) => {
        return <span>{params.row?.user?.username}</span>;
      },
    },
    {
      field: "content",
      headerName: "Content",
      width: 250,
      renderCell: (params) => {
        return (
          <div className="whitespace-normal break-words">
            {params.row?.content}
          </div>
        );
      },
    },
    {
      field: "star",
      headerName: "Star",
      width: 150,
    },
    {
      field: "discuss",
      headerName: "Discuss",
      width: 150,
      renderCell: (params) => {
        return <RatingDiscuss selectedRating={params.row} />;
      },
    },
  ];

  const actionColumn = [
    {
      field: "action",
      headerName: "",
      renderCell: (params) => {
        return (
          <RatingTableRowAction
            id={params.row._id}
            onClickView={onClickView}
            onClickDelete={onClickDelete}
          />
        );
      },
    },
  ];
  return (
    <Table
      rows={rows}
      dataColumns={ratingColumns}
      actionColumn={actionColumn}
    />
  );
};

export default RatingTable;
