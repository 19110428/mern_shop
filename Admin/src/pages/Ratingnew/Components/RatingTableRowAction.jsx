import { FiTrash2 } from "react-icons/fi";
import { MdOutlineRemoveRedEye } from "react-icons/md";
import TableRowAction from "../../../Components/TableNew/TableRowAction";

const RatingTableRowAction = ({ id, onClickView, onClickDelete }) => {
  return (
    <div className="flex items-center justify-end space-x-2">
      <TableRowAction id={id} title="Xem" onClick={onClickView}>
        <MdOutlineRemoveRedEye size={16} />
      </TableRowAction>
      <TableRowAction
        id={id}
        status="danger"
        title="Xóa"
        onClick={onClickDelete}
      >
        <FiTrash2 size={16} />
      </TableRowAction>
    </div>
  );
};

export default RatingTableRowAction;
