export const AUTHENTICATION_PATH = {
  LOGIN_PATH: "/auth/login",
  REGISTER_PATH: "/auth/register",
  FORGET_PASSWORD_PATH: "/auth/forget-password",
  RESET_PASSWORD_PATH: "/auth/reset-password",
  REFRESH_TOKEN_PATH: "v1/access-token",
};

export const MY_PATH = {
  HOME_PATH: "/my",
  APP_PATH: "/my/apps",
  PAYMENT_HISTORY_PATH: "/my/payment-histories",
  USER_CREDIT_HISTORY_PATH: "/my/user-credit-histories",
  ACCOUNT_PATH: "/my/profile/account",
  NOTIFICATIONS_PATH: "/my/profile/notifications",
};

export const ADMIN_PATH = {
  HOME_PATH: "/admin",
  APP_PATH: "/admin/apps",
  USER_PATH: "/admin/users",
  PAYMENT_HISTORY_PATH: "/admin/payment-histories",
  USER_CREDIT_HISTORY_PATH: "/admin/user-credit-histories",
  ACCOUNT_PATH: "/admin/profile/account",
};

export const SYSTEM_PATH = {
  HOME_PATH: "/system",
  CONFIG_PATH: "/system/configs",
  SCRIPT_ENDPOINT_PATH: "/system/script-endpoints",
  SCRIPT_PATH: "/system/scripts",
  PAYMENT_TYPE_PATH: "/system/payment-types",
  PAYMENT_STATUS_PATH: "/system/payment-statuses",
  ACCOUNT_PATH: "/system/profile/account",
};

export const COMMON_PATH = {
  HOME_PATH: "/",
  ABOUT_PATH: "/about",
  BLOG_PATH: "/blog",
  PARTNER_PATH: "/partner",
  CONTACT_PATH: "/contact",
  GETTING_STARTED_PATH: "/getting-started",
  DOCUMENTATION_PATH: "/documentations",
  GUIDE_PATH: "/guides",
  FAQ_PATH: "/faqs",
  TERM_PATH: "/terms",
  NOTICE_PATH: "/notices",
  CLAIM_PATH: "/claims",
  NOT_FOUND_PATH: "/*",
};
