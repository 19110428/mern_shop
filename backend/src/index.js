const express = require("express");
const morgan = require("morgan");
const route = require("./routes");
const db = require("./config/db");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const app = express();
const passportSetup = require("./services/passport");
const passport = require("passport");
const cookieSession = require("cookie-session");
const session = require('express-session')

//Middleware
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// HTTP logger
app.use(morgan("combined"));

// DB
db.connect();

// app.use(
//   cookieSession({ name: "session", keys: ["lama"], maxAge: 24 * 60 * 60 * 100 })
// );
app.use(session({
  secret: 'somethingsecretgoeshere',
  resave: false,
  saveUninitialized: false,
}));
app.use(passport.initialize());
app.use(passport.session());

//Cookie
app.use(cors({
  origin: ["http://localhost:3000","http://127.0.0.1:3000","http://localhost:4000","http://127.0.0.1:4000"],
  methods: "GET,POST,PUT,DELETE",
  credentials: true,
}));
app.use(cookieParser());


// Routes init
route(app);

app.listen(8000, () => {
  console.log(`App listening on port 8000`);
});