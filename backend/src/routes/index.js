const authRouter = require("./common/auth");
const userRouter = require("./common/user");
const productRouter = require("./common/product");
const orderRouter = require("./common/order");
const orderDetailRouter = require("./common/orderdetail");
const paymentRouter = require("./common/payment");
const ratingRouter = require("./common/rating");
const voucherRouter = require("./common/voucher")

function route(app) {
  app.use("/auth", authRouter);

  app.use("/user", userRouter);

  app.use("/product", productRouter);

  app.use("/order", orderRouter);

  app.use("/orderdetail", orderDetailRouter);

  app.use("/rating", ratingRouter);

  app.use("/voucher", voucherRouter);

  app.use("/payment", paymentRouter);

  app.use("/voucher", voucherRouter);

  app.use("/payment", paymentRouter);
}

module.exports = route;
