var express = require("express");
var router = express.Router();
const authController = require("../../app/controllers/common/AuthController");
const {verifyToken} = require("../../app/controllers/common/verifyController.js");
const passport = require("passport");

//REGISTER NORMAL
router.post("/register", authController.register);
//LOGIN NORMAL
router.post("/login", authController.login);
//LOGOUT PASSPORT
router.get("/logout", (req, res) => {
  req.logout(function(err) {
    if (err) {
      return next(err);
    }
    res.redirect("http://localhost:3000/");
  });
  
});
//LOG OUT
router.post("/logout", authController.logOut);

//REFRESH TOKEN
router.post("/refresh", authController.requestRefreshToken);
// RETURN USER INFO
router.get("/login/success", authController.loginSuccess);

router.get(
  "/google",
  passport.authenticate("google", { scope: ["profile", "email"] })
);

router.get(
  "/google/callback",
  passport.authenticate("google", {
    successRedirect: "http://localhost:3000/",
    failureRedirect: "http://localhost:3000/login",
  })
);

router.get(
  "/facebook",
  passport.authenticate("facebook", { scope: ["public_profile"] })
);

router.get(
  "/facebook/callback",
  passport.authenticate("facebook", {
    successRedirect: "http://localhost:3000/",
    failureRedirect: "http://localhost:3000/login",
  })
);

router.post("/phone/signup", authController.signup);

router.post("/phone/verify", authController.verifyOTP);

module.exports = router;
