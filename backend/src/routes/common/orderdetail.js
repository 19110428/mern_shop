var express = require("express");
var router = express.Router();

const orderdetailController = require("../../app/controllers/common/orderDetailController.js");
const {
  verifyToken,
  verifyTokenAndAdmin,
  verifyTokenAndUserAuthorization,
  verifyTokenAndSeller,
} = require("../../app/controllers/common/verifyController.js");

//local/order/all
//GET ALL USERS

router.get("/all", orderdetailController.getAllorderDetail);

//DELETE USER
router.delete("/delete/:id", orderdetailController.deleteorderDetail);

router.get("/get/:id", orderdetailController.getorderDetail);

router.put("/edit/:id", orderdetailController.update);

router.post("/new", orderdetailController.neworder);

router.get("/getLength", orderdetailController.getorderDetailLength);

module.exports = router;
