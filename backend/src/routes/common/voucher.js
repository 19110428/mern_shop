var express = require("express");
var router = express.Router();

const voucherController = require("../../app/controllers/common/VoucherController.js");

//local/voucher/all
//GET ALL VOUCHER
router.get("/all", voucherController.getAllVoucher);

//DELETE VOUCHER
router.delete(
  "/delete/:id",

  voucherController.deleteVoucher
);

router.get("/get/:id", voucherController.getVoucher);

router.put("/edit/:id", voucherController.update);

router.post("/new", voucherController.newVoucher);



module.exports = router;
