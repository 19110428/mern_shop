var express = require("express");
var router = express.Router();

const orderController = require("../../app/controllers/common/orderController.js");
const {
  verifyToken,
  verifyTokenAndAdmin,
  verifyTokenAndUserAuthorization,
  verifyTokenAndSeller,
} = require("../../app/controllers/common/verifyController.js");

//local/order/all
//GET ALL USERS
router.get(
  "/getseller/:id",
  verifyTokenAndSeller,
  orderController.getAllorderByIdSeller
);

// tat ca Order 1 ng
router.get("/getbyid/:id",orderController.getAllorderById);

// Get 1 order by order id
router.get("/getorderdetail/:id",orderController.getOrderById);


router.get("/all",orderController.getAllorder);

//DELETE USER
router.delete(
  "/delete/:id",
  verifyTokenAndUserAuthorization,
  orderController.deleteorder
);

router.get("/get/:id", verifyTokenAndUserAuthorization, orderController.getorder);

router.put("/edit/:id", orderController.update);

router.post("/new", orderController.neworder);

router.get("/getFull/:id", orderController.getFullorder);

router.get("/getLength", orderController.getorderLength);

module.exports = router;
