var express = require("express");
const { reqPayUrl } = require("../../services/momo");

const axios = require("axios");
var router = express.Router();
const {
    verifyToken,
    verifyTokenAndAdmin,
    verifyTokenAndUserAuthorization,
    verifyTokenAndSeller,
  } = require("../../app/controllers/common/verifyController.js");
const order = require("../../app/models/order");

  router.get("/", (req, res) => {
    res.json({ name: "momo payment" });
});

router.post("/momo/notify", async (req, res, next) => {
    console.log("da thong bao");
    const data = req.body;
    const orderId = data.orderId.split("-")[0];
    console.log("----------ỏderid",orderId);
    console.log(data.resultCode);
    if (data.resultCode === 0) {
        try {
          await order.updateOne({ _id: orderId }, {
            payment: {
                name: "momo",
                paid: true,
            }.then((res)=>{console.log("--------------------",res)})
        })
        } catch (err) {
            next(err);
        }
    }
    // else{
    //   try{

    //   }catch(err){
    //     next(err)
    //   }
    // }
    console.log(data);
});

router.post("/paymentUrl", (request, response) => {
    const data = request.body;
    const params = {
        ...data,
    };
    const payUrl = reqPayUrl(request, response, params);
});




 module.exports = router;
