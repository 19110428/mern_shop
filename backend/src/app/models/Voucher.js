const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Voucher = new Schema(
  {
    _id: { type: String },
    name: { type: String, },
    description:{type:String},
    discount: { type: String },
    image:{type:String},
    redeem_time: {type:Date},
  },
  { timestamps: true }
);

module.exports = mongoose.model("Voucher", Voucher);

