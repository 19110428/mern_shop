const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const slug = require("mongoose-slug-generator");

mongoose.plugin(slug);
//const AutoIncrement = require('mongoose-sequence')(mongoose);

const Product = new Schema(
  {
    _id: { type: String },
    title: { type: String },
    price: { type: String },
    info: { type: String },
    gallery: [],
    img: {
      type: String,
      default: "",
    },
    promotion: { type: String },
    discount: { type: String },
    tag: { type: String },
    gift: { type: String },
    star: { type: String, default: "" },
    totalVote: { type: String, default: "" },
    amount: { type: String, default: "" },
    status: { type: String, default: "" },
    category: { type: String },
    brand: { type: String },
    parameter: {},
    slug: {
      type: String,
      slug: "name",
      unique: true,
    },
  },
  { _id: false, timestamps: true }
);
Product.index({title: 'text'});

//Product.plugin(AutoIncrement,);
module.exports = mongoose.model("Product", Product);
