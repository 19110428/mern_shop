const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const slug = require("mongoose-slug-generator");
const order = require("./order");
const Product = require("./Product");
//const AutoIncrement = require('mongoose-sequence')(mongoose);
mongoose.plugin(slug);

const orderDetail = new Schema(
  {
    _id: { type: String },
    order_id: { type: String, ref: order },
    product_id: { type: String, ref: Product },
    unit_price: { type: String },
    quantity: { type: String },
  },
  { _id: false, timestamps: true }
);
//orderDetail.plugin(AutoIncrement);
module.exports = mongoose.model("orderDetail", orderDetail);
