const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const User = require("./User");
const Product = require("./Product");


const order = new Schema(
  {
    _id: { type: String },
    customer_id: { type: String, ref: User },
    fullname:{type:String},
    phone: { type: String },
    address: { type: String },
    receiver: { type: String },
    payment: { },
    totalPrice:{},
    totalQuantity:{},
    status: { type: String },
    order_items:[],
    time:{}
  },
  { _id: false, timestamps: true }
);

module.exports = mongoose.model("order", order);
