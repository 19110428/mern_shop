const orderDetail = require("../../models/orderDetail");

class orderDetailController {
  // [GET] /orderDetail/all
  async getAllorderDetail(req, res, next) {
    orderDetail
      .find({})
      .then((orderDetail) => {
        res.status(200).json(orderDetail);
      })
      .catch(() => {
        res.status(500).json(err);
      });

    // res.send('detail'+req.params.slug)
  }

  // [delete] /orderDetail/delete/:id
  async deleteorderDetail(req, res) {
    try {
      await orderDetail.findByIdAndDelete(req.params.id);
      res.status(200).json("orderDetail deleted");
    } catch (err) {
      return res.status(500).json(err);
    }
  }
  // [GET] /orderDetail/get/:id
  async getorderDetail(req, res, next) {
    await orderDetail
      .findById(req.params.id)
      .then((orderDetail) => {
        res.status(200).json(orderDetail);
      })
      .catch(next);
  }
  //[PUT]  /orderDetail/edit/:id
  async update(req, res, next) {
    await orderDetail
      .updateOne({ _id: req.params.id }, req.body)
      .then(() => res.status(200).json("Updated Success"))
      .catch(next);
  }

  //  [POSt] /Product/new
  async neworder(req, res, next) {
    const formData = req.body;

    const neworder = new orderDetail(formData);

    neworder
      .save()
      .then(() => res.status(200).json(neworder))
      .catch((error) => {
        res.status(500).json(error);
      });
  }

  async getorderDetailLength(req, res) {
    await orderDetail
      .find()
      .count()
      .then((user) => res.status(200).json(user))
      .catch((err) => {ư
        return res.status(500).json(err);
      });
  }
}

module.exports = new orderDetailController();
