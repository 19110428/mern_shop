const User = require("../../models/User");
const bcrypt = require("bcrypt");
const { promise } = require("bcrypt/promises");
const jwt = require("jsonwebtoken");

class UserController {
  // [GET] /user/all
  async getAllUser(req, res, next) {
    User.find({})
      .then((user) => {
        res.status(200).json(user);
      })
      .catch(() => {
        res.status(500).json(err);
      });

    // res.send('detail'+req.params.slug)
  }

  // [delete] /user/delete/:id
  async deleteUser(req, res) {
    try {
      await User.findByIdAndDelete(req.params.id);
      res.status(200).json("User deleted");
    } catch (err) {
      return res.status(500).json(err);
    }
  }
  // [GET] /user/get/:id
  async getUser(req, res, next) {
    await User.find({ userId: req.params.id })
      .then((user) => {
        console.log(user);
        res.status(200).json(user);
      })
      .catch(next);
  }
  //[PUT]  /user/edit/:id
  async update(req, res, next) {
    await User.updateOne({ _id: req.params.id }, req.body)
      .then(() => {
        User.findById(req.params.id).then((user) => {
          res.status(200).json(user);
        });
      })
      .catch((err) => {
        return res.status(500).json(err);
      });
  }
  //[PUT]  /user/editPass/:id
  async updatePass(req, res, next) {
     // Get info
     const formData = req.body;
     // Encode pass
     const salt = await bcrypt.genSalt(10); 
     formData.password = await bcrypt.hash(formData.password, salt);
    
    await User.updateOne({ _id: req.params.id }, formData)
    .then(() => {
        User.findById(req.params.id).then((user) => {
          res.status(200).json(user);
        });
      })
      .catch((err) => {
        return res.status(500).json(err);
      });
  }
  

  //  [POSt] /user/new
  async newUser(req, res, next) {
    try {
      const count = await User.find().count();
      const formData = req.body;

      User.init();
      const temp = { ...formData, userId: count + 1 };
      const user = new User(temp);

      user
        .save()
        .then((user) => res.status(200).json(user))
        .catch((error) => {
          console.log(error);
          res.status(500).json(error);
        });
    } catch (error) {
      console.log(error);
    }
  }

  async getUserLength(req, res) {
    await User.find()
      .count()
      // await User.aggregate( [
      //     { $count: 'username'  }
      // ])
      .then((user) => res.status(200).json(user))
      .catch((err) => {
        return res.status(500).json(err);
      });
  }
}

module.exports = new UserController();
