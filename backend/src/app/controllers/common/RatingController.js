const Rating = require("../../models/Rating");

class RatingController {
  // [GET] /Rating/all
  async getAllRating(req, res, next) {
    Rating.find({})
      .then((rating) => {
        res.status(200).json(rating);
      })
      .catch(() => {
        res.status(500).json(err);
      });
  }

  // [GET] /Rating/all
  // async getAllRatingByUser(req, res, next) {
  //   Rating.find({})
  //     .populate("customer_id")
  //     .then((Rating) => {
  //       res.status(200).json(Rating);
  //     })
  //     .catch(() => {
  //       res.status(500).json(err);
  //     });

  //   // res.send('detail'+req.params.slug)
  // }

  // [delete] /Rating/delete/:id
  async deleteRating(req, res) {
    try {
      await Rating.findByIdAndDelete(req.params.id);
      res.status(200).json("Rating deleted");
    } catch (err) {
      return res.status(500).json(err);
    }
  }
  // [GET] /Rating/get/:id product
  async getRating(req, res, next) {
    await Rating.find({ product_id: req.params.id })
      .populate("user_id")
      .then((rating) => {
        res.status(200).json(rating);
      })
      .catch(next);
  }

  //[PUT]  /Rating/edit/:id
  async updateRating(req, res, next) {
    const rating = await Rating.findById(req.params.id);

    const check = rating.discuss.some((e) => {
      if (e._id === req.body._id) return true;
    });
    if (check) {
      await Rating.updateOne(
        {
          _id: req.params.id,
          "discuss._id": req.body._id,
        },
        { $set: { "discuss.$.discuss": [req.body] } }
      )
        .then(() => res.status(200).json("Reply Success"))
        .catch(next);
    } else {
      await Rating.updateOne(
        { _id: req.params.id },
        { $push: { discuss: req.body } }
      )
        .then(() => res.status(200).json("Updated Success"))
        .catch(next);
    }
  }

  //[PUT]  /rating/:id/addDiscuss
  async addDiscussRating(req, res, next) {
    await Rating.updateOne(
      { _id: req.params.id },
      { $push: { discuss: req.body } }
    )
      .then(() => res.status(200).json("Reply Success"))
      .catch((err) => {
        return res.status(500).json(err);
      });
  }

  //  [POSt] /Product/new
  async newRating(req, res, next) {
    const formData = req.body;

    const newRating = new Rating(formData);

    newRating
      .save()
      .then(() => res.status(200).json(newRating))
      .catch((error) => {
        res.status(500).json(error);
      });
  }

  async getRatingPage(req, res, next) {
    const perPage = 10; //10docs in single page
    const page = req.params.id;
    await Rating.find({})
      .skip(perPage * page)
      .limit(perPage)
      .populate("product_id")
      .populate("customer_id")
      .then((rating) => res.status(200).json(rating))
      .catch((error) => {
        res.status(500).json(error);
      });
  }
}

module.exports = new RatingController();
