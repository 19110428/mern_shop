const order = require("../../models/order");
const orderDetail = require("../../models/orderDetail");
const mongoose = require("mongoose");

class orderController {
  // [GET] /order/all
  async getAllorder(req, res, next) {
    order.find({})

      .then((order) => {
        res.status(200).json(order);
      })
      .catch(() => {
        res.status(500).json(err);
      });

    // res.send('detail'+req.params.slug)
  }

  // [delete] /order/delete/:id
  async deleteorder(req, res) {
    try {
      await order.findByIdAndDelete(req.params.id);
      res.status(200).json("order deleted");
    } catch (err) {
      return res.status(500).json(err);
    }
  }
  // [GET] /order/get/:id
  async getorder(req, res, next) {
    await order.findById(req.params.id)
      .then((order) => {
        res.status(200).json(order);
      })
      .catch(next);
  }
  //[PUT]  /order/edit/:id
  async update(req, res, next) {
    await order.updateOne({ _id: req.params.id }, req.body)
      .then(() => res.status(200).json("Updated Success"))
      .catch(next);
  }
  //  [POSt] /Product/new
  async neworder(req, res, next) {
    const formData = req.body;

    const neworder = new order(formData);

    neworder
      .save()
      .then(() => res.status(200).json(neworder))
      .catch((error) => {
        res.status(500).json(error);
        console.log(error);
      });
  }

  //  [POSt] /
  async getFullorder(req, res, next) {
    orderDetail.find({ order_id: req.params.id })
      .populate("order_id")
      .populate({
        path: "order_id",
        populate: { path: "customer_id" },
      })

      .then((order) => {
        console.log(req.params.id);
        res.status(200).json(order);
      })
      .catch((error) => {
        res.status(500).json(error);
        console.log(error);
      });
  }
  // [GET] /order/all
  async getAllorderById(req, res, next) {
    order.find({ customer_id: req.params.id })
     
      .then((order) => {
        res.status(200).json(order);
      })
      .catch((err) => {
        res.status(500).json(err);
      });

    // res.send('detail'+req.params.slug)
  }

  async getOrderById(req, res, next) {
    order.findById(req.params.id )
     
      .then((order) => {
        res.status(200).json(order);
      })
      .catch((err) => {
        res.status(500).json(err);
      });

    // res.send('detail'+req.params.slug)
  }

  // [GET] /order/all
  async getAllorderByIdSeller(req, res, next) {
    order.find({ seller_id: req.params.id })
      // .populate('seller_id')
      .populate("customer_id")
      .populate("pay_id")
      .populate("status")
      .then((order) => {
        res.status(200).json(order);
      })
      .catch(() => {
        res.status(500).json(err);
      });

    // res.send('detail'+req.params.slug)
  }

  async getorderLength(req, res) {
    await order.find()
      .count()
      .then((user) => res.status(200).json(user))
      .catch((err) => {
        return res.status(500).json(err);
      });
  }
}

module.exports = new orderController();
