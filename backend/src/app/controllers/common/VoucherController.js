const Voucher = require("../../models/Voucher");

class VoucherController {
  // [GET] /voucher/all
  async getAllVoucher(req, res, next) {
    Voucher.find({})
      .then((voucher) => {
        res.status(200).json(voucher);
      })
      .catch(() => {
        res.status(500).json(err);
      });

    // res.send('detail'+req.params.slug)
  }

  // [delete] /voucher/delete/:id
  async deleteVoucher(req, res) {
    try {
      await Voucher.findByIdAndDelete(req.params.id);
      res.status(200).json("Voucher deleted");
    } catch (err) {
      return res.status(500).json(err);
    }
  }
  // [GET] /voucher/get/:id
  async getVoucher(req, res, next) {
    await Voucher.find({ voucherId: req.params.id })
      .then((voucher) => {
        console.log(voucher)
        res.status(200).json(voucher);
      })
      .catch(next);
  }
  //[PUT]  /voucher/edit/:id
  async update(req, res, next) {
    await Voucher.updateOne({ _id: req.params.id }, req.body)
      .then(() => {
        Voucher.findById(req.params.id).then((voucher) => {
          res.status(200).json(voucher);
        });
      })
      .catch((err) => {
        return res.status(500).json(err);
      });
  }
  
  //  [POSt] /voucher/new
  async newVoucher(req, res, next) {
    try {
      const dataVoucher = await Voucher.find();
      const count = last(dataVoucher)?._id ?? 0;
      const formData = req.body;

      Voucher.init();
      const temp = { ...formData, _id: Number(count) + 1 };
      const voucher = new Voucher(temp);

      voucher
        .save()
        .then((voucher) => res.status(200).json(voucher))
        .catch((error) => {
          console.log(error);
          res.status(500).json(error);
        });
    } catch (error) {
      console.log(error);
    }
  }

  
}

module.exports = new VoucherController();
