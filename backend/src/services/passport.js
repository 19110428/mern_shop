const GoogleStrategy = require("passport-google-oauth20").Strategy;
const FacebookStrategy = require("passport-facebook").Strategy;
const passport = require("passport");


const GOOGLE_CLIENT_ID ="980008302286-b9ar3crpidujm6abjdpptabhjvuicbub.apps.googleusercontent.com";
const GOOGLE_CLIENT_SECRET = "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";

FACEBOOK_APP_ID = "1453202605185152";
FACEBOOK_APP_SECRET = "bfb6d1ac960003b288e75efcced7b7fd";
const User = require("../app/models/User");

const Counter = require("../app/models/Counter");

passport.use(
  new GoogleStrategy(
    {
      clientID: GOOGLE_CLIENT_ID,
      clientSecret: GOOGLE_CLIENT_SECRET,
      callbackURL: "/auth/google/callback",
    },
    function (accessToken, refreshToken, profile, done) {
     
      User.findOne({ googleId: profile.id }).then(existingUser => {
        if (existingUser) {
          done(null, existingUser);
        } else {
          // insert new user id
          Counter.findOneAndUpdate({collectionName:"user"},
                                  {"$inc":{seq:1}},{new:true},
                                  (err,cd)=>{
                                      let id = cd.seq
                                      new User({
                                        userId: id,
                                          username:profile.emails[0].value,
                                          phone:"",
                                          verifyMail:true,
                                          googleId:profile.id,
                                          fullname: profile.displayName,
                                          gender:"",
                                          birthday:"",
                                          image: profile._json.picture
                                      }).save()
                                      .then((info) => done(null, info ))         
                                  })

          
           
        }
      });
    }
    
  )
);


passport.use(
  new FacebookStrategy(
    {
      clientID: FACEBOOK_APP_ID,
      clientSecret: FACEBOOK_APP_SECRET,
      callbackURL: "/auth/facebook/callback",
    },
    function (accessToken, refreshToken, profile, done) {
      
      done(null, profile);
      // User.findOrCreate({ googleId: profile.id }, function (err, user) {
      //   return cb(err, user);
      // });
    }
  )
);


// // For facebook
// passport.use(
//   new FacebookStrategy(
//     {
//       clientID: keys.FACEBOOK_APP_ID,
//       clientSecret: keys.FACEBOOK_APP_SECRET,
//       callbackURL: "/auth/facebook/callback"
//     },
//     (accessToken, refreshToken, profile, done) => {
//       console.log(profile);
//       /* ========= DATABASE CHECK PRE EXIST AND INSERT QUERY: START =========  */
//       // check if user id already inserted
//       User.findOne({ userId: profile.id }).then(existingUser => {
//         if (existingUser) {
//           done(null, existingUser);
//         } else {
//           // new user case
//           // insert new user id
//           new User({
//             userId: profile.id,
//             username: profile.displayName,
//             picture: profile._json.picture
//           })
//             .save()
//             .then(user => {
//               done(null, user);
//             });
//         }
//       });
//       /* ========= DATABASE CHECK PRE EXIST AND INSERT QUERY: END =========  */
//     }
//   )
// );

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});

